﻿
function GetUsername() {

    $.ajax({
        type: "GET",
        url: '/Home/GetUserNameList',
        success: function (data) {

            var Username = data[0].UserName;



            if (Username == "Scoto") {
                //alert('Scoto');                
                $('#Menu-Dashboard').css('display', 'block');

                $('#Menu-Master').css('display', 'block');
                $('#Menu-UserCreation').css('display', 'block');
                $('#Menu-UserRights').css('display', 'block');
                $('#Menu-HolidayMaster').css('display', 'block');
                $('#Menu-RouteMaster').css('display', 'block');
                $('#Menu-PFESIMaster').css('display', 'block');
                $('#Menu-LabourAllotment').css('display', 'block');

                $('#Menu-BiometricMachine').css('display', 'block');


                $('#Menu-EmployeeProfile').css('display', 'block');
                $('#Menu-EmployeeDetails').css('display', 'block');
                $('#Menu-EmployeeApproval').css('display', 'block');
                $('#Menu-EmployeeStatu').css('display', 'block');

                $('#ManualEntry').css('display', 'block');
                $('#Menu-ManualAttendance').css('display', 'block');
                $('#Menu-ManualOT').css('display', 'block');
                $('#Manu-ManualLeave').css('display', 'block');
                $('#Menu-ManualTimeDelete').css('display', 'block');

                $('#Menu-Report').css('display', 'block');


            }
            else {

                //alert(Username);

                $('#Menu-Dashboard').css('display', 'none');

                $('#Menu-Master').css('display', 'none');
                $('#Menu-UserCreation').css('display', 'none');
                $('#Menu-UserRights').css('display', 'none');
                $('#Menu-HolidayMaster').css('display', 'none');
                $('#Menu-RouteMaster').css('display', 'none');
                $('#Menu-PFESIMaster').css('display', 'none');
                $('#Menu-LabourAllotment').css('display', 'none');

                $('#Menu-BiometricMachine').css('display', 'none');


                $('#Menu-EmployeeProfile').css('display', 'none');
                $('#Menu-EmployeeDetails').css('display', 'none');
                $('#Menu-EmployeeApproval').css('display', 'none');
                $('#Menu-EmployeeStatu').css('display', 'none');

                $('#ManualEntry').css('display', 'none');
                $('#Menu-ManualAttendance').css('display', 'none');
                $('#Menu-ManualOT').css('display', 'none');
                $('#Manu-ManualLeave').css('display', 'none');
                $('#Menu-ManualTimeDelete').css('display', 'none');

                $('#Menu-Report').css('display', 'none');

            }

            $.ajax({
                type: "GET",
                url: '/Home/GetHideAndShow',

                success: function (data) {
                    //alert('hi');
                    $.each(data, function (key, item) {

                        var MenuID = item.MenuName;
                        var FormID = item.FormName;

                        $('#' + MenuID + '').css('display', 'block');
                        $('#' + FormID + '').css('display', 'block');

                    });


                }

            });
        }

    });

}


$(document).ready(function () {

    $('#Menu-Dashboard').css('display', 'none');   

    $('#Menu-Master').css('display', 'none');
    $('#Menu-UserCreation').css('display', 'none');
    $('#Menu-UserRights').css('display', 'none');
    $('#Menu-HolidayMaster').css('display', 'none');
    $('#Menu-RouteMaster').css('display', 'none');
    $('#Menu-PFESIMaster').css('display', 'none');   
    $('#Menu-LabourAllotment').css('display', 'none');

    $('#Menu-BiometricMachine').css('display', 'none'); 


    $('#Menu-EmployeeProfile').css('display', 'none');
    $('#Menu-EmployeeDetails').css('display', 'none');
    $('#Menu-EmployeeApproval').css('display', 'none');
    $('#Menu-EmployeeStatu').css('display', 'none');  

    $('#ManualEntry').css('display', 'none');
    $('#Menu-ManualAttendance').css('display', 'none');
    $('#Menu-ManualOT').css('display', 'none');
    $('#Manu-ManualLeave').css('display', 'none');
    $('#Menu-ManualTimeDelete').css('display', 'none');

    $('#Menu-Report').css('display', 'none');
  

    GetUsername();




});