﻿$(function () {

    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });

    $('.select2').select2();

    $('#example1').DataTable();
    $('#example2').DataTable();

});


function LoadReportMenu() {
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GetReportMenu',
        success: function (data) {

            $("#ddlReportMenu").empty();
            $("#ddlReportMenu").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlReportMenu").append($('<option></option>').val(val.UserName).text(val.UserName));

            })
        }
    })

}

function LoadReportSubMenu() {
    alert('hi');
    var Report_Head = $("#ddlReportMenu").val();
    $("#ddlReportSubMenu").removeAttr("disabled");
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GetReportSubMenu/?Report_Head=' + Report_Head + '',
        success: function (data) {

            $("#ddlReportSubMenu").empty();
            $("#ddlReportSubMenu").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlReportSubMenu").append($('<option></option>').val(val.WagesType).text(val.WagesType));

            })
        }
    })

}

function LoadReportShift() {
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GettShifType',
        success: function (data) {

            $("#ddlShift").empty();
            $("#ddlShift").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlShift").append($('<option></option>').val(val.ShiftType).text(val.ShiftType));

            })
        }
    })

}
function LoadDepartment() {
    //alert('Dept');
   
    $.ajax({
        type: "GET",
        url: '/Report/GetDepartment',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlDept').empty();
            $("#ddlDept").append($('<option></option>').val('0').text('-Select-'));
            $.each(data, function (i, val) {

                $("#ddlDept").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadReportWages() {
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GettWagesType',
        success: function (data) {

            $("#ddlWagesType").empty();
            $("#ddlWagesType").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlWagesType").append($('<option></option>').val(val.WagesType).text(val.WagesType));

            })
        }
    })

}




$("#btnLoad").click(function () {

    var FromDate = $('#txtFromDate').val();
    var ShiftType = $('#ddlShift').val();
    var ReportNames = $('#ddlReportSubMenu').val();

    if (ReportNames == "DayAttendanceDayWise") {

        alert(ReportNames);
        var isAllValid = true;

        //if ($('#ddlShift').val() == '0') {

        //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

        //    isAllValid = false;
        //}
        //else {

        //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
        //}

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                type: "GET",
                url: '/Report/DayWiseDatatable/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '',
                success: function (data) {

                    var data1 = $('#example2').DataTable();
                    data1.clear();
                    $.each(data, function (key, item) {

                        data1.row.add([
                        item.SNo,
                        item.EmpNo,
                        item.EmpName,
                        item.DeptName,
                        item.TimeIn,
                        item.TimeOut,
                        item.Total_Hrs,
                       //"<button class='btn btn-success' type='button' onClick='EditEmpID(\"" + item.EmpNo + "\")'><i class='fa fa-pencil'></i></button> "
                        ]).draw();
                    });

                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }

    }
    //DayAttendanceBetWDates


});

$(document).ready(function () {

    LoadReportMenu();
    LoadReportSubMenu();
    LoadReportShift();
    LoadReportWages();  
    LoadDepartment();
  

    $("#btnReport").click(function () {
        alert('hi');
        var FromDate = $('#txtFromDate').val();
        var ToDate = $('#txtToDate').val();
        var ShiftType = $('#ddlShift').val();
        var WagesType = $('#ddlWagesType').val();
        var ReportNames = $('#ddlReportSubMenu').val();
        var EmpNo = $('#txtEmpNo').val();

        alert(ReportNames);
        if (ReportNames == "DAY ATTENDANCE - DAY WISE") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/DayAttendanceDayWise/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }
        else if (ReportNames == "DAY ATTENDANCE - BETWEEN DATES") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/MusterReportBWDates/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }
        //Salary Consolidate
        if (ReportNames == "SALARY CONSOLIDATE REPORT") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/SalaryConsolidate/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }




        //BelowHours
        if (ReportNames == "BELOW HOURS REPORT") {


            var isAllValid = true;

           

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/belowhour/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '&BelowHour=' + BelowHour + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }

    }





        //Abovehours
        if (ReportNames == "ABOVE HOURS REPORT") {


            var isAllValid = true;

           

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/Abovehour/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '&AboveHour=' + AboveHour + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }




    // Betweenhours
        if (ReportNames == "BELOW  AND ABOVE HOURS REPORT") {


        var isAllValid = true;

           

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/Betweenhour/?AboveHour=' + AboveHour + '&BelowHour=' + BelowHour + '&FromDate=' + FromDate + '&ShiftType=' + ShiftType + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }

    }




    //  Payrollot
    if (ReportNames == "PAYROLL OT") {


        var isAllValid = true;

        if ($('#ddlWagesType').val() == '0') {

            document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            isAllValid = false;
        }
        else {

            document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
        }

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if ($('#txtToDate').val().trim() == '') {

            document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

            isAllValid = false;
        }
        else {

            document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/payrollotreport/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }

    }









    //AbsentBetWDates

    if (ReportNames == "ABSENT REPORT - BETWEEN DATES") {

        var isAllValid = true;

        if ($('#ddlWagesType').val() == '0') {

            document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            isAllValid = false;
        }
        else {

            document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
        }

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if ($('#txtToDate').val().trim() == '') {

            document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

            isAllValid = false;
        }
        else {

            document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/AbsentReportBetweenDates/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }
    }

    //AbsentDayWise
        
    if (ReportNames == "ABSENT REPORT DAY WISE") {


        var isAllValid = true;

           

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/AbsentReportDayWise/?FromDate=' + FromDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }

    }

    //Day Attendance Summary

    if (ReportNames == "DAY ATTENDANCE SUMMARY") {


        var isAllValid = true;



        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/DayAttendanceSummary/?FromDate=' + FromDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }

    }

      



    //hours attendance between dates
        
    if (ReportNames == "HOURS ATTENDANCE - BETWEEN DATES") {

        var isAllValid = true;

        if ($('#ddlWagesType').val() == '0') {

            document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            isAllValid = false;
        }
        else {

            document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
        }

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if ($('#txtToDate').val().trim() == '') {

            document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

            isAllValid = false;
        }
        else {

            document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/HoursBtwnDates/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }
    }


    
    if (ReportNames == "DAY EMPLOYEE SUMMARY") {

        var isAllValid = true;

        //if ($('#ddlWagesType').val() == '0') {

        //    document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

        //    isAllValid = false;
        //}
        //else {

        //    document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
        //}

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if ($('#txtToDate').val().trim() == '') {

            document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

            isAllValid = false;
        }
        else {

            document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
        }

           

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/ImproperPunch/?FromDate=' + FromDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }
    }

    //PayRollAttendance
    if (ReportNames == "PAYROLL ATTENDANCE") {

        var isAllValid = true;

        if ($('#ddlWagesType').val() == '0') {

            document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            isAllValid = false;
        }
        else {

            document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
        }

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }


        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/PayRollAttendance/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }
    }

    //MisMatchShif

    if (ReportNames == "MISMATCH SHIFT REPORT - DAY WISE") {

        var isAllValid = true;

        //if ($('#ddlShift').val() == '0') {

        //    document.getElementById("ShiftTypeerror").innerHTML = "Select the SHift";

        //    isAllValid = false;
        //}
        //else {

        //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
        //}

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }


        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/MisMatchShifDayWise/?FromDate=' + FromDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }
    }

    //DayAttendanceDayWiseOT

    if (ReportNames == "DAY ATTENDANCE WITH OT HOURS") {


        var isAllValid = true;

        //if ($('#ddlShift').val() == '0') {

        //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

        //    isAllValid = false;
        //}
        //else {

        //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
        //}

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/DayAttendanceDayWiseWithOT/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }

    }
    //EmployeeWiseDayAttendBTDates


    if (ReportNames == "EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES") {


        var isAllValid = true;

        if ($('#txtEmpNo').val() == '') {

            document.getElementById("EmpNoerror").innerHTML = "Enter the EmpNo";

            isAllValid = false;
        }
        else {

            document.getElementById("EmpNoerror").innerHTML = " ";  // remove it
        }

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if ($('#txtToDate').val().trim() == '') {

            document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

            isAllValid = false;
        }
        else {

            document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/EmployeeWiseAttendBTDates/?EmpNo=' + EmpNo + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }

    }

    //OTimeBTDates

    if (ReportNames == "OT REPORT - BETWEEN DATES") {

        var isAllValid = true;

        if ($('#ddlShift').val() == '0') {

            document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            isAllValid = false;
        }
        else {

            document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
        }

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if ($('#txtToDate').val().trim() == '') {

            document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

            isAllValid = false;
        }
        else {

            document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/Report/ReportMaster',
                success: function () {
                    var url = '/Report/OTTimeBWTDates/?ShiftType=' + ShiftType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }
    }



    });

    

   
});

function ShowAndHide() {

var ReportNames = $('#ddlReportSubMenu').val();

if (ReportNames == "0") {

    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
  

    $("#ddlShift").attr("disabled", true);
    $("#ddlWagesType").attr("disabled", true);
    $("#txtFromDate").attr("disabled", true);
    $("#txtToDate").attr("disabled", true);
    $("#txtEmpNo").attr("disabled", true);

}
else if (ReportNames == "DAY ATTENDANCE - DAY WISE") {

    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');

    $("#ddlShift").attr("disabled", false);
    $("#ddlWagesType").attr("disabled", true);
    $("#txtFromDate").attr("disabled", false);
    $("#txtToDate").attr("disabled", false);
    $("#txtEmpNo").attr("disabled", true);

    $('#btnReport').css('display', 'block');
    $('#btnExcel').css('display', 'none');

}
else if (ReportNames == "DAY ATTENDANCE - DAY WISE") {



    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


    $('#DaysWiseDatatable1').css('display', 'block');
    $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


    $("#ddlShift").removeAttr("disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");
    $("#txtEmpNo").attr("disabled", "disabled");

    $('#btnReport').css('display', 'block');
    $('#btnExcel').css('display', 'none');

}

else if (ReportNames == "SALARY CONSOLIDATE REPORT") {


    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


         
       
    $("#ddlShift").attr("disabled", "disabled");

    $("#ddlEmployeeName").attr("disabled", "disabled");
    $("#ddlDept").attr("disabled", "disabled");
    $("#ddlYear").attr("disabled", "disabled");
    $("#ddlIsAct").attr("disabled", "disabled");
      
    $("#ddlCategory").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");
    $("#txtAboveHours").attr("disabled", "disabled");
    $("#txtBelowHours").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");

    $("#ddlShift").removeAttr("disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");




         

}






else if (ReportNames == "BELOW  AND ABOVE HOURS REPORT") {

    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


         
       
    $("#ddlShift").attr("disabled", "disabled");

    $("#ddlEmployeeName").attr("disabled", "disabled");
    $("#ddlDept").attr("disabled", "disabled");
    $("#ddlYear").attr("disabled", "disabled");
    $("#ddlIsAct").attr("disabled", "disabled");
      
    $("#ddlCategory").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");
    $("#txtAboveHours").removeAttr("disabled");
    $("#txtBelowHours").removeAttr("disabled");
    $("#ddlMonth").attr("disabled", "disabled");

    $("#ddlShift").removeAttr("disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");





        

}
else if (ReportNames == "ABOVE HOURS REPORT") {


    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


         
       
    $("#ddlShift").attr("disabled", "disabled");

    $("#ddlEmployeeName").attr("disabled", "disabled");
    $("#ddlDept").attr("disabled", "disabled");
    $("#ddlYear").attr("disabled", "disabled");
    $("#ddlIsAct").attr("disabled", "disabled");
      
    $("#ddlCategory").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");
    $("#txtAboveHours").removeAttr("disabled");
    $("#txtBelowHours").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");

    $("#ddlShift").removeAttr("disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");








}
else if (ReportNames == "BELOW HOURS REPORT") {



    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


         
       
    $("#ddlShift").attr("disabled", "disabled");

    $("#ddlEmployeeName").attr("disabled", "disabled");
    $("#ddlDept").attr("disabled", "disabled");
    $("#ddlYear").attr("disabled", "disabled");
    $("#ddlIsAct").attr("disabled", "disabled");
      
    $("#ddlCategory").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");
    $("#txtAboveHours").attr("disabled", "disabled");
    $("#txtBelowHours").removeAttr("disabled");
    $("#ddlMonth").attr("disabled", "disabled");

    $("#ddlShift").removeAttr("disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");




          

}
else if (ReportNames == "ABSENT REPORT DAY WISE") {



    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


         
       
    $("#ddlShift").attr("disabled", "disabled");

    $("#ddlEmployeeName").attr("disabled", "disabled");
    $("#ddlDept").attr("disabled", "disabled");
    $("#ddlYear").attr("disabled", "disabled");
    $("#ddlIsAct").attr("disabled", "disabled");
      
    $("#ddlCategory").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");
    $("#txtAboveHours").attr("disabled", "disabled");
    $("#txtBelowHours").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");






}
else if (ReportNames == "DAY ATTENDANCE SUMMARY") {



    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


         
       
    $("#ddlShift").attr("disabled", "disabled");

    $("#ddlEmployeeName").attr("disabled", "disabled");
    $("#ddlDept").attr("disabled", "disabled");
    $("#ddlYear").attr("disabled", "disabled");
    $("#ddlIsAct").attr("disabled", "disabled");
      
    $("#ddlCategory").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");
    $("#txtAboveHours").attr("disabled", "disabled");
    $("#txtBelowHours").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");  

  


          
     
}
        
else if (ReportNames == "PAYROLL OT") {



    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


         
       
    $("#ddlShift").attr("disabled", "disabled");

    $("#ddlEmployeeName").attr("disabled", "disabled");
    $("#ddlDept").attr("disabled", "disabled");
    $("#ddlYear").attr("disabled", "disabled");
    $("#ddlIsAct").attr("disabled", "disabled");
      
    $("#ddlCategory").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");
    $("#txtAboveHours").attr("disabled", "disabled");
    $("#txtBelowHours").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").removeAttr("disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").removeAttr("disabled");



}




else if (ReportNames == "ABSENT REPORT - BETWEEN DATES") {





    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


         
       
    $("#ddlShift").attr("disabled", "disabled");

    $("#ddlEmployeeName").attr("disabled", "disabled");
    $("#ddlDept").attr("disabled", "disabled");
    $("#ddlYear").attr("disabled", "disabled");
    $("#ddlIsAct").attr("disabled", "disabled");
      
    $("#ddlCategory").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");
    $("#txtAboveHours").attr("disabled", "disabled");
    $("#txtBelowHours").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").removeAttr("disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").removeAttr("disabled");


  



}




else if (ReportNames == "HOURS ATTENDANCE - BETWEEN DATES") {




    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');


         
       
    $("#ddlShift").attr("disabled", "disabled");

    $("#ddlEmployeeName").attr("disabled", "disabled");
    $("#ddlDept").attr("disabled", "disabled");
    $("#ddlYear").attr("disabled", "disabled");
    $("#ddlIsAct").attr("disabled", "disabled");
      
    $("#ddlCategory").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");
    $("#txtAboveHours").attr("disabled", "disabled");
    $("#txtBelowHours").attr("disabled", "disabled");
    $("#ddlMonth").attr("disabled", "disabled");

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").removeAttr("disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").removeAttr("disabled");




}

else if (ReportNames == "DAY ATTENDANCE SUMMARY") {

    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");
    //$("#txtLotNo").attr("disabled", "disabled");
    //$("#txtBatchCount").attr("disabled", "disabled");
    //$("#txtArrivalNo").attr("disabled", "disabled");
    $("#txtEmpNo").attr("disabled", "disabled");

}

else if (ReportNames == "PAYROLL ATTENDANCE") {

    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").removeAttr("disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").removeAttr("disabled");
    //$("#txtLotNo").attr("disabled", "disabled");
    //$("#txtBatchCount").attr("disabled", "disabled");
    //$("#txtArrivalNo").attr("disabled", "disabled");
    $("#txtEmpNo").attr("disabled", "disabled");

}

else if (ReportNames == "MISMATCH SHIFT REPORT - DAY WISE") {

    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");
    //$("#txtLotNo").attr("disabled", "disabled");
    //$("#txtBatchCount").attr("disabled", "disabled");
    //$("#txtArrivalNo").attr("disabled", "disabled");
    $("#txtEmpNo").attr("disabled", "disabled");

}


else if (ReportNames == "DAY ATTENDANCE WITH OT HOURS") {

    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');

    $("#ddlShift").removeAttr("disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").attr("disabled", "disabled");
    //$("#txtLotNo").attr("disabled", "disabled");
    //$("#txtBatchCount").attr("disabled", "disabled");
    //$("#txtArrivalNo").attr("disabled", "disabled");
    $("#txtEmpNo").attr("disabled", "disabled");

}

else if (ReportNames == "EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES") {

    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").attr("disabled", "disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").removeAttr("disabled");
    //$("#txtLotNo").attr("disabled", "disabled");
    //$("#txtBatchCount").attr("disabled", "disabled");
    //$("#txtArrivalNo").attr("disabled", "disabled");
    $("#txtEmpNo").removeAttr("disabled");

}

else if (ReportNames == "OT REPORT - BETWEEN DATES") {

    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $("#txtEmpNo").val(' ');

    $("#ddlShift").attr("disabled", "disabled");
    //$("#ddlLocSite").attr("disabled", "disabled");
    $("#ddlWagesType").removeAttr("disabled");
    $("#txtFromDate").removeAttr("disabled");
    $("#txtToDate").removeAttr("disabled");
    //$("#txtLotNo").attr("disabled", "disabled");
    //$("#txtBatchCount").attr("disabled", "disabled");
    //$("#txtArrivalNo").attr("disabled", "disabled");
    $("#txtEmpNo").attr("disabled", "disabled");

}




}


