﻿var Company = []

function LoadCompany(element) {
    if (Company.length == 0) {
        //ajax function for fetch data
        $.ajax({
            type: "GET",
            url: '/Home/GetCompany',
            success: function (data) {
                Company = data;
                //render department
                renderCompany(element);
            }
        })
    }
    else {
        //render department to the element
        renderCompany(element);
    }
}

function renderCompany(element) {
    var $ele = $(element);
    $ele.empty();
    $ele.append($('<option></option>').val('0').text('Company Name'));
    //console.log(Department);
    //alert(Department.length);
    $.each(Company, function (i, val) {
        //alert(i);
        //console.log(Department);
        $ele.append($('<option></option>').val(val.CompCode).text(val.CompName));
    })
}


function LoadLocation() {
    var selectedValue = $("#ddlCompany").val();
    if (selectedValue != 0) {
        //$("#ddlCompany").parent().next(".validation").remove(); // remove it
        document.getElementById("ddlComperrorname").innerHTML = " ";
        //ajax function for fetch data
        $.ajax({
            type: "GET",
            url: '/Home/GetLocation?Ccode=' + selectedValue + '',
            success: function (data) {
                $("#ddlLocation").empty();
                $("#ddlLocation").append($('<option></option>').val('0').text('Location Name'));
                $.each(data, function (i, val) {
                    //alert(i);
                    //console.log(Department);
                    $("#ddlLocation").append($('<option></option>').val(val.LocCode).text(val.LocName));
                })

            }
        })
    }
    else {
        //alert("Select company");
        $("#ddlCompany").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select company</div>");
    }
}


function validateUsername(Username) {
    if (Username == "") {
        document.getElementById("Unameerror").innerHTML = "Enter the UserName";
        //alert("Invalid email address");
        return false;
    }
    else {
        document.getElementById("Unameerror").innerHTML = " ";
        return true;
    }
}

function validatePassword(Password) {
    if (Password == "") {
        document.getElementById("Passworderror").innerHTML = "Enter the Password";
        //alert("Invalid email address");
        return false;
    }
    else {
        document.getElementById("Passworderror").innerHTML = " ";
        return true;
    }
}


$(document).ready(function () {
    LoadCompany($('#ddlCompany'));

    $('#btnLogin').click(function () {
        //alert('HI');
        //document.getElementById("wrong").style.visibility = "hidden";
        var isAllValid = true;
        if ($('#ddlCompany').val().trim() == "0") {
            document.getElementById("ddlComperrorname").innerHTML = "Please select Company";

            isAllValid = false;
        }
        else {

            document.getElementById("ddlComperrorname").innerHTML = " ";
        }
        if ($('#ddlLocation').val().trim() == "0") {
            document.getElementById("ddlLocationerrorname").innerHTML = "Please select Location";
            isAllValid = false;
        }
        else {
            document.getElementById("ddlLocationerrorname").innerHTML = " ";
        }

        if (isAllValid) {

            var Username = $('#txtUname').val().trim()
            var PWD = $('#txtPassword').val().trim()
            var Ccode = $('#ddlCompany').val().trim()
            var Lcode = $('#ddlLocation').val().trim()

            $.ajax({
                url: '/Home/LoginUser/?Username=' + Username + '&PWD=' + PWD + '&Ccode=' + Ccode + '&Lcode=' + Lcode + '',
                type: "POST",
                //data: JSON.stringify(EmpNo),
                //dataType: "JSON",
                //contentType: "application/json",
                success: function (d) {
                console.log(d);
                    if (d[0].Name == "True") {
                        //alert('HI');
                        var url = d[0].ID;

                        //alert(url);

                        if (url == null) {
                            var url1 = "/Home/PayrollDashboard";
                            window.location.href = url1;
                        }
                        else {
                            //alert(url);
                            //window.location.href = url;
                            var url1 = "/Home/PayrollDashboard";
                            window.location.href = url1;
                        }

                    }

                    else {
                        //alert('Check with username password');
                        $('#txtUname').val('');
                        $('#txtPassword').val('');
                        //document.getElementById("wrong").style.visibility = "visible";
                    }
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });
        }

    });

    $("#txtPassword").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#btnLogin").click();
        }
    });
});
