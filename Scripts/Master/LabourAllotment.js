$(function () {

    //alert('Test')
    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });

    $('.select2').select2();
    $('#example1').DataTable();
    $('#TimeInTable').DataTable();
    $('#TimeOutTable').DataTable();

    $('#OTDataTable').DataTable();
    $('#AllLeaveTable').DataTable();
    $('#AttendDataTable').DataTable();
    $('#LeaveTable').DataTable();

});
function LoadDesignation() {
    //alert('Dept');

    
        //alert(DeptID);
        $.ajax({
            type: "GET",
            url: '/Master/GetDesignation',
            success: function (data) {
                //console.log(data);
                //Employee = data;
                $('#ddlDesignation').empty();
                $("#ddlDesignation").append($('<option></option>').val('0').text('--Select Designation--'));
                $.each(data, function (i, val) {

                    $("#ddlDesignation").append($('<option></option>').val(val.ID).text(val.Name));

                })
            }
        })
    
}


function LoadWorkerCode() {
    //alert('Dept');
    var DesignID = $('#ddlDesignation').val();
    if (DesignID != '0')
    {
    $.ajax({
        type: "GET",
        url: '/Master/GetWorkerCode/?DesignID=' + DesignID + '',
        success: function (data) {
            $("#txtWorkercode").val(data);
        }
    })
    }
    else
    {
        $("#txtWorkercode").val('');
    }

}

function sum() {

    document.getElementById('txtTotal').value = parseFloat(document.getElementById('txtGeneral').value) + parseFloat(document.getElementById('txtSHIFT1').value) + parseFloat(document.getElementById('txtSHIFT2').value) + parseFloat(document.getElementById('txtSHIFT3').value);
}

 function clear() {
     $("#ddlDesignation").val('0').change();
     $("#txtWorkercode").val('');
     $("#txtGeneral").val('0');
     $("#txtSHIFT1").val('0');
     $("#txtSHIFT2").val('0');
     $("#txtSHIFT3").val('0');
     $("#txtTotal").val('0');
     document.getElementById("Designationerror").innerHTML = " ";
 }

 $(document).ready(function () {
     LoadLabourAllotmentDetails();
    LoadDesignation();
    LoadLabourAllotmentDetails();
    LoadEmployeeType();
 });

 function LoadEmployeeType() {
     //alert('Dept');

     $.ajax({
         type: "GET",
         url: '/Employee/GetEmployeeType',
         success: function (data) {
             //console.log(data);
             //Employee = data;
             $('#ddlEmpType').empty();
             $("#ddlEmpType").append($('<option></option>').val('0').text('--Select Employee Type--'));
             $.each(data, function (i, val) {

                 $("#ddlEmpType").append($('<option></option>').val(val.ID).text(val.Name));

             })
         }
     })
 }

 $('#btnAdd').click(function () {
     
     $('#LabourMain').css('display', 'none');

     $('#LabourSub').css('display', 'block');

 });

$('#BtnSave').click(function () {

    //alert('hi');
    //validation 
    var isAllValid = true;

    if ($('#ddlDesignation').val() == '0') {
        document.getElementById("Designationerror").innerHTML = "Select the Designation";
        isAllValid = false;
    }
    else {

        document.getElementById("Designationerror").innerHTML = " "; // remove it
    }


    //Save if valid
    if (isAllValid) {
        var RoomDet = {
            ID: $('#ddlDesignation').val(),
            Name: $('#txtWorkercode').val().trim(),
            Category: $('#txtGeneral').val().trim(),
            Shift: $('#txtSHIFT1').val().trim(),
            SubCatName: $('#txtSHIFT2').val().trim(),
            MachineID: $('#txtSHIFT3').val().trim(),
            LastName: $('#txtTotal').val().trim(),

        }
        console.log(RoomDet);
        $.ajax({
            url: "/Master/InsertLabourAllotment",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    LoadDesignation();
                    LoadLabourAllotmentDetails();
                    clear();
                    alert('Inserted Successfully..');
                    $('#LabourSub').css('display', 'none');
                    $('#LabourMain').css('display', 'block');

                }
                else if (d == "Update") {
                    LoadDesignation();
                    LoadLabourAllotmentDetails();
                    clear();
                    alert('Update Successfully...');
                    $('#LabourSub').css('display', 'none');
                    $('#LabourMain').css('display', 'block');

                }
                
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }


});

$('#BtnClear').click(function () {
    clear();
});

function LoadLabourAllotmentDetails() {

    $.ajax({
        type: "GET",
        url: '/Master/GetLabourAllotment',
        success: function (data) {
            var data1 = $('#example8').DataTable();
            data1.clear();
            if (data.length != 0)
            {
                $.each(data, function (key, item) {
                    data1.row.add([
                    item.Name,
                    item.Category,
                    item.Shift,
                    item.SubCatName,
                    item.MachineID,
                    item.LastName,
                    item.DeptName,
                   "<button class='btn btn-success' type='button' onClick='EditLabourAllotment(\"" + item.ID + "\")'><i class='fa fa-pencil'></i></button> <button class='btn btn-danger' type='button' onClick='DeleteLabourAllotment(\"" + item.ID + "\")'><i class='fa fa-times-circle'></i></button>"
                    ]).draw();
                });
            }
            else
            {
                var data1 = $('#example8').DataTable();
                data1.clear();
            }
           
        }
    });
}

function EditLabourAllotment(index) {

    $.ajax({
        type: "GET",
        url: '/Master/GetEditLabourAllotment/?ID=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;


                $('#LabourMain').css('display', 'none');

                $('#LabourSub').css('display', 'block');

                $("#ddlDesignation").val(item.ID).change();
                $("#txtWorkercode").val(item.Category);
                $("#txtGeneral").val(item.Shift);
                $("#txtSHIFT1").val(item.SubCatName);
                $("#txtSHIFT2").val(item.MachineID);
                $("#txtSHIFT3").val(item.LastName);
                $("#txtTotal").val(item.DeptName);


            });

        }
    });

}

function DeleteLabourAllotment(index) {

    $.ajax({
        type: "GET",
        url: '/Master/GetDeleteLabourAllotment/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadDesignation();
                LoadLabourAllotmentDetails();
                clear();
                
                $('#LabourSub').css('display', 'none');
                $('#LabourMain').css('display', 'block');
                alert('Deleted Successfully..');
                
            }
            else if (data == "No Data") {
                LoadDesignation();
                LoadLabourAllotmentDetails();
                clear();

                $('#LabourSub').css('display', 'none');
                $('#LabourMain').css('display', 'block');
                alert('No Data...');
                

            }
        }
    });
}