﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AttendanceMVC
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        //void Session_Start(object sender, EventArgs e)
        //{
        //    HttpContext.Current.Session.Add("__MyAppSession", string.Empty);
        //}

        protected void Session_Start(Object sender, EventArgs e)
        {
            string culture = "";
            HttpContext current = HttpContext.Current;
            if (culture == "" && current.Request.Cookies["locale"] != null)
                culture = current.Request.Cookies["locale"].Value;
            else
                //culture = Configuration.Settings.SiteLanguage;

            current.Session["locale"] = culture;
            //Utility.setUsers("0", "0", "public");  

            try
            {
                //ScholarDBUtilities.addSessionCount();
            }
            catch (Exception ex)
            {
            }
        }

    }
}
