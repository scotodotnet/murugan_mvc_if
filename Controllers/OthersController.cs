﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI;
using System.Globalization;
using System.Data.OleDb;
using System.IO;

namespace AttendanceMVC.Controllers
{
    public class OthersController : Controller
    {
         string Ccode = "";
         string Lcode = "";
         string SessionCcode = "";
         string SessionLcode = "";
         string SessionUserName;
         string SessionUserID;

        string SSQL = "";
        DateTime date1;
        DateTime date2;
        DateTime Date2 = new DateTime();

        string PartyName;
        DateTime frmDate;
        DateTime toDate;
        string Machine_ID_Encrypt;
        static bool isUK = false;
        string mIpAddress_IN;
        string mIpAddress_OUT;
        string ESI_Total_Amt = "0";
        string PF_Total_Amt = "0";
        static string StdWork = "0";
        static string Temp_Con_Days = "";
        static string Temp_Con_OT = "";
        static string Temp_Spl_Days = "";
        static string Temp_Spl_OT = "";
        static string EmpNo = "", ExisistingCode = "", MachineNo = "", Days = "", Months = "", FinancialYear = "";
        static string TotalDays = "", NFh = "", weekoff = "", OTHoursNew = "", Fixed_Work_Days = "", NFH_Work_Days_Statutory = "", WH_Work_Present = "", Total_Work = "0";
        static string BasicSalary = "", Allowance1amt = "", Allowance2amt = "", Deduction1amt = "", Deduction2amt = "", ConvenyAmt = "", SplAmt = "";
        static string ExistingCode = "", MachineID = "", EligibleCateen = "", EligibleIncentive = "", EligibleSnacks = "", Eligible_PF = "", Eligible_ESI = "";
        static string PF_per = "", ESI_per = "", StaffSalary = "", EmployeerPFone = "", EmployeerPFTwo = "", EmployeerESI = "";
        string basic1 = "0.00";
        string HRA1 = "0.00";
        string SplAllow1 = "0.00";
        static string basic = "0";
        static string Advance = "0";
        static string pfAmt = "0";
        static string ESI = "0";
        static string stamp = "0";
        static string Union = "0";
        static string TotalEarnings = "0";
        static string TotalDeductions = "0";
        static string NetPay = "0";
        static string Words = "";
        static string PFS = "0.00";
        static string ExistNo = "";
        static string PF_salary;
        static string Employer_PF = "0.00";
        static string Employer_ESI = "0.00";
        static string ded3 = "0";
        static string ded4 = "0";
        static string ded5 = "0";
        static string All3 = "0";
        static string All4 = "0";
        static string All5 = "0";
        static string CanteenDays = "0";
        static string HRA_Str = "0";
        static string Spl_Allow_Str = "0";
        static string OTSal = "0";

        static string All1 = "0";
        static string All2 = "0";
        static string Ded1 = "0";
        static string ded2 = "0";
        static string DayIncentive = "0";




        // GET: Others
        public ActionResult AttendanceUpload()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult SalaryUpload()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();

            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult AttendanceUpload(string FromDate, string ToDate, string WagesType, string FinYear, string Months, HttpPostedFileBase file)
        {
            bool ErrFlag = false;
            string message = "";
            DataTable SHift = new DataTable();
            string Year = "";
            string Month = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT_Check = new DataTable();

            List<ManualAttend> empList = new List<ManualAttend>();

            if (FromDate == "")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Enter the FromDate')</script>");

            }
            else if (ToDate == "")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Enter the ToDate')</script>");

            }
            else if (WagesType == "0")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Select the WagesType')</script>");

            }
            else if (FinYear == "0")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Select the Financial Year')</script>");

            }
            else if (Months == "0")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Select the Month')</script>");

            }
            if (!ErrFlag)
            {
                date1 = Convert.ToDateTime(FromDate);
                Date2 = Convert.ToDateTime(ToDate);
                int daycount = (int)(((Date2 - date1).TotalDays) + 1);
                //string Mn = Convert.ToDateTime(FromDate).ToString("MMMM");
                //string yy = Convert.ToDateTime(FromDate).Year.ToString();

                string Mn = Months.ToString();
                string yy = FinYear.ToString();

                DataTable dt = new DataTable();
                DataTable DT_Wages = new DataTable();
                string Wages = "0";
                DataSet ds = new DataSet();


                //return Content("<script language='javascript' type='text/javascript'>alert     ('Requested Successfully ');</script>");
                string excelConnectionString = "";

                if (Request.Files["file"].ContentLength > 0)
                {
                    string fileExtension =
                                        System.IO.Path.GetExtension(Request.Files["file"].FileName);
                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        string fileLocation = Path.GetFileName(Request.Files["file"].FileName);
                        string filename = fileLocation;
                        string targetpath = Server.MapPath("~/Upload/");
                        file.SaveAs(targetpath + filename);
                        string pathToExcelFile = targetpath + filename;
                        var connectionString = "";
                        if (fileExtension == ".xls")
                        {
                            connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                        }
                        //connection String for xlsx file format.
                        else if (fileExtension == ".xlsx")
                        {
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=Excel 12.0;";
                        }
                        //Create Connection to Excel work book and add oledb namespace
                        OleDbConnection excelConnection = new OleDbConnection(connectionString);
                        excelConnection.Open();
                        //DataTable dt = new DataTable();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", excelConnection);
                        excelConnection.Close();

                        using (OleDbCommand cmd = excelConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            excelConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;


                        objDataAdapter.Fill(ds);

                        dt = ds.Tables[0];



                        for (int i = 0; i < dt.Rows.Count; i++)
                        {


                            //string FD = (Convert.ToDecimal(daycount) - Convert.ToDecimal(DT.Rows[i]["Wh_Count"].ToString())).ToString();

                            string Department = dt.Rows[i][3].ToString();
                            string DeptCode = "0";
                            DataTable DT_Dept = new DataTable();
                            SSQL = "Select *from Department_Mst"; //where CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                            SSQL = SSQL + " where DeptName='" + Department.Trim() + "'";
                            DT_Dept = ManualAttend.GetDetails(SSQL);

                            if (DT_Dept.Rows.Count != 0)
                            {
                                DeptCode = DT_Dept.Rows[0]["DeptCode"].ToString();
                            }

                            string MachineID = dt.Rows[i][0].ToString();
                            string EmpNo = dt.Rows[i][0].ToString();
                            string ExistingCode = dt.Rows[i][1].ToString();
                            string FirstName = dt.Rows[i][2].ToString();
                            string WorkingDays = dt.Rows[i][4].ToString();
                            string CL = dt.Rows[i][5].ToString();
                            string TotDays = dt.Rows[i][16].ToString();
                            //string AbsentDays = dt.Rows[i][7].ToString();
                            string weekoff = dt.Rows[i][7].ToString();
                            string NF_Count = dt.Rows[i][9].ToString();
                            string NFH_Present = dt.Rows[i][10].ToString();
                            string WH_Count = dt.Rows[i][7].ToString();
                            string WH_Present = dt.Rows[i][8].ToString();
                            string OT_Hours = dt.Rows[i][5].ToString();
                            string FD = dt.Rows[i][13].ToString();
                            string Canteen = dt.Rows[i][12].ToString();

                            int j = 0;
                            if (Department == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Department. The Row Number is " + j + "')</script>");
                            }
                            else if (MachineID == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the MachineID. The Row Number is " + j + "')</script>");
                            }
                            else if (ExistingCode == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the ExistingCode. The Row Number is " + j + "')</script>");
                            }
                            else if (WorkingDays == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Days. The Row Number is " + j + "')</script>");
                            }
                            else if (OT_Hours == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the OT Hours. The Row Number is " + j + "')</script>");
                            }
                            else if (WH_Count == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Week Off. The Row Number is " + j + "')</script>");
                            }
                            else if (NF_Count == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the NFH Count. The Row Number is " + j + "')</script>");
                            }
                            else if (NFH_Present == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the NFH Present. The Row Number is " + j + "')</script>");
                            }
                            else if (WH_Present == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Week Off Present. The Row Number is " + j + "')</script>");

                            }

                            else if (FD == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Fixed Working Days. The Row Number is " + j + "')</script>");
                            }
                            else if (Canteen == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Canteen. The Row Number is " + j + "')</script>");
                            }


                            if (!ErrFlag)
                            {
                                DataTable DT_Check1 = new DataTable();
                                SSQL = "Select *from AttenanceDetails where EmpNo='" + EmpNo + "' And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                                SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                                SSQL = SSQL + " And Months='" + Mn + "'";
                                SSQL = SSQL + " And Ccode='" + Session["SessionCcode"].ToString() + "' And Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                DT_Check1 = ManualAttend.GetDetails(SSQL);
                                if (DT_Check1.Rows.Count != 0)
                                {
                                    SSQL = "Delete from AttenanceDetails where EmpNo='" + EmpNo + "' And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                                    SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                                    SSQL = SSQL + " And Months='" + Mn + "'";
                                    SSQL = SSQL + " And Ccode='" + Session["SessionCcode"].ToString() + "' And Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                    ManualAttend.GetDetails(SSQL);
                                }


                                SSQL = "insert into AttenanceDetails(DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,TotalDays,NFh,WorkingDays,CL,AbsentDays,";
                                SSQL = SSQL + "weekoff,FromDate,ToDate,Modeval,home,halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,";
                                SSQL = SSQL + "NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,Day_Shift,Night_Shift,WH_Work_Present,OTDays,";
                                SSQL = SSQL + "Ccode,Lcode)values";
                                SSQL = SSQL + "('" + DeptCode + "','" + EmpNo + "','" + ExistingCode + "',";
                                SSQL = SSQL + "'" + WorkingDays + "','" + Mn + "','" + yy + "',GetDate(),'" + TotDays.ToString() + "','" + NF_Count.ToString() + "',";
                                SSQL = SSQL + "'" + TotDays.ToString() + "','0.0','0.0','" + WH_Count + "','" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "','" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',";
                                SSQL = SSQL + "'0','0.0','0.0','0.0','" + Canteen + "','" + OT_Hours + "','" + FD + "','" + WH_Present.ToString() + "','" + NFH_Present.ToString() + "',";
                                SSQL = SSQL + "'0.0','0.0','0.0','0.0','0.0','0.0',";
                                SSQL = SSQL + "'" + Session["SessionCcode"].ToString() + "','" + Request.Cookies["SessionLcode"].Value.ToString() + "')";
                                ManualAttend.GetDetails(SSQL);
                            }
                            else
                            {
                                excelConnection.Close();
                                return View("AttendanceUpload");
                            }
                            // Status = "Success";
                        }

                        excelConnection.Close();
                    }

                    Response.Write("<script>alert('Attendance Successfully Uploaded')</script>");

                    return View("AttendanceUpload");


                }
                else
                {
                    Response.Write("<script>alert('Please Upload the file correctly')</script>");
                    return View("AttendanceUpload");
                }
                //Response.co
                //return View();
            }
            else
            {
                return View("AttendanceUpload");
            }


        }

        [HttpPost]
        public ActionResult SalaryUpload(string FromDate, string ToDate, string WagesType, string FinYear, string Months, HttpPostedFileBase file)
        {
            bool ErrFlag = false;
            string message = "";
            DataTable SHift = new DataTable();
            string Year = "";
            string Month = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT_Check = new DataTable();
            if (FromDate == "")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Enter the FromDate')</script>");

            }
            else if (ToDate == "")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Enter the ToDate')</script>");

            }
            else if (WagesType == "0")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Select the WagesType')</script>");

            }
            else if (FinYear == "0")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Select the Financial Year')</script>");

            }
            else if (Months == "0")
            {
                ErrFlag = true;
                Response.Write("<script>alert('Select the Month')</script>");

            }

            if (!ErrFlag)
            {
                List<ManualAttend> empList = new List<ManualAttend>();
                date1 = Convert.ToDateTime(FromDate);
                Date2 = Convert.ToDateTime(ToDate);
                int daycount = (int)(((Date2 - date1).TotalDays) + 1);
                //string Mn = Convert.ToDateTime(FromDate).ToString("MMMM");
                //string yy = Convert.ToDateTime(FromDate).Year.ToString();

                string Mn = Months.ToString();
                string yy = FinYear.ToString();

                DataTable dt = new DataTable();
                DataTable DT_Wages = new DataTable();
                string Wages = "0";

                DataSet ds = new DataSet();


                DataTable dt_Attend = new DataTable();
                DataTable dt_Employee = new DataTable();
                DataTable dtpf = new DataTable();
                DataTable dt_Deduction = new DataTable();
                string Status = "";
                string Query = "";
                string SalaryThro = "";
                string BankName = "";
                string BranchCode = "";
                string AccountNo = "";
                string CanteenDays = "";
                string OTSal = "0";
                string LabourAmt = "";
                string CanteenAmt = "";
                string EligibleCateen = "";
                string OTEligible = "";
                string BasicDA = "0";
                string Conveyance = "0";
                string WashingAllow = "0";

                //Get PF ESI Details
                Query = "select * from MstESIPF where Ccode='" + Session["SessionCcode"].ToString() + "' and Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                dtpf = ManualAttend.GetDetails(Query);
                if (dtpf.Rows.Count > 0)
                {
                    PF_per = dtpf.Rows[0]["PF_per"].ToString();
                    ESI_per = dtpf.Rows[0]["ESI_per"].ToString();
                    StaffSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                    EmployeerPFone = dtpf.Rows[0]["EmployeerPFone"].ToString();
                    EmployeerPFTwo = dtpf.Rows[0]["EmployeerPFTwo"].ToString();
                    EmployeerESI = dtpf.Rows[0]["EmployeerESI"].ToString();
                }
                else
                {
                    PF_per = "0"; ESI_per = "0"; StaffSalary = "0"; EmployeerPFone = "0"; EmployeerPFTwo = "0"; EmployeerESI = "0";
                }

                //return Content("<script language='javascript' type='text/javascript'>alert     ('Requested Successfully ');</script>");
                string excelConnectionString = "";

                if (Request.Files["file"].ContentLength > 0)
                {
                    string fileExtension =
                                        System.IO.Path.GetExtension(Request.Files["file"].FileName);
                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        string fileLocation = Path.GetFileName(Request.Files["file"].FileName);
                        string filename = fileLocation;
                        string targetpath = Server.MapPath("~/Upload/");
                        file.SaveAs(targetpath + filename);
                        string pathToExcelFile = targetpath + filename;
                        var connectionString = "";
                        if (fileExtension == ".xls")
                        {
                            connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                        }
                        //connection String for xlsx file format.
                        else if (fileExtension == ".xlsx")
                        {
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=Excel 12.0;";
                        }
                        //Create Connection to Excel work book and add oledb namespace
                        OleDbConnection excelConnection = new OleDbConnection(connectionString);
                        excelConnection.Open();
                        //DataTable dt = new DataTable();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", excelConnection);
                        excelConnection.Close();

                        using (OleDbCommand cmd = excelConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            excelConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;


                        objDataAdapter.Fill(ds);

                        dt = ds.Tables[0];



                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            EmpNo = dt.Rows[i][0].ToString();
                            All3 = dt.Rows[i][4].ToString();
                            All4 = dt.Rows[i][5].ToString();
                            All5 = dt.Rows[i][6].ToString();
                            ded3 = dt.Rows[i][7].ToString();
                            ded4 = dt.Rows[i][8].ToString();
                            ded5 = dt.Rows[i][9].ToString();
                            Advance = dt.Rows[i][10].ToString();


                            int j = 0;
                            if (EmpNo == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Employee Number. The Row Number is " + j + "')</script>");
                            }
                            else if (All3 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Allowance3. The Row Number is " + j + "')</script>");
                            }
                            else if (All4 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Allowance4. The Row Number is " + j + "')</script>");
                            }
                            else if (All5 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Allowance5. The Row Number is " + j + "')</script>");
                            }
                            else if (ded3 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Deduction3. The Row Number is " + j + "')</script>");
                            }
                            else if (ded4 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Deduction4. The Row Number is " + j + "')</script>");
                            }
                            else if (ded5 == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Deduction5. The Row Number is " + j + "')</script>");
                            }
                            else if (Advance == "")
                            {
                                ErrFlag = true;
                                j = i + 2;
                                Response.Write("<script>alert('Enter the Advance. The Row Number is " + j + "')</script>");
                            }


                            if (!ErrFlag)
                            {
                                //Get Attendance
                                SSQL = "Select * from AttenanceDetails where convert(datetime,FromDate,103)=convert(datetime,'" + FromDate + "',103) and convert(datetime,ToDate,103)=convert(datetime,'" + ToDate + "',103) ";
                                SSQL = SSQL + " And EmpNo='" + EmpNo + "'";
                                SSQL = SSQL + "and Ccode='" + Session["SessionCcode"].ToString() + "' and Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                dt_Attend = ManualAttend.GetDetails(SSQL);
                                if (dt_Attend.Rows.Count > 0)
                                {
                                    // Get Employee Profile
                                    Query = "select ExistingCode,MachineID,SalaryThro,BankName,BranchCode,AccountNo,EligibleCateen,EligibleIncentive,EligibleSnacks,Eligible_PF,";
                                    Query = Query + "Eligible_ESI,BasicSalary,Allowance1amt,Allowance2amt,Deduction1amt,Deduction2amt,ConvenyAmt,SplAmt,EligibleCateen,OTEligible,";
                                    Query = Query + "LabourAmt,OTSal,StdWrkHrs from Employee_Mst where EmpNo='" + EmpNo.ToString() + "'";
                                    Query = Query + " And CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                    dt_Employee = ManualAttend.GetDetails(Query);
                                    if (dt_Employee.Rows.Count > 0)
                                    {

                                        //Fill Attendance
                                        EmpNo = dt_Attend.Rows[0]["EmpNo"].ToString();
                                        Days = dt_Attend.Rows[0]["Days"].ToString();
                                        Months = dt_Attend.Rows[0]["Months"].ToString();
                                        FinancialYear = dt_Attend.Rows[0]["FinancialYear"].ToString();
                                        NFh = dt_Attend.Rows[0]["NFh"].ToString();
                                        NFH_Work_Days_Statutory = dt_Attend.Rows[0]["NFH_Work_Days_Statutory"].ToString();
                                        WH_Work_Present = dt_Attend.Rows[0]["WH_Work_Present"].ToString();
                                        weekoff = dt_Attend.Rows[0]["weekoff"].ToString();
                                        Fixed_Work_Days = dt_Attend.Rows[0]["Fixed_Work_Days"].ToString();
                                        OTHoursNew = dt_Attend.Rows[0]["OTHoursNew"].ToString();
                                        Total_Work = dt_Attend.Rows[0]["TotalDays"].ToString();
                                        CanteenDays = dt_Attend.Rows[0]["ThreeSided"].ToString();
                                        

                                        //Fill Profile
                                        ExisistingCode = dt_Employee.Rows[0]["ExistingCode"].ToString();
                                        MachineID = dt_Employee.Rows[0]["MachineID"].ToString();
                                        EligibleCateen = dt_Employee.Rows[0]["EligibleCateen"].ToString();
                                        EligibleIncentive = dt_Employee.Rows[0]["EligibleIncentive"].ToString();
                                        EligibleSnacks = dt_Employee.Rows[0]["EligibleSnacks"].ToString();
                                        Eligible_PF = dt_Employee.Rows[0]["Eligible_PF"].ToString();
                                        Eligible_ESI = dt_Employee.Rows[0]["Eligible_ESI"].ToString();
                                        BasicSalary = dt_Employee.Rows[0]["BasicSalary"].ToString();
                                        Allowance1amt = dt_Employee.Rows[0]["Allowance1amt"].ToString();
                                        Allowance2amt = dt_Employee.Rows[0]["Allowance2amt"].ToString();
                                        Deduction1amt = dt_Employee.Rows[0]["Deduction1amt"].ToString();
                                        Deduction2amt = dt_Employee.Rows[0]["Deduction2amt"].ToString();
                                        ConvenyAmt = dt_Employee.Rows[0]["ConvenyAmt"].ToString();
                                        SplAmt = dt_Employee.Rows[0]["SplAmt"].ToString();
                                        SalaryThro = dt_Employee.Rows[0]["SalaryThro"].ToString();
                                        LabourAmt = dt_Employee.Rows[0]["LabourAmt"].ToString();
                                        OTEligible = dt_Employee.Rows[0]["OTEligible"].ToString();
                                        OTSal = dt_Employee.Rows[0]["OTSal"].ToString(); //Edit by Narmatha
                                        StdWork = dt_Employee.Rows[0]["StdWrkHrs"].ToString();

                                        if (EmpNo.Trim() == "103013")
                                        {
                                            string Val = "0";
                                        }

                                        if (WagesType == "1")
                                        {
                                            // Staff Salary //
                                            //Basic Calc
                                            basic1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(Days))).ToString();
                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                            string OTAmt = "";
                                            if (OTEligible.ToUpper() == "YES")
                                            {
                                                if (OTHoursNew != "0")
                                                {
                                                    string HrSal = (Convert.ToDecimal(basic1) / Convert.ToDecimal(8)).ToString();
                                                    //HrSal = (Math.Round(Convert.ToDecimal(HrSal), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    OTAmt = ((Convert.ToDecimal(OTHoursNew)) * (Convert.ToDecimal(HrSal))).ToString();
                                                    OTAmt = (Math.Round(Convert.ToDecimal(OTAmt), 0, MidpointRounding.AwayFromZero)).ToString();

                                                }
                                                else
                                                {
                                                    OTAmt = "0";
                                                }
                                            }
                                            else
                                            {
                                                OTAmt = "0";
                                            }

                                            string Basic_Per = "0";
                                            string HRA_Per = "0";
                                            string Convey_Per = "0";
                                            string Washing_Per = "0";
                                            string Other_Per = "0";

                                            DataTable DT_Sal = new DataTable();
                                            SSQL = "Select *from SalaryDetails_Mst where CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                            SSQL = SSQL + " And Emp_Type='1'";
                                            DT_Sal = ManualAttend.GetDetails(SSQL);

                                            if (DT_Sal.Rows.Count != 0)
                                            {
                                                Basic_Per = DT_Sal.Rows[0]["Basic_Percentage"].ToString();
                                                HRA_Per = DT_Sal.Rows[0]["HRA_Percentage"].ToString();
                                                Convey_Per = DT_Sal.Rows[0]["Convey_Percentage"].ToString();
                                                Washing_Per = DT_Sal.Rows[0]["Washing_Percentage"].ToString();
                                                Other_Per = DT_Sal.Rows[0]["Other_Percentage"].ToString();
                                            }
                                            else
                                            {
                                                Basic_Per = "0";
                                                HRA_Per = "0";
                                                Convey_Per = "0";
                                                Washing_Per = "0";
                                                Other_Per = "0";
                                            }


                                            PF_Total_Amt = (Convert.ToDecimal(basic) + Convert.ToDecimal(OTAmt)).ToString();
                                            BasicDA = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Basic_Per)) / 100).ToString();
                                            Conveyance = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Convey_Per)) / 100).ToString();
                                            WashingAllow = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Washing_Per)) / 100).ToString();

                                            //HRA Calculate
                                            //HRA1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(26)).ToString();
                                            //HRA_Str = (Convert.ToDecimal(HRA1) * (Convert.ToDecimal(Days))).ToString();
                                            HRA1 = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(HRA_Per)) / 100).ToString();
                                            HRA_Str = HRA1;
                                            HRA_Str = (Math.Round(Convert.ToDecimal(HRA_Str), 0, MidpointRounding.AwayFromZero)).ToString();


                                            ESI_Total_Amt = (Convert.ToDecimal(BasicDA) + Convert.ToDecimal(HRA_Str)).ToString();

                                            //Spl. Allowence Calculate
                                            //SplAllow1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(26)).ToString();
                                            //Spl_Allow_Str = (Convert.ToDecimal(SplAllow1) * (Convert.ToDecimal(Days))).ToString();
                                            SplAllow1 = "0";
                                            Spl_Allow_Str = "0";
                                            Spl_Allow_Str = (Math.Round(Convert.ToDecimal(Spl_Allow_Str), 0, MidpointRounding.AwayFromZero)).ToString();


                                            //Get Fixed Allowance and Deduction
                                            All1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance1amt)).ToString();
                                            All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();

                                            All2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance2amt)).ToString();
                                            All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();

                                            Ded1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction1amt)).ToString();
                                            Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 0, MidpointRounding.AwayFromZero)).ToString();

                                            ded2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction2amt)).ToString();
                                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 0, MidpointRounding.AwayFromZero)).ToString();


                                            //PF Calculation
                                            if (Eligible_PF == "1")
                                            {
                                                PF_salary = "1";

                                                string PF_Check = BasicDA;

                                                //string PF_Check = ((Convert.ToDecimal(basic) * Convert.ToDecimal(50)) / 100).ToString();
                                                if (Convert.ToDecimal(PF_Check) >= Convert.ToDecimal(StaffSalary))
                                                {
                                                    PFS = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(PF_per)) / 100).ToString();
                                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    Employer_PF = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                                else
                                                {
                                                    decimal PF_val = (Convert.ToDecimal(PF_Check));
                                                    PFS = ((PF_val * Convert.ToDecimal(PF_per)) / 100).ToString();
                                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    Employer_PF = ((PF_val * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                            }
                                            else
                                            {
                                                PFS = "0";
                                                Employer_PF = "0";
                                            }


                                            //ESI Calculation
                                            if (Eligible_ESI == "1")
                                            {
                                                //string ESI_Check = ((Convert.ToDecimal(basic) * Convert.ToDecimal(80)) / 100).ToString();
                                                decimal ESI_val = (Convert.ToDecimal(ESI_Total_Amt));
                                                ESI = ((ESI_val * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                                ESI = (Math.Round(Convert.ToDecimal(ESI), 1, MidpointRounding.ToEven)).ToString();

                                                

                                                Employer_ESI = ((ESI_val * Convert.ToDecimal(EmployeerESI)) / 100).ToString();
                                                Employer_ESI = (Math.Round(Convert.ToDecimal(Employer_ESI), 1, MidpointRounding.ToEven)).ToString();

                                                //Employee ESI Round
                                                string[] split_val = ESI.Split('.');
                                                string Ist = split_val[0].ToString();
                                                string IInd = split_val[1].ToString();

                                                if (Convert.ToInt32(IInd) > 0)
                                                {
                                                    ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                                }
                                                else
                                                {
                                                    ESI = Ist;
                                                }

                                                //Employeer ESI Round
                                                string[] Emp_split_val = Employer_ESI.Split('.');
                                                string Emp_Ist = Emp_split_val[0].ToString();
                                                string Emp_IInd = Emp_split_val[1].ToString();

                                                if (Convert.ToInt32(Emp_IInd) > 0)
                                                {
                                                    Employer_ESI = (Convert.ToInt32(Emp_Ist) + 1).ToString();
                                                }
                                                else
                                                {
                                                    Employer_ESI = Emp_Ist;
                                                }

                                            }
                                            else
                                            {
                                                ESI = "0";
                                                Employer_ESI = "0";
                                            }

                                           

                                            CanteenAmt = "0";

                                            //if (EligibleCateen == "1")
                                            //{
                                            //    string CanDays = (Convert.ToDouble(Total_Work) - Convert.ToDouble(CanteenDays)).ToString();
                                            //    CanteenAmt = ((Convert.ToDecimal(LabourAmt)) * (Convert.ToDecimal(CanDays))).ToString();
                                            //    CanteenAmt = (Math.Round(Convert.ToDecimal(CanteenAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //    CanteenAmt = "0";
                                            //}
                                            //else
                                            //{
                                            //    CanteenAmt = "0";
                                            //}

                                            //Incentive Calc
                                            if (EligibleIncentive == "1")
                                            {
                                                string IncentiveDays = (Convert.ToDecimal(Total_Work) - Convert.ToDecimal(weekoff)).ToString();

                                                if (Convert.ToDecimal(Days) >= Convert.ToDecimal(IncentiveDays))
                                                {
                                                    //DayIncentive = (Convert.ToDecimal(10) * Convert.ToDecimal(Days)).ToString();
                                                    DayIncentive = "520";
                                                    DayIncentive = (Math.Round(Convert.ToDecimal(DayIncentive), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                                else
                                                {
                                                    DayIncentive = "0";
                                                }
                                            }
                                            else
                                            {
                                                DayIncentive = "0";
                                            }


                                            if (ConvenyAmt == "")
                                            {
                                                ConvenyAmt = "0";
                                            }
                                            else
                                            {
                                                if ((Convert.ToDecimal(ConvenyAmt) == Convert.ToDecimal(0)) || (Convert.ToDecimal(Fixed_Work_Days) == Convert.ToDecimal(0)))
                                                {
                                                    ConvenyAmt = "0";
                                                }
                                                else
                                                {
                                                    ConvenyAmt = (Convert.ToDecimal(ConvenyAmt) / Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                                    ConvenyAmt = (Convert.ToDecimal(ConvenyAmt) * (Convert.ToDecimal(Days))).ToString();
                                                }
                                                

                                            }

                                            if (SplAmt == "")
                                            {
                                                SplAmt = "0";

                                            }
                                            else
                                            {
                                                if ((Convert.ToDecimal(SplAmt) == Convert.ToDecimal(0)) || (Convert.ToDecimal(Fixed_Work_Days) == Convert.ToDecimal(0)))
                                                {
                                                    SplAmt = "0";
                                                }
                                                else
                                                {
                                                    SplAmt = (Convert.ToDecimal(SplAmt) / Convert.ToDecimal(Fixed_Work_Days)).ToString();
                                                    SplAmt = (Convert.ToDecimal(SplAmt) * (Convert.ToDecimal(Days))).ToString();
                                                }
                                               
                                            }

                                            TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(All4) + Convert.ToDecimal(All5) + Convert.ToDecimal(OTAmt)).ToString();
                                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                                            TotalDeductions = (Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(CanteenAmt) + Convert.ToDecimal(ConvenyAmt) + Convert.ToDecimal(SplAmt)).ToString();
                                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();

                                            if (Convert.ToDecimal(NetPay) < 0)
                                            {
                                                NetPay = "0";
                                            }
                                            double d1 = Convert.ToDouble(NetPay.ToString());
                                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                            string RoundOffNetPay = rounded1.ToString();

                                            string Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";

                                            string ProcessMode = "";
                                            DataTable DT_sal = new DataTable();
                                            SSQL = "Select *from SalaryDetails where EmpNo='" + EmpNo + "' And Month='" + Mn + "'";
                                            SSQL = SSQL + " And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                                            SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                                            SSQL = SSQL + " And Ccode='" + Session["SessionCcode"].ToString() + "' And Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                            DT_sal = ManualAttend.GetDetails(SSQL);
                                            if (DT_sal.Rows.Count != 0)
                                            {
                                                ProcessMode = DT_sal.Rows[0]["Process_Mode"].ToString();
                                                if (ProcessMode == "1")
                                                {
                                                    SSQL = "Delete from SalaryDetails where EmpNo='" + EmpNo + "' And Month='" + Mn + "'";
                                                    SSQL = SSQL + " And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                                                    SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                                                    SSQL = SSQL + " And Ccode='" + Session["SessionCcode"].ToString() + "' And Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                                    ManualAttend.GetDetails(SSQL);
                                                }
                                            }

                                            if (ProcessMode != "2")
                                            {
                                                SSQL = "insert into SalaryDetails(EmpNo,MachineNo,ExisistingCode,SalaryThrough,Wagestype,";
                                                SSQL = SSQL + "FromBankACno,FromBankName,FromBranch,Totalworkingdays,PerDaySal,";
                                                SSQL = SSQL + "BasicandDA,ProvidentFund,HRA,ESI,GrossEarnings,allowances1,allowances2,allowances3,";
                                                SSQL = SSQL + "allowances4,allowances5,Deduction1,Deduction2,Deduction3,Deduction4,Deduction5,";
                                                SSQL = SSQL + "Messdeduction,Losspay,TotalDeductions,NetPay,SalaryDate,Month,Year,FinancialYear,";
                                                SSQL = SSQL + "LeaveDays,LOPDays,TransDate,OverTime,NFh,NFH_Work_Present,FDA,VDA,Advance,Stamp,PfSalary,Words,";
                                                SSQL = SSQL + "WorkedDays,Process_Mode,Weekoff,CL,Fbasic,FFDA,FHRA,Emp_PF,FromDate,ToDate,HomeDays,";
                                                SSQL = SSQL + "HalfNightAmt,FullNightAmt,SpinningAmt,DayIncentive,ThreesidedAmt,Basic_SM,";
                                                SSQL = SSQL + "BasicAndDANew,BasicHRA,EduAllow,MediAllow,BasicRAI,WashingAllow,";
                                                SSQL = SSQL + "RoundOffNetPay,OTHoursNew,OTHoursAmtNew,Leave_Credit_Use,Fixed_Work_Days,";
                                                SSQL = SSQL + "WH_Work_Days,DedOthers1,DedOthers2,EmployeerPFone,EmployeerPFTwo,EmployeerESI,";
                                                SSQL = SSQL + "Leave_Credit_Days,Leave_Credit_Add,Days_Shift,Three_Shift,";
                                                SSQL = SSQL + "Night_Shift,Atten_Inc_Week,Pref_Inc,Spn_Inc,Atten_Inc_Month,Total_Basic,ConvAllow,Conveyance,Ccode,Lcode)values";
                                                SSQL = SSQL + "('" + EmpNo + "','" + MachineID + "','" + ExisistingCode + "','" + SalaryThro.Trim() + "',";
                                                SSQL = SSQL + "'" + WagesType + "','" + AccountNo + "','" + BankName + "','" + BranchCode + "',";
                                                SSQL = SSQL + "'" + Total_Work + "','" + basic1 + "','" + BasicDA + "','" + PFS + "','" + HRA_Str + "','" + ESI + "',";
                                                SSQL = SSQL + "'" + TotalEarnings + "','" + All1 + "','" + All2 + "','" + All3 + "','" + All4 + "','" + All5 + "',";
                                                SSQL = SSQL + "'" + Ded1 + "','" + ded2 + "','" + ded3 + "','" + ded4 + "','" + ded5 + "','" + CanteenAmt + "',";
                                                SSQL = SSQL + "'" + SplAmt + "','" + TotalDeductions + "','" + NetPay + "',GetDate(),'" + Mn + "','" + yy + "','" + yy + "',";
                                                SSQL = SSQL + "'0','0',GetDate(),'0','" + NFh + "','" + NFH_Work_Days_Statutory + "','0','0','" + Advance + "','0','" + PFS + "','" + Words + "',";
                                                SSQL = SSQL + "'" + Days + "','1','" + weekoff + "','0','" + basic + "','0','0','0','" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',";
                                                SSQL = SSQL + "'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "','0','0','0','0','" + DayIncentive + "','0','" + basic + "',";
                                                SSQL = SSQL + "'" + basic + "','0','0','0','0','" + WashingAllow + "','" + RoundOffNetPay + "','" + OTHoursNew + "','0','0','" + Fixed_Work_Days + "',";
                                                SSQL = SSQL + "'" + WH_Work_Present + "','0','0','" + PFS + "','" + Employer_PF + "','" + Employer_ESI + "','0','0','0','" + CanteenDays + "',";
                                                SSQL = SSQL + "'0','0','0','0','0','0','" + ConvenyAmt + "','" + Conveyance + "','" + Session["SessionCcode"].ToString() + "','" + Request.Cookies["SessionLcode"].Value.ToString() + "')";
                                                ManualAttend.GetDetails(SSQL);
                                            }
                                        }
                                        else if (WagesType == "2")
                                        {


                                            if(EmpNo== "202184")
                                            {
                                                string ww="0";
                                            }

                                            //Labour Salary
                                            //Basic Calc
                                            basic1 = (Convert.ToDecimal(BasicSalary)).ToString();
                                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(Days))).ToString();
                                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                                            string OTAmt = "";
                                            if (OTEligible.ToUpper() == "YES")
                                            {
                                                if (OTHoursNew != "0")
                                                {
                                                    //string HrSal = (Convert.ToDecimal(basic1) / Convert.ToDecimal(8)).ToString();
                                                    string HrSal = OTSal; 


                                                    OTAmt = ((Convert.ToDecimal(OTHoursNew)) * (Convert.ToDecimal(HrSal))).ToString();
                                                    OTAmt = (Math.Round(Convert.ToDecimal(OTAmt), 0, MidpointRounding.AwayFromZero)).ToString();

                                                }
                                                else
                                                {
                                                    OTAmt = "0";
                                                }
                                            }
                                            else
                                            {
                                                OTAmt = "0";
                                            }

                                            string Basic_Per = "0";
                                            string HRA_Per = "0";
                                            string Convey_Per = "0";
                                            string Washing_Per = "0";
                                            string Other_Per = "0";

                                            DataTable DT_Sal = new DataTable();
                                            SSQL = "Select *from SalaryDetails_Mst where CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                            SSQL = SSQL + " And Emp_Type='2'";
                                            DT_Sal = ManualAttend.GetDetails(SSQL);

                                            if (DT_Sal.Rows.Count != 0)
                                            {
                                                Basic_Per = DT_Sal.Rows[0]["Basic_Percentage"].ToString();
                                                HRA_Per = DT_Sal.Rows[0]["HRA_Percentage"].ToString();
                                                Convey_Per = DT_Sal.Rows[0]["Convey_Percentage"].ToString();
                                                Washing_Per = DT_Sal.Rows[0]["Washing_Percentage"].ToString();
                                                Other_Per = DT_Sal.Rows[0]["Other_Percentage"].ToString();
                                            }
                                            else
                                            {
                                                Basic_Per = "0";
                                                HRA_Per = "0";
                                                Convey_Per = "0";
                                                Washing_Per = "0";
                                                Other_Per = "0";
                                            }
                                           

                                            PF_Total_Amt = (Convert.ToDecimal(basic) + Convert.ToDecimal(OTAmt)).ToString();
                                            BasicDA = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Basic_Per)) / 100).ToString();
                                            Conveyance = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Convey_Per)) / 100).ToString();
                                            WashingAllow = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(Washing_Per)) / 100).ToString();

                                            //HRA Calculate
                                            //HRA1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(26)).ToString();
                                            //HRA_Str = (Convert.ToDecimal(HRA1) * (Convert.ToDecimal(Days))).ToString();
                                            HRA1 = ((Convert.ToDecimal(PF_Total_Amt) * Convert.ToDecimal(HRA_Per)) / 100).ToString();
                                            HRA_Str = HRA1;
                                            HRA_Str = (Math.Round(Convert.ToDecimal(HRA_Str), 0, MidpointRounding.AwayFromZero)).ToString();



                                            //Spl. Allowence Calculate
                                            //SplAllow1 = (Convert.ToDecimal(BasicSalary)).ToString();
                                            //Spl_Allow_Str = (Convert.ToDecimal(SplAllow1) * (Convert.ToDecimal(Days))).ToString();
                                            SplAllow1 = "0";
                                            Spl_Allow_Str = "0"; ;
                                            Spl_Allow_Str = (Math.Round(Convert.ToDecimal(Spl_Allow_Str), 0, MidpointRounding.AwayFromZero)).ToString();

                                            ESI_Total_Amt = (Convert.ToDecimal(BasicDA) + Convert.ToDecimal(HRA_Str)).ToString();

                                            //Get Fixed Allowance and Deduction
                                            All1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance1amt)).ToString();
                                            All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();

                                            All2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance2amt)).ToString();
                                            All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();

                                            Ded1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction1amt)).ToString();
                                            Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 0, MidpointRounding.AwayFromZero)).ToString();

                                            ded2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction2amt)).ToString();
                                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 0, MidpointRounding.AwayFromZero)).ToString();


                                            //PF Calculation
                                            if (Eligible_PF == "1")
                                            {
                                                PF_salary = "1";

                                                string PF_Check = BasicDA;

                                                ///string PF_Check = ((Convert.ToDecimal(BasicDA) * Convert.ToDecimal(50)) / 100).ToString();
                                                if (Convert.ToDecimal(PF_Check) >= Convert.ToDecimal(StaffSalary))
                                                {
                                                    PFS = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(PF_per)) / 100).ToString();
                                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    Employer_PF = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                                else
                                                {
                                                    decimal PF_val = (Convert.ToDecimal(PF_Check));
                                                    PFS = ((PF_val * Convert.ToDecimal(PF_per)) / 100).ToString();
                                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                                    Employer_PF = ((PF_val * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                            }
                                            else
                                            {
                                                PFS = "0";
                                                Employer_PF = "0";
                                            }


                                            //ESI Calculation
                                            if (Eligible_ESI == "1")
                                            {
                                                //string ESI_Check = ((Convert.ToDecimal(basic) * Convert.ToDecimal(80)) / 100).ToString();
                                                decimal ESI_val = (Convert.ToDecimal(ESI_Total_Amt));
                                                ESI = ((ESI_val * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                                ESI = (Math.Round(Convert.ToDecimal(ESI), 1, MidpointRounding.ToEven)).ToString();

                                                Employer_ESI = ((ESI_val * Convert.ToDecimal(EmployeerESI)) / 100).ToString();
                                                Employer_ESI = (Math.Round(Convert.ToDecimal(Employer_ESI), 1, MidpointRounding.ToEven)).ToString();

                                                //Employee ESI Round
                                                string[] split_val = ESI.Split('.');
                                                string Ist = split_val[0].ToString();
                                                string IInd = split_val[1].ToString();

                                                if (Convert.ToInt32(IInd) > 0)
                                                {
                                                    ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                                }
                                                else
                                                {
                                                    ESI = Ist;
                                                }

                                                //Employeer ESI Round
                                                string[] Emp_split_val = Employer_ESI.Split('.');
                                                string Emp_Ist = Emp_split_val[0].ToString();
                                                string Emp_IInd = Emp_split_val[1].ToString();

                                                if (Convert.ToInt32(Emp_IInd) > 0)
                                                {
                                                    Employer_ESI = (Convert.ToInt32(Emp_Ist) + 1).ToString();
                                                }
                                                else
                                                {
                                                    Employer_ESI = Emp_Ist;
                                                }

                                            }
                                            else
                                            {
                                                ESI = "0";
                                                Employer_ESI = "0";
                                            }

                                            CanteenAmt = "0";

                                            //if (EligibleCateen == "1")
                                            //{
                                            //    string CanDays = (Convert.ToDouble(Total_Work) - Convert.ToDouble(CanteenDays)).ToString();
                                            //    CanteenAmt = ((Convert.ToDecimal(LabourAmt)) * (Convert.ToDecimal(CanDays))).ToString();
                                            //    CanteenAmt = (Math.Round(Convert.ToDecimal(CanteenAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                            //}
                                            //else
                                            //{
                                            //    CanteenAmt = "0";
                                            //}


                                            

                                            if (EligibleIncentive == "1")
                                            {
                                                string IncentiveDays = (Convert.ToDecimal(Total_Work) - Convert.ToDecimal(weekoff)).ToString();

                                                if (Convert.ToDecimal(Days) >= Convert.ToDecimal(IncentiveDays))
                                                {
                                                    //DayIncentive = (Convert.ToDecimal(10) * Convert.ToDecimal(Days)).ToString();
                                                    DayIncentive = "520";
                                                    DayIncentive = (Math.Round(Convert.ToDecimal(DayIncentive), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                                else
                                                {
                                                    DayIncentive = "0";
                                                }
                                            }
                                            else
                                            {
                                                DayIncentive = "0";
                                            }

                                            if (ConvenyAmt == "")
                                            {
                                                ConvenyAmt = "0";
                                            }
                                            else
                                            {
                                                if (StdWork == "") { StdWork = "0"; }
                                                if ((Convert.ToDecimal(ConvenyAmt) == Convert.ToDecimal(0)) && (Convert.ToDecimal(StdWork) == Convert.ToDecimal(0)))
                                                {
                                                    ConvenyAmt = "0";
                                                }
                                                else
                                                {


                                                    Temp_Con_Days = (Convert.ToDecimal(ConvenyAmt) * Convert.ToDecimal(Days)).ToString();
                                                    Temp_Con_OT = (Convert.ToDecimal(ConvenyAmt) / Convert.ToDecimal(StdWork)).ToString();
                                                    Temp_Con_OT = (Convert.ToDecimal(Temp_Con_OT) * Convert.ToDecimal(OTHoursNew)).ToString();

                                                    ConvenyAmt = (Convert.ToDecimal(Temp_Con_Days) + Convert.ToDecimal(Temp_Con_OT)).ToString();
                                                    ConvenyAmt = (Math.Round(Convert.ToDecimal(ConvenyAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }
                                            }

                                            if (SplAmt == "")
                                            {
                                                SplAmt = "0";
                                            }
                                            else
                                            {
                                                //SplAmt = (Convert.ToDecimal(SplAmt) * Convert.ToDecimal(Days)).ToString();
                                                if (StdWork == "") { StdWork = "0"; }
                                                if ((Convert.ToDecimal(SplAmt) == Convert.ToDecimal(0)) && (Convert.ToDecimal(StdWork) == Convert.ToDecimal(0)))
                                                {
                                                    SplAmt = "0";
                                                }
                                                else
                                                {


                                                    Temp_Spl_Days = (Convert.ToDecimal(SplAmt) * Convert.ToDecimal(Days)).ToString();
                                                    Temp_Spl_OT = (Convert.ToDecimal(SplAmt) / Convert.ToDecimal(StdWork)).ToString();
                                                    Temp_Spl_OT = (Convert.ToDecimal(Temp_Spl_OT) * Convert.ToDecimal(OTHoursNew)).ToString();

                                                    SplAmt = (Convert.ToDecimal(Temp_Spl_Days) + Convert.ToDecimal(Temp_Spl_OT)).ToString();
                                                    SplAmt = (Math.Round(Convert.ToDecimal(SplAmt), 0, MidpointRounding.AwayFromZero)).ToString();
                                                }

                                            }

                                            TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(All4) + Convert.ToDecimal(All5) + Convert.ToDecimal(OTAmt)).ToString();
                                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                                            TotalDeductions = (Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5) + Convert.ToDecimal(CanteenAmt) + Convert.ToDecimal(ConvenyAmt) + Convert.ToDecimal(SplAmt)).ToString();
                                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();

                                            if (Convert.ToDecimal(NetPay) < 0)
                                            {
                                                NetPay = "0";
                                            }
                                            double d1 = Convert.ToDouble(NetPay.ToString());
                                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                            string RoundOffNetPay = rounded1.ToString();

                                            string Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";

                                            string ProcessMode = "";
                                            DataTable DT_sal = new DataTable();
                                            SSQL = "Select * from SalaryDetails where EmpNo='" + EmpNo + "' And Month='" + Mn + "'";
                                            SSQL = SSQL + " And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                                            SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                                            SSQL = SSQL + " And Ccode='" + Session["SessionCcode"].ToString() + "' And Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                            DT_sal = ManualAttend.GetDetails(SSQL);
                                            if (DT_sal.Rows.Count != 0)
                                            {
                                                ProcessMode = DT_sal.Rows[0]["Process_Mode"].ToString();
                                                if (ProcessMode == "1")
                                                {
                                                    SSQL = "Delete from SalaryDetails where EmpNo='" + EmpNo + "' And Month='" + Mn + "'";
                                                    SSQL = SSQL + " And CONVERT(datetime,FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                                                    SSQL = SSQL + " And CONVERT(datetime,ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                                                    SSQL = SSQL + " And Ccode='" + Session["SessionCcode"].ToString() + "' And Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                                                    ManualAttend.GetDetails(SSQL);
                                                }
                                            }

                                            if (ProcessMode != "2")
                                            {
                                                SSQL = "insert into SalaryDetails(EmpNo,MachineNo,ExisistingCode,SalaryThrough,Wagestype,";
                                                SSQL = SSQL + "FromBankACno,FromBankName,FromBranch,Totalworkingdays,PerDaySal,";
                                                SSQL = SSQL + "BasicandDA,ProvidentFund,HRA,ESI,GrossEarnings,allowances1,allowances2,allowances3,";
                                                SSQL = SSQL + "allowances4,allowances5,Deduction1,Deduction2,Deduction3,Deduction4,Deduction5,";
                                                SSQL = SSQL + "Messdeduction,Losspay,TotalDeductions,NetPay,SalaryDate,Month,Year,FinancialYear,";
                                                SSQL = SSQL + "LeaveDays,LOPDays,TransDate,OverTime,NFh,NFH_Work_Present,FDA,VDA,Advance,Stamp,PfSalary,Words,";
                                                SSQL = SSQL + "WorkedDays,Process_Mode,Weekoff,CL,Fbasic,FFDA,FHRA,Emp_PF,FromDate,ToDate,HomeDays,";
                                                SSQL = SSQL + "HalfNightAmt,FullNightAmt,SpinningAmt,DayIncentive,ThreesidedAmt,Basic_SM,";
                                                SSQL = SSQL + "BasicAndDANew,BasicHRA,EduAllow,MediAllow,BasicRAI,WashingAllow,";
                                                SSQL = SSQL + "RoundOffNetPay,OTHoursNew,OTHoursAmtNew,Leave_Credit_Use,Fixed_Work_Days,";
                                                SSQL = SSQL + "WH_Work_Days,DedOthers1,DedOthers2,EmployeerPFone,EmployeerPFTwo,EmployeerESI,";
                                                SSQL = SSQL + "Leave_Credit_Days,Leave_Credit_Add,Days_Shift,Three_Shift,";
                                                SSQL = SSQL + "Night_Shift,Atten_Inc_Week,Pref_Inc,Spn_Inc,Atten_Inc_Month,Total_Basic,ConvAllow,Conveyance,Ccode,Lcode)values";
                                                SSQL = SSQL + "('" + EmpNo + "','" + MachineID + "','" + ExisistingCode + "','" + SalaryThro.Trim() + "',";
                                                SSQL = SSQL + "'" + WagesType + "','" + AccountNo + "','" + BankName + "','" + BranchCode + "',";
                                                SSQL = SSQL + "'" + Total_Work + "','" + basic1 + "','" + BasicDA + "','" + PFS + "','" + HRA_Str + "','" + ESI + "',";
                                                SSQL = SSQL + "'" + TotalEarnings + "','" + All1 + "','" + All2 + "','" + All3 + "','" + All4 + "','" + All5 + "',";
                                                SSQL = SSQL + "'" + Ded1 + "','" + ded2 + "','" + ded3 + "','" + ded4 + "','" + ded5 + "','" + CanteenAmt + "',";
                                                SSQL = SSQL + "'" + SplAmt + "','" + TotalDeductions + "','" + NetPay + "',GetDate(),'" + Mn + "','" + yy + "','" + yy + "',";
                                                SSQL = SSQL + "'0','0',GetDate(),'0','" + NFh + "','" + NFH_Work_Days_Statutory + "','0','0','" + Advance + "','0','" + PFS + "','" + Words + "',";
                                                SSQL = SSQL + "'" + Days + "','1','" + weekoff + "','0','" + basic + "','0','0','0','" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',";
                                                SSQL = SSQL + "'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "','0','0','0','0','" + DayIncentive + "','0','" + basic + "',";
                                                SSQL = SSQL + "'" + basic + "','0','0','0','0','" + WashingAllow + "','" + RoundOffNetPay + "','" + OTHoursNew + "','" + OTAmt + "','0','" + Fixed_Work_Days + "',";
                                                SSQL = SSQL + "'" + WH_Work_Present + "','0','0','" + PFS + "','" + Employer_PF + "','" + Employer_ESI + "','0','0','0','" + CanteenDays + "',";
                                                SSQL = SSQL + "'0','0','0','0','0','0','" + ConvenyAmt + "','" + Conveyance + "','" + Session["SessionCcode"].ToString() + "','" + Request.Cookies["SessionLcode"].Value.ToString() + "')";
                                                ManualAttend.GetDetails(SSQL);
                                            }

                                        }



                                    }
                                }

                            }
                            else
                            {
                                excelConnection.Close();
                                return View("SalaryUpload");
                            }

                        }
                        excelConnection.Close();
                        Response.Write("<script>alert('Salary Calculation finished Successfully')</script>");
                        return View("SalaryUpload");
                    }
                }
                else
                {
                    Response.Write("<script>alert('Please Upload the file correctly')</script>");
                    return View("SalaryUpload");
                }
            }
            else
            {
                return View("SalaryUpload");
            }
            return View("SalaryUpload");
        }

        public string NumerictoNumber(int Number, bool isUK)
        {
            if (Number == 0)
                return "Zero";

            string and = isUK ? "and " : "";
            if (Number == -2147483648)
                return "Minus Two Billion One Hundred " + and +
            "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
            "Six Hundred " + and + "Forty Eight";

            int[] num = new int[4];
            int first = 0;
            int u, h, t = 0;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (Number < 0)
            {
                sb.Append("Minus");
                Number = -Number;
            }

            string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
            string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
            string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
            num[0] = Number % 1000;
            num[1] = Number / 1000;
            num[2] = Number / 1000000;
            num[1] = num[1] - 1000 * num[2];  // thousands
            num[2] = num[2] - 100 * num[3];//laks
            num[3] = Number / 10000000;     // billions
            num[2] = num[2] - 1000 * num[3];  // millions

            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0) continue;
                u = num[i] % 10;
                t = num[i] / 10;
                h = num[i] / 100;
                t = t - 10 * h;

                if (h > 0)
                    sb.Append(words0[h] + " Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i < first)
                        sb.Append(and);

                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);

                }

                if (i != 0)
                    sb.Append(words3[i - 1]);

            }

            return sb.ToString().TrimEnd();



        }

        public JsonResult GetEmployeeType()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select EmpTypeCd,EmpType,CASE when EmpCategory='1' then 'Staff' else 'Labour' END as EmpCategory from MstEmployeeType ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["EmpTypeCd"].ToString();
                list.Name = dt.Rows[i]["EmpType"].ToString();
                list.Category = dt.Rows[i]["EmpCategory"].ToString();
                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FinancialYear1()
        {
            List<MasterDate> PartyList = new List<MasterDate>();
            int currentYear = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                MasterDate list = new MasterDate();
                list.ID = currentYear.ToString();
                list.Name = (currentYear.ToString() + "-" + Convert.ToString(currentYear + 1)).ToString();
                currentYear = currentYear - 1;
                PartyList.Add(list);
            }
            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SalaryDownload()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult AttendanceDownload()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }
        

        public void GetAttendanceDownload(string WagesType,string AttendanceDate)
        {
            DataTable AutoDTable = new DataTable();
            DataTable dsEmployee = new DataTable();

            //AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Token No");
            AutoDTable.Columns.Add("EmpName");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("Days");
            AutoDTable.Columns.Add("OT Hours");
            AutoDTable.Columns.Add("OT Days");
            AutoDTable.Columns.Add("W.H");
            AutoDTable.Columns.Add("W.H.P");
            AutoDTable.Columns.Add("N/FH");
            AutoDTable.Columns.Add("NFH W.Days");
            AutoDTable.Columns.Add("SPG Allow");
            AutoDTable.Columns.Add("Canteen");
            AutoDTable.Columns.Add("Fixed W.Days");
            AutoDTable.Columns.Add("DAY SHIFT");
            AutoDTable.Columns.Add("NIGHT SHIFT");
            AutoDTable.Columns.Add("Total Month");

            string SSQL = "";
            SSQL = "select Cast(EM.MachineID As int) As MachineID,EM.ExistingCode,isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as FirstName,isnull(DM.DeptName,'') as DeptName ";
            SSQL = SSQL + " ,isnull(EM.OTEligible,'') as OTEligible from Employee_Mst EM Inner join Department_Mst DM on EM.DeptName = DM.DeptCode ";
            SSQL = SSQL + " where EM.CompCode='" + Session["SessionCcode"].ToString() + "' And EM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.RelieveDate, 103)>=CONVERT(DATETIME,'"+ AttendanceDate + "',103))";
            if (WagesType != "0")
            {
                SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
            }
            //SSQL = SSQL + " And DM.Compcode='" + Session["SessionCcode"].ToString() + "' And DM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            SSQL = SSQL + " Order By EM.MachineID";

            dsEmployee = ManualAttend.GetDetails(SSQL);

            int Ink = 0;

            for (int i = 0; i < dsEmployee.Rows.Count; i++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[Ink]["MachineID"] = dsEmployee.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[Ink]["Token No"] = dsEmployee.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[Ink]["EmpName"] = dsEmployee.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[Ink]["DeptName"] = dsEmployee.Rows[i]["DeptName"].ToString();
                AutoDTable.Rows[Ink]["Days"] = "0";
                AutoDTable.Rows[Ink]["OT Hours"] = "0";
                AutoDTable.Rows[Ink]["OT Days"] = "0";
                AutoDTable.Rows[Ink]["W.H"] = "0";
                AutoDTable.Rows[Ink]["W.H.P"] = "0";
                AutoDTable.Rows[Ink]["N/FH"] = "0";
                AutoDTable.Rows[Ink]["NFH W.Days"] = "0";
                AutoDTable.Rows[Ink]["SPG Allow"] = "0";
                AutoDTable.Rows[Ink]["Canteen"] = "0";
                AutoDTable.Rows[Ink]["Fixed W.Days"] = "0";
                AutoDTable.Rows[Ink]["DAY SHIFT"] = "0";
                AutoDTable.Rows[Ink]["NIGHT SHIFT"] = "0";
                AutoDTable.Rows[Ink]["Total Month"] = "0";

                Ink = Ink + 1;
            }

            UploadDataTableToExcel(AutoDTable);

        }

        public void GetSalaryDownload(string WagesType,string AttendanceDate)
        {
            DataTable AutoDTable = new DataTable();
            DataTable dsEmployee = new DataTable();

            //AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Token No");
            AutoDTable.Columns.Add("EmpName");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("Allowance3");
            AutoDTable.Columns.Add("Allowance4");
            AutoDTable.Columns.Add("Allowance5");
            AutoDTable.Columns.Add("Detection3");
            AutoDTable.Columns.Add("Detection4");
            AutoDTable.Columns.Add("Detection5");
            AutoDTable.Columns.Add("Advance");

            string SSQL = "";
            SSQL = "select Cast(EM.MachineID As int) As MachineID,EM.ExistingCode,isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as FirstName,isnull(DM.DeptName,'') as DeptName ";
            SSQL = SSQL + " ,isnull(EM.OTEligible,'') as OTEligible from Employee_Mst EM Inner join Department_Mst DM on EM.DeptName = DM.DeptCode ";
            SSQL = SSQL + " where EM.CompCode='" + Session["SessionCcode"].ToString() + "' And EM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.RelieveDate, 103)>=CONVERT(DATETIME,'" + AttendanceDate + "',103))";
            if (WagesType != "0")
            {
                SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
            }
            //SSQL = SSQL + " And DM.Compcode='" + Session["SessionCcode"].ToString() + "' And DM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            SSQL = SSQL + " Order By EM.MachineID";

            dsEmployee = ManualAttend.GetDetails(SSQL);

            int Ink = 0;

            for (int i = 0; i < dsEmployee.Rows.Count; i++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[Ink]["MachineID"] = dsEmployee.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[Ink]["Token No"] = dsEmployee.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[Ink]["EmpName"] = dsEmployee.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[Ink]["DeptName"] = dsEmployee.Rows[i]["DeptName"].ToString();
                AutoDTable.Rows[Ink]["Allowance3"] = "0";
                AutoDTable.Rows[Ink]["Allowance4"] = "0";
                AutoDTable.Rows[Ink]["Allowance5"] = "0";
                AutoDTable.Rows[Ink]["Detection3"] = "0";
                AutoDTable.Rows[Ink]["Detection4"] = "0";
                AutoDTable.Rows[Ink]["Detection5"] = "0";
                AutoDTable.Rows[Ink]["Advance"] = "0";

                Ink = Ink + 1;
            }

            UploadDataTableToAttendExcel(AutoDTable);



        }


        protected void UploadDataTableToExcel(DataTable dtRecords)
        {
            string XlsPath = Server.MapPath(@"~/Add_data/AttendanceDownload.xls");
            string attachment = string.Empty;
            if (XlsPath.IndexOf("\\") != -1)
            {
                string[] strFileName = XlsPath.Split(new char[] { '\\' });
                attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
            }
            else
                attachment = "attachment; filename=" + XlsPath;
            try
            {
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/vnd.ms-excel";
                string tab = string.Empty;

                foreach (DataColumn datacol in dtRecords.Columns)
                {
                    Response.Write(tab + datacol.ColumnName);
                    tab = "\t";
                }
                Response.Write("\n");

                foreach (DataRow dr in dtRecords.Rows)
                {
                    tab = "";
                    for (int j = 0; j < dtRecords.Columns.Count; j++)
                    {
                        Response.Write(tab + Convert.ToString(dr[j]));
                        tab = "\t";
                    }

                    Response.Write("\n");
                }
                Response.End();
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
            }
        }

        protected void UploadDataTableToAttendExcel(DataTable dtRecords)
        {
            string XlsPath = Server.MapPath(@"~/Add_data/SalaryDownload.xls");
            string attachment = string.Empty;
            if (XlsPath.IndexOf("\\") != -1)
            {
                string[] strFileName = XlsPath.Split(new char[] { '\\' });
                attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
            }
            else
                attachment = "attachment; filename=" + XlsPath;
            try
            {
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/vnd.ms-excel";
                string tab = string.Empty;

                foreach (DataColumn datacol in dtRecords.Columns)
                {
                    Response.Write(tab + datacol.ColumnName);
                    tab = "\t";
                }
                Response.Write("\n");

                foreach (DataRow dr in dtRecords.Rows)
                {
                    tab = "";
                    for (int j = 0; j < dtRecords.Columns.Count; j++)
                    {
                        Response.Write(tab + Convert.ToString(dr[j]));
                        tab = "\t";
                    }

                    Response.Write("\n");
                }
                Response.End();
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
            }
        }


    }
}
