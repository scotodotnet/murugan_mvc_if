﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web.UI;
using System.Globalization;

namespace AttendanceMVC.Controllers
{

    public class SalaryProcessController : Controller
    {
         string Ccode = "";
         string Lcode = "";
         string SessionCcode = "";
         string SessionLcode = "";
         string SessionUserName;
         string SessionUserID;
        string PartyName;
        DateTime frmDate;
        DateTime toDate;
        string Machine_ID_Encrypt;

        string mIpAddress_IN;
        string mIpAddress_OUT;

        string SSQL = "";
        DateTime date1;
        DateTime date2;
        DateTime Date2 = new DateTime();

        static string EmpNo = "", ExisistingCode = "", MachineNo = "", Days = "", Months = "", FinancialYear = "";
        static string TotalDays = "", NFh = "", weekoff = "", OTHoursNew = "", Fixed_Work_Days = "", NFH_Work_Days_Statutory = "", WH_Work_Present = "";
        static string BasicSalary = "", Allowance1amt = "", Allowance2amt = "", Deduction1amt = "", Deduction2amt = "", ConvenyAmt = "", SplAmt = "";
        static string ExistingCode = "", MachineID = "", EligibleCateen = "", EligibleIncentive = "", EligibleSnacks = "", Eligible_PF = "", Eligible_ESI = "";
        static string PF_per = "", ESI_per = "", StaffSalary = "", EmployeerPFone = "", EmployeerPFTwo = "", EmployeerESI = "";
        string basic1 = "0.00";
        string HRA1 = "0.00";
        string SplAllow1 = "0.00";
        static string basic = "0";
        static string Advance = "0";
        static string pfAmt = "0";
        static string ESI = "0";
        static string stamp = "0";
        static string Union = "0";
        static string TotalEarnings = "0";
        static string TotalDeductions = "0";
        static string NetPay = "0";
        static string Words = "";
        static string PFS = "0.00";
        static string ExistNo = "";
        static string PF_salary;
        static string Employer_PF = "0.00";
        static string Employer_ESI = "0.00";
        static string ded3 = "0";
        static string ded4 = "0";
        static string ded5 = "0";
        static string All3 = "0";
        static string Al4 = "0";
        static string All5 = "0";
        static string HRA_Str = "0";
        static string Spl_Allow_Str = "0";

        static string All1 = "0";
        static string All2 = "0";
        static string Ded1 = "0";
        static string ded2 = "0";

        // GET: SalaryProcess

        public JsonResult checkSession()
        {
            MasterDate c = new MasterDate();
            if (Session["Ccode"] != null)
            {
                c.sessionValue = true;
            }
            else
            {
                c.sessionValue = false;

            }
            return Json(c, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AttdendanceMove()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult AdvanceEntry()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult ManualDeduction()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetEditEmpID(string EmpNo)
        {
            DataTable employees = new DataTable();

            string query = "select EmpNo,ExistingCode,(FirstName + '.' + MiddleInitial) as EmpName,DM.DeptName,MT.EmpType  from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode inner join MstEmployeeType MT on MT.EmpTypeCd=EM.Wages where EM.IsActive='Yes' and EmpNo='" + EmpNo + "'";

            employees = ManualAttend.GetDetails(query);

            List<ManualAttend> empList = new List<ManualAttend>();
            for (int i = 0; i < employees.Rows.Count; i++)
            {
                ManualAttend student = new ManualAttend();
                student.EmpNo = employees.Rows[i]["EmpNo"].ToString();
                student.ExistingCode = employees.Rows[i]["ExistingCode"].ToString();
                student.EmpName = employees.Rows[i]["EmpName"].ToString();
                student.DeptName = employees.Rows[i]["DeptName"].ToString();
                student.WagesType = employees.Rows[i]["EmpType"].ToString();


                empList.Add(student);
            }


            return Json(empList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEmpID()
        {
            DataTable dt = new DataTable();
            string SQL = "Select EM.EmpNo,EM.ExistingCode,EM.FirstName,DM.DeptName from Employee_Mst EM inner join Department_Mst DM on EM.DeptName=DM.DeptCode where EM.IsActive='Yes' and EM.CompCode='" + Session["SessionCcode"].ToString() + "' and EM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            dt = ManualAttend.GetDetails(SQL);

            List<ManualAttend> PartyList = new List<ManualAttend>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ManualAttend Qtylist = new ManualAttend();
                Qtylist.EmpNo = dt.Rows[i]["EmpNo"].ToString();
                Qtylist.ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                Qtylist.EmpName = dt.Rows[i]["FirstName"].ToString();
                Qtylist.DeptName = dt.Rows[i]["DeptName"].ToString();
                PartyList.Add(Qtylist);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AttendanceConversion(string FromDate, string ToDate, string WagesType)
        {
            DataTable SHift = new DataTable();
            string Year = "";
            string Month = "";
            string SSQL = "";
            DataTable DT = new DataTable();

            List<ManualAttend> empList = new List<ManualAttend>();
            date1 = Convert.ToDateTime(FromDate);
            Date2 = Convert.ToDateTime(ToDate);
            int daycount = (int)(((Date2 - date1).TotalDays) + 1);
            string Mn = Convert.ToDateTime(FromDate).Month.ToString();
            string yy = Convert.ToDateTime(FromDate).Year.ToString();
            DataTable DT_Wages = new DataTable();
            string Wages = "0";

            SSQL = "select Distinct EmpTypeCd,EmpType from MstEmployeeType where EmpType='" + WagesType + "' order by EmpType Asc";
            DT_Wages = ManualAttend.GetDetails(SSQL);
            if (DT_Wages.Rows.Count != 0)
            {
                Wages = DT_Wages.Rows[0]["EmpTypeCd"].ToString();
            }



            SSQL = "select EM.EmpNo,EM.FirstName,LD.ExistingCode,DM.DeptCode,DM.DeptName,isnull(sum(LD.Present),0) as Present,isnull(sum(LD.OTHours),0) as OTHours,";
            SSQL = SSQL + "(isnull(sum(LD.Present),0)-(isnull(sum(LD.Wh_Present_Count),0)+isnull(sum(LD.NFH_Present_Count),0))) as Wrkng_Days,";
            SSQL = SSQL + "isnull(sum(LD.Wh_Count),0) as Wh_Count,isnull(sum(LD.NFH_Count),0) as NFH_Count,";
            SSQL = SSQL + "isnull(sum(LD.Wh_Present_Count),0) as Wh_Present_Count,isnull(sum(LD.NFH_Present_Count),0) as NFH_Present_Count";
            SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on EM.EmpNo=LD.MachineID inner join Department_Mst DM on LD.DeptName=DM.DeptCode";
            SSQL = SSQL + " where LD.CompCode='" + Session["SessionCcode"].ToString() + "' And LD.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            SSQL = SSQL + " And EM.CompCode='" + Session["SessionCcode"].ToString() + "' And EM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            SSQL = SSQL + " And CONVERT(datetime,LD.Attn_Date_Str,120) >= CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            SSQL = SSQL + " And CONVERT(datetime,LD.Attn_Date_Str,120) <= CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            if (WagesType != "0")
            {
                SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
            }
            SSQL = SSQL + " Group By EM.EmpNo,EM.FirstName,LD.ExistingCode,DM.DeptCode,DM.DeptName,LD.MachineID,LD.DeptName,DM.DeptCode";
            DT = ManualAttend.GetDetails(SSQL);


            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ManualAttend List = new ManualAttend();

                List.EmpNo = DT.Rows[i]["EmpNo"].ToString();
                List.EmpName = DT.Rows[i]["FirstName"].ToString();
                List.ExistingCode = DT.Rows[i]["ExistingCode"].ToString();
                List.AttnStatus = DT.Rows[i]["DeptCode"].ToString();
                List.DeptName = DT.Rows[i]["DeptName"].ToString();
                List.AttnDate = DT.Rows[i]["Wrkng_Days"].ToString();
                List.AttnDateStr = (Convert.ToDecimal(daycount) - Convert.ToDecimal(DT.Rows[i]["Wh_Count"].ToString())).ToString();
                List.LogTimeIn = DT.Rows[i]["Wh_Count"].ToString();
                List.LogTimeOut = DT.Rows[i]["NFH_Count"].ToString();
                List.LeaveDesc = DT.Rows[i]["Wh_Present_Count"].ToString();
                List.LeaveType = DT.Rows[i]["NFH_Present_Count"].ToString();
                List.FormName = DT.Rows[i]["OTHours"].ToString();
                List.Machine_No = Mn.ToString();
                List.MenuName = yy.ToString();
                List.ShiftType = daycount.ToString();

                empList.Add(List);

            }

            return Json(empList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult SaveDeduction(ManualAttend EmpDetails)
        {
            string Query = "";
            string status = "Insert";
            DataTable dt_Save = new DataTable();


            SSQL = "";
            SSQL = SSQL + "select * from ManDeduction_Details where EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,FromDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) and ";
            SSQL = SSQL + "convert(datetime,ToDate,103)=convert(datetime,'" + EmpDetails.AttnDateStr + "',103) and CompCode='" + Session["SessionCcode"].ToString() + "' and LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            dt_Save = ManualAttend.GetDetails(SSQL);

            if (dt_Save.Rows.Count != 0)
            {
                SSQL = "";
                SSQL = SSQL + "delete from ManDeduction_Details where EmpNo='" + EmpDetails.EmpNo + "' and convert(datetime,FromDate,103)=convert(datetime,'" + EmpDetails.AttnDate + "',103) and ";
                SSQL = SSQL + "convert(datetime,ToDate,103)=convert(datetime,'" + EmpDetails.AttnDateStr + "',103) and CompCode='" + Session["SessionCcode"].ToString() + "' and LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                ManualAttend.GetDetails(SSQL);

                status = "Update";
            }

            SSQL = "";
            SSQL = SSQL + "Insert into ManDeduction_Details(EmpNo,ExistingCode,EmpName,EmpType,Allowance1,Allowance2,";
            SSQL = SSQL + "Allowance3,Deduction1,Deduction2,Deduction3,OthersDeduction1,OthersDeduction2,FromDate,ToDate,CompCode,LocCode) values(";
            SSQL = SSQL + "'" + EmpDetails.EmpNo + "','" + EmpDetails.ExistingCode + "','" + EmpDetails.EmpName + "','" + EmpDetails.WagesType + "',";
            SSQL = SSQL + "'" + EmpDetails.Allowance3 + "','" + EmpDetails.Allowance4 + "','" + EmpDetails.Allowance5 + "','" + EmpDetails.Deduction3 + "',";
            SSQL = SSQL + "'" + EmpDetails.Deduction4 + "',0,0,'" + EmpDetails.Deduction5 + "',convert(datetime,'" + EmpDetails.AttnDate + "',103),";
            SSQL = SSQL + "convert(datetime,'" + EmpDetails.AttnDateStr + "',103),'" + Session["SessionCcode"].ToString() + "','" + Request.Cookies["SessionLcode"].Value.ToString() + "')";


            ManualAttend.GetDetails(SSQL);

            return Json(status);
        }
        public JsonResult AttendanceConversionSave(string FromDate, string ToDate, string WagesType)
        {
            string message = "";
            DataTable SHift = new DataTable();
            string Year = "";
            string Month = "";
            string SSQL = "";
            DataTable DT = new DataTable();
            DataTable DT_Check = new DataTable();

            List<ManualAttend> empList = new List<ManualAttend>();
            date1 = Convert.ToDateTime(FromDate);
            Date2 = Convert.ToDateTime(ToDate);
            int daycount = (int)(((Date2 - date1).TotalDays) + 1);
            string Mn = Convert.ToDateTime(FromDate).ToString("MMMM");
            string yy = Convert.ToDateTime(FromDate).Year.ToString();

            DataTable DT_Wages = new DataTable();
            string Wages = "0";

            SSQL = "select Distinct EmpTypeCd,EmpType from MstEmployeeType where EmpType='" + WagesType + "' order by EmpType Asc";
            DT_Wages = ManualAttend.GetDetails(SSQL);
            if (DT_Wages.Rows.Count != 0)
            {
                Wages = DT_Wages.Rows[0]["EmpTypeCd"].ToString();
            }

            SSQL = "select EM.EmpNo,EM.FirstName,LD.ExistingCode,DM.DeptCode,DM.DeptName,isnull(sum(LD.Present),0) as Present,isnull(sum(LD.OTHours),0) as OTHours,";
            SSQL = SSQL + "(isnull(sum(LD.Present),0)-(isnull(sum(LD.Wh_Present_Count),0)+isnull(sum(LD.NFH_Present_Count),0))) as Wrkng_Days,";
            SSQL = SSQL + "isnull(sum(LD.Wh_Count),0) as Wh_Count,isnull(sum(LD.NFH_Count),0) as NFH_Count,";
            SSQL = SSQL + "isnull(sum(LD.Wh_Present_Count),0) as Wh_Present_Count,isnull(sum(LD.NFH_Present_Count),0) as NFH_Present_Count";
            SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on EM.EmpNo=LD.MachineID inner join Department_Mst DM on LD.DeptName=DM.DeptCode";
            SSQL = SSQL + " where LD.CompCode='" + Session["SessionCcode"].ToString() + "' And LD.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            SSQL = SSQL + " And EM.CompCode='" + Session["SessionCcode"].ToString() + "' And EM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "' and EM.Wages='" + WagesType + "'";
            SSQL = SSQL + " And CONVERT(datetime,LD.Attn_Date_Str,120) >= CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            SSQL = SSQL + " And CONVERT(datetime,LD.Attn_Date_Str,120) <= CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            if (Wages != "0")
            {
                SSQL = SSQL + " And EM.Wages='" + Wages + "'";
            }
            SSQL = SSQL + " Group By EM.EmpNo,EM.FirstName,LD.ExistingCode,DM.DeptCode,DM.DeptName,LD.MachineID,LD.DeptName,DM.DeptCode";
            DT = ManualAttend.GetDetails(SSQL);

            SSQL = "Delete from Temp_AttenanceDetails where Ccode='" + Session["SessionCcode"].ToString() + "' And Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            ManualAttend.GetDetails(SSQL);


            for (int i = 0; i < DT.Rows.Count; i++)
            {

                string FD = (Convert.ToDecimal(daycount) - Convert.ToDecimal(DT.Rows[i]["Wh_Count"].ToString())).ToString();


            


                SSQL = "insert into Temp_AttenanceDetails(DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,TotalDays,NFh,WorkingDays,CL,AbsentDays,";
                SSQL = SSQL + "weekoff,FromDate,ToDate,Modeval,home,halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,";
                SSQL = SSQL + "NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,Day_Shift,Night_Shift,WH_Work_Present,OTDays,";
                SSQL = SSQL + "Ccode,Lcode)values";
                SSQL = SSQL + "('" + DT.Rows[i]["DeptCode"].ToString() + "','" + DT.Rows[i]["EmpNo"].ToString() + "','" + DT.Rows[i]["ExistingCode"].ToString() + "',";
                SSQL = SSQL + "'" + DT.Rows[i]["Wrkng_Days"].ToString() + "','" + Mn + "','" + yy + "',GetDate(),'" + daycount.ToString() + "','" + DT.Rows[i]["NFH_Count"].ToString() + "',";
                SSQL = SSQL + "'" + daycount.ToString() + "','0.0','0.0','" + DT.Rows[i]["Wh_Count"].ToString() + "','" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "','" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',";
                SSQL = SSQL + "'0','0.0','0.0','0.0','0.0','" + DT.Rows[i]["OTHours"].ToString() + "','" + FD + "','" + DT.Rows[i]["Wh_Present_Count"].ToString() + "','" + DT.Rows[i]["NFH_Present_Count"].ToString() + "',";
                SSQL = SSQL + "'0.0','0.0','0.0','0.0','0.0','0.0',";
                SSQL = SSQL + "'" + Session["SessionCcode"].ToString() + "','" + Request.Cookies["SessionLcode"].Value.ToString() + "')";
                ManualAttend.GetDetails(SSQL);

                message = "Success";
            }

            SSQL = "";
            SSQL = SSQL + "select TA.EmpNo,EM.ExistingCode,EM.FirstName,DM.DeptName,TA.Days,TA.WH_Work_Days,TA.WH_Work_Present,TA.NFh,TA.NFH_Work_Days_Statutory, ";
            SSQL = SSQL + "TA.OTHoursNew from Temp_AttenanceDetails TA inner join Employee_Mst EM on TA.EmpNo=EM.EmpNo ";
            SSQL = SSQL + "inner join Department_Mst DM on DM.DeptCode=EM.DeptName where Ccode='" + Session["SessionCcode"].ToString() + "' and Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            DT = ManualAttend.GetDetails(SSQL);
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ManualAttend List = new ManualAttend();

                List.EmpNo = DT.Rows[i]["EmpNo"].ToString();
                List.EmpName = DT.Rows[i]["FirstName"].ToString();
                List.ExistingCode = DT.Rows[i]["ExistingCode"].ToString();
                //List.AttnStatus = DT.Rows[i]["DeptCode"].ToString();
                List.DeptName = DT.Rows[i]["DeptName"].ToString();
                List.AttnDate = DT.Rows[i]["Days"].ToString();
                List.AttnDateStr = (Convert.ToDecimal(daycount) - Convert.ToDecimal(DT.Rows[i]["WH_Work_Days"].ToString())).ToString();
                List.LogTimeIn = DT.Rows[i]["WH_Work_Days"].ToString();
                List.LogTimeOut = DT.Rows[i]["NFh"].ToString();
                List.LeaveDesc = DT.Rows[i]["WH_Work_Present"].ToString();
                List.LeaveType = DT.Rows[i]["NFH_Work_Days_Statutory"].ToString();
                List.FormName = DT.Rows[i]["OTHoursNew"].ToString();
                List.Machine_No = Mn.ToString();
                List.MenuName = yy.ToString();
                List.ShiftType = daycount.ToString();

                empList.Add(List);

            }

            return Json(empList, JsonRequestBehavior.AllowGet);

            //return Json(message, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GeneratePaylip(string FromDate, string ToDate,string WagesType)
        {
            DataTable dt_Attend = new DataTable();
            DataTable dt_Employee = new DataTable();
            DataTable dtpf = new DataTable();
            DataTable dt_Deduction = new DataTable();
            string Status = "";
            string Query = "";

            //Get PF ESI Details
            Query = "select * from MstESIPF where Ccode='" + Session["SessionCcode"].ToString() + "' and Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            dtpf = ManualAttend.GetDetails(Query);
            if (dtpf.Rows.Count > 0)
            {
                PF_per = dtpf.Rows[0]["PF_per"].ToString();
                ESI_per = dtpf.Rows[0]["ESI_per"].ToString();
                StaffSalary = dtpf.Rows[0]["StaffSalary"].ToString();
                EmployeerPFone = dtpf.Rows[0]["EmployeerPFone"].ToString();
                EmployeerPFTwo = dtpf.Rows[0]["EmployeerPFTwo"].ToString();
                EmployeerESI = dtpf.Rows[0]["EmployeerESI"].ToString();
            }
            else
            {
                PF_per = "0"; ESI_per = "0"; StaffSalary = "0";
                EmployeerPFone = "0"; EmployeerPFTwo = "0"; EmployeerESI = "0";
            }




            //Get Attendance
            SSQL = "Select * from Temp_AttenanceDetails where convert(datetime,FromDate,103)=convert(datetime,'" + FromDate + "',103) and convert(datetime,FromDate,103)=convert(datetime,'" + FromDate + "',103) ";
            SSQL = SSQL + "and Ccode='" + Session["SessionCcode"].ToString() + "' and Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            dt_Attend = ManualAttend.GetDetails(SSQL);
            if (dt_Attend.Rows.Count > 0)
            {
                for (int i = 0; i < dt_Attend.Rows.Count; i++)
                {
                    // Get Employee Profile
                    Query = "select ExistingCode,MachineID,EligibleCateen,EligibleIncentive,EligibleSnacks,Eligible_PF,";
                    Query = Query + "Eligible_ESI,BasicSalary,Allowance1amt,Allowance2amt,Deduction1amt,Deduction2amt,ConvenyAmt,SplAmt from Employee_Mst where EmpNo='" + dt_Attend.Rows[i]["EmpNo"].ToString() + "'";
                    dt_Employee = ManualAttend.GetDetails(Query);
                    if (dt_Employee.Rows.Count > 0)
                    {

                        //Fill Attendance
                        EmpNo = dt_Attend.Rows[i]["EmpNo"].ToString();
                        Days = dt_Attend.Rows[i]["Days"].ToString();
                        Months = dt_Attend.Rows[i]["Months"].ToString();
                        FinancialYear = dt_Attend.Rows[i]["FinancialYear"].ToString();
                        NFh = dt_Attend.Rows[i]["NFh"].ToString();
                        NFH_Work_Days_Statutory = dt_Attend.Rows[i]["NFH_Work_Days_Statutory"].ToString();
                        WH_Work_Present = dt_Attend.Rows[i]["WH_Work_Present"].ToString();
                        weekoff = dt_Attend.Rows[i]["weekoff"].ToString();
                        OTHoursNew = dt_Attend.Rows[i]["OTHoursNew"].ToString();

                        //Fill Profile
                        ExisistingCode = dt_Employee.Rows[0]["ExistingCode"].ToString();
                        MachineID = dt_Employee.Rows[0]["MachineID"].ToString();
                        EligibleCateen = dt_Employee.Rows[0]["EligibleCateen"].ToString();
                        EligibleIncentive = dt_Employee.Rows[0]["EligibleIncentive"].ToString();
                        EligibleSnacks = dt_Employee.Rows[0]["EligibleSnacks"].ToString();
                        Eligible_PF = dt_Employee.Rows[0]["Eligible_PF"].ToString();
                        Eligible_ESI = dt_Employee.Rows[0]["Eligible_ESI"].ToString();
                        BasicSalary = dt_Employee.Rows[0]["BasicSalary"].ToString();
                        Allowance1amt = dt_Employee.Rows[0]["Allowance1amt"].ToString();
                        Allowance2amt = dt_Employee.Rows[0]["Allowance2amt"].ToString();
                        Deduction1amt = dt_Employee.Rows[0]["ExistingCode"].ToString();
                        Deduction2amt = dt_Employee.Rows[0]["Deduction2amt"].ToString();
                        ConvenyAmt = dt_Employee.Rows[0]["ConvenyAmt"].ToString();
                        SplAmt = dt_Employee.Rows[0]["SplAmt"].ToString();

                        //Get Manual Dedection
                        Query = "";
                        Query = Query + "select Allowance1,Allowance2,Allowance3,Deduction1,Deduction2,Deduction3,CanteenDays from ManDeduction_Details ";
                        Query = Query + "Where convert(datetime,FromDate,103)=convert(datetime,'" + FromDate + "') and convert(datetime,ToDate,103)=convert(datetime,'" + ToDate + "') ";
                        Query = Query + "and CompCode='" + Session["SessionCcode"].ToString() + "' and LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";


                        if (WagesType == "1")
                        {
                                                          // Staff Salary //
                            //Basic Calc
                            basic1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(Fixed_Work_Days)).ToString();
                            basic = (Convert.ToDecimal(basic1) * (Convert.ToDecimal(Days))).ToString();
                            basic = (Math.Round(Convert.ToDecimal(basic), 0, MidpointRounding.AwayFromZero)).ToString();

                            //HRA Calculate
                            HRA1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(26)).ToString();
                            HRA_Str = (Convert.ToDecimal(HRA1) * (Convert.ToDecimal(Days))).ToString();


                            //Spl. Allowence Calculate
                            SplAllow1 = (Convert.ToDecimal(BasicSalary) / Convert.ToDecimal(26)).ToString();
                            Spl_Allow_Str = (Convert.ToDecimal(SplAllow1) * (Convert.ToDecimal(Days))).ToString();

                            
                            //Get Fixed Allowance and Deduction
                            All1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance1amt)).ToString();
                            All1 = (Math.Round(Convert.ToDecimal(All1), 0, MidpointRounding.AwayFromZero)).ToString();
                           
                            All2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Allowance2amt)).ToString();
                            All2 = (Math.Round(Convert.ToDecimal(All2), 0, MidpointRounding.AwayFromZero)).ToString();
                            
                            Ded1 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction1amt)).ToString();
                            Ded1 = (Math.Round(Convert.ToDecimal(Ded1), 0, MidpointRounding.AwayFromZero)).ToString();
                           
                            ded2 = ((Convert.ToDecimal(Days)) * Convert.ToDecimal(Deduction2amt)).ToString();
                            ded2 = (Math.Round(Convert.ToDecimal(ded2), 0, MidpointRounding.AwayFromZero)).ToString();


                          


                            //PF Calculation
                            if (Eligible_PF == "1")
                            {
                                PF_salary = "1";
                                if (Convert.ToDecimal(basic) >= Convert.ToDecimal(StaffSalary))
                                {
                                    PFS = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(PF_per)) / 100).ToString();
                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                    Employer_PF = ((Convert.ToDecimal(StaffSalary) * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                }
                                else
                                {
                                    decimal PF_val = (Convert.ToDecimal(basic));
                                    PFS = ((PF_val * Convert.ToDecimal(PF_per)) / 100).ToString();
                                    PFS = (Math.Round(Convert.ToDecimal(PFS), 0, MidpointRounding.AwayFromZero)).ToString();

                                    Employer_PF = ((PF_val * Convert.ToDecimal(EmployeerPFone)) / 100).ToString();
                                    Employer_PF = (Math.Round(Convert.ToDecimal(Employer_PF), 0, MidpointRounding.AwayFromZero)).ToString();
                                }
                            }
                            else
                            {
                                PFS = "0";
                                Employer_PF = "0";
                            }


                            //ESI Calculation
                            if (Eligible_ESI == "1")
                            {
                                decimal ESI_val = (Convert.ToDecimal(basic) + Convert.ToDecimal(HRA_Str) + Convert.ToDecimal(Spl_Allow_Str));
                                ESI = ((ESI_val * Convert.ToDecimal(ESI_per)) / 100).ToString();
                                ESI = (Math.Round(Convert.ToDecimal(ESI), 1, MidpointRounding.ToEven)).ToString();

                                Employer_ESI = ((ESI_val * Convert.ToDecimal(Employer_ESI)) / 100).ToString();
                                Employer_ESI = (Math.Round(Convert.ToDecimal(Employer_ESI), 1, MidpointRounding.ToEven)).ToString();

                                //Employee ESI Round
                                string[] split_val = ESI.Split('.');
                                string Ist = split_val[0].ToString();
                                string IInd = split_val[1].ToString();

                                if (Convert.ToInt32(IInd) > 0)
                                {
                                    ESI = (Convert.ToInt32(Ist) + 1).ToString();
                                }
                                else
                                {
                                    ESI = Ist;
                                }

                                //Employeer ESI Round
                                string[] Emp_split_val = Employer_ESI.Split('.');
                                string Emp_Ist = Emp_split_val[0].ToString();
                                string Emp_IInd = Emp_split_val[1].ToString();

                                if (Convert.ToInt32(Emp_IInd) > 0)
                                {
                                    Employer_ESI = (Convert.ToInt32(Emp_Ist) + 1).ToString();
                                }
                                else
                                {
                                    Employer_ESI = Emp_Ist;
                                }

                            }
                            else
                            {
                                ESI = "0";
                                Employer_ESI = "0";
                            }



                            TotalEarnings = (Convert.ToDecimal(basic) + Convert.ToDecimal(All1) + Convert.ToDecimal(All2) + Convert.ToDecimal(All3) + Convert.ToDecimal(Al4) + Convert.ToDecimal(0) + Convert.ToDecimal(All5)).ToString();
                            TotalEarnings = (Math.Round(Convert.ToDecimal(TotalEarnings), 2, MidpointRounding.ToEven)).ToString();
                            TotalDeductions = (Convert.ToDecimal(PFS) + Convert.ToDecimal(ESI) + Convert.ToDecimal(Advance) + Convert.ToDecimal(stamp) + Convert.ToDecimal(0) + Convert.ToDecimal(Ded1) + Convert.ToDecimal(ded2) + Convert.ToDecimal(ded3) + Convert.ToDecimal(ded4) + Convert.ToDecimal(ded5)).ToString();
                            TotalDeductions = (Math.Round(Convert.ToDecimal(TotalDeductions), 2, MidpointRounding.ToEven)).ToString();
                            NetPay = (Convert.ToDecimal(TotalEarnings) - Convert.ToDecimal(TotalDeductions)).ToString();
                            NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();


                        }
                        else
                        {
                            //Labour Salary
                        }



      

                    }



                }
                
            }
            else
            {
                Status = "Error";
            }

            return Json(Status, JsonRequestBehavior.AllowGet);
        }

    }
}