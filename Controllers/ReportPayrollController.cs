﻿using AttendanceMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace AttendanceMVC.Controllers
{
    public class ReportPayrollController : Controller
    {
         string Ccode = "";
         string Lcode = "";
         string SessionCcode = "";
         string SessionLcode = "";
         string SessionUserName;
         string SessionUserID;
        System.Web.UI.WebControls.DataGrid grid =
                          new System.Web.UI.WebControls.DataGrid();

        string SSQL = "";
        // GET: ReportPayroll
        public ActionResult Voucher()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult PaySlip()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public JsonResult GetEmployeeType()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "Select EmpTypeCd,EmpType,CASE when EmpCategory='1' then 'Staff' else 'Labour' END as EmpCategory from MstEmployeeType ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["EmpTypeCd"].ToString();
                list.Name = dt.Rows[i]["EmpType"].ToString();
                list.Category = dt.Rows[i]["EmpCategory"].ToString();
                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCategoryType()
        {
            DataTable dt = new DataTable();
            //DataTable dt1 = new DataTable();

            List<MasterDate> PartyList = new List<MasterDate>();


            string SQL = "select CateID,CateName from MstCategory"; //where LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "' ";
            dt = MasterDate.GetDetails(SQL);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MasterDate list = new MasterDate();

                list.ID = dt.Rows[i]["CateID"].ToString();
                list.Name = dt.Rows[i]["CateName"].ToString();
                //list.Category = dt.Rows[i]["EmpCategory"].ToString();
                PartyList.Add(list);
            }

            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FinancialYear1()
        {
            List<MasterDate> PartyList = new List<MasterDate>();
            int currentYear = DateTime.Now.Year;
            for (int i = 0; i < 10; i++)
            {
                MasterDate list = new MasterDate();
                list.ID = currentYear.ToString();
                list.Name = (currentYear.ToString() + "-" + Convert.ToString(currentYear + 1)).ToString();
                currentYear = currentYear - 1;
                PartyList.Add(list);
            }
            return Json(PartyList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult VoucherDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {

            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                GetVoucherDetails(FromDate, ToDate, WagesType, FinYear, Months);

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        public ActionResult IncentiveDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {

            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                GetIncentiveDetails(FromDate, ToDate, WagesType, FinYear, Months);

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        public void GetIncentiveDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {
            DataTable DT = new DataTable();
            DataTable AutoDTable = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("NAME");
            AutoDTable.Columns.Add("DEPT");
            AutoDTable.Columns.Add("WorkDays");
            AutoDTable.Columns.Add("AMOUNT");


            SSQL = "select (SD.EmpNo) as EmpNo,(ED.FirstName) as NAME,(DM.DeptName) as DEPT,(SD.WorkedDays) as WorkDays,(SD.DayIncentive) as AMOUNT from SalaryDetails SD inner join Employee_Mst ED on ED.EmpNo=SD.EmpNo and ED.LocCode=SD.Lcode ";
            SSQL = SSQL + "inner join Department_Mst DM on DM.DeptCode=ED.DeptName and DM.LocCode=ED.LocCode";
            SSQL = SSQL + " where ED.CompCode='" + Session["SessionCcode"].ToString() + "' And ED.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            SSQL = SSQL + " And DM.CompCode='" + Session["SessionCcode"].ToString() + "' And DM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            SSQL = SSQL + " And SD.Ccode='" + Session["SessionCcode"].ToString() + "' And SD.Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "' and SD.DayIncentive<>'0'";
            
            if (WagesType != "0")
            {
                SSQL = SSQL + " And ED.Wages='" + WagesType + "'";
                SSQL = SSQL + " And SD.Wagestype='" + WagesType + "'";
            }
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(datetime,SD.FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                SSQL = SSQL + " And CONVERT(datetime,SD.ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            }
            if (FinYear != "0")
            {
                SSQL = SSQL + " And SD.FinancialYear='" + FinYear + "'";
            }
            if (Months != "0")
            {
                SSQL = SSQL + " And SD.Month='" + Months + "'";
            }

            SSQL = SSQL + " order by DM.DeptName asc ";

            DT = ManualAttend.GetDetails(SSQL);

            double NetAmt = 0;


            double count = 0;
            int SNo = 1;

            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SNo"] = SNo;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = DT.Rows[i]["EmpNo"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NAME"] = DT.Rows[i]["NAME"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = DT.Rows[i]["DEPT"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WorkDays"] = DT.Rows[i]["WorkDays"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AMOUNT"] = DT.Rows[i]["AMOUNT"].ToString();

                NetAmt = NetAmt + Convert.ToDouble(DT.Rows[i]["AMOUNT"].ToString());

                count = count + 1;
                SNo = SNo + 1;
            }

            if (count > 0)
            {

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = "";
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NAME"] = "";
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = "";
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WorkDays"] = "Total";
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AMOUNT"] = NetAmt;

            }

            else
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = "";
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NAME"] = "";
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DEPT"] = "";
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WorkDays"] = "";
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AMOUNT"] = "";
            }


            NetAmt = 0;


            DataTable DT_Comp = new DataTable();
            string CompName = "";
            SSQL = "Select * from Company_Mst where CompCode='" + Session["SessionCcode"].ToString() + "'";
            DT_Comp = ManualAttend.GetDetails(SSQL);

            if (DT_Comp.Rows.Count != 0)
            {
                CompName = DT_Comp.Rows[0]["CompName"].ToString();
            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=Incentive.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">" + CompName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + Request.Cookies["SessionLcode"].Value.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">INCENTIVE LIST FOR THE MONTH OF " + Months + " - " + FinYear + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
        public void GetVoucherDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {
            DataTable DT = new DataTable();

            SSQL = "Select EM.EmpNo as EmpNo,EM.FirstName as EmpName,DM.DeptName as Dept,CM.CateName as Type,SD.PerDaySal as SalperDay,EM.BasicSalary as BaseSalary,";
            SSQL = SSQL + "SD.WorkedDays as WorkDays,SD.Basic_SM as EarnedSalary,SD.OTHoursNew as OTHrs,SD.OTHoursAmtNew as OTSalary,SD.GrossEarnings as TotalEarnedSalary,";
            SSQL = SSQL + "SD.PfSalary as PF,SD.ESI,SD.Deduction3 as Medical,SD.Advance,SD.Deduction5 as LessCanteen,SD.ConvAllow as Conveyance,SD.Losspay as Spl,SD.TotalDeductions as TotDed,SD.NetPay as NetSalary,";
            SSQL = SSQL + "SD.RoundOffNetPay as NetAmt,SD.FromBankACno as AccNo from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo inner join Department_Mst DM";
            SSQL = SSQL + " on EM.DeptName=DM.DeptCode inner join MstCategory CM on EM.WageCategoty=CM.CateID";
            SSQL = SSQL + " where EM.CompCode='" + Session["SessionCcode"].ToString() + "' And EM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            //SSQL = SSQL + " And DM.CompCode='" + Session["SessionCcode"].ToString() + "' And DM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            SSQL = SSQL + " And SD.Ccode='" + Session["SessionCcode"].ToString() + "' And SD.Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            //SSQL = SSQL + " And CM.CompCode='" + Session["SessionCcode"].ToString() + "' And CM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            if (WagesType != "0")
            {
                SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
                SSQL = SSQL + " And SD.Wagestype='" + WagesType + "'";
            }
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(datetime,SD.FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                SSQL = SSQL + " And CONVERT(datetime,SD.ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            }
            if (FinYear != "0")
            {
                SSQL = SSQL + " And SD.FinancialYear='" + FinYear + "'";
            }
            if (Months != "0")
            {
                SSQL = SSQL + " And SD.Month='" + Months + "'";
            }

            DT = ManualAttend.GetDetails(SSQL);


            DataTable DT_Comp = new DataTable();
            string CompName = "";
            SSQL = "Select *from Company_Mst where CompCode='" + Session["SessionCcode"].ToString() + "'";
            DT_Comp = ManualAttend.GetDetails(SSQL);

            if (DT_Comp.Rows.Count != 0)
            {
                CompName = DT_Comp.Rows[0]["CompName"].ToString();
            }

            grid.DataSource = DT;
            grid.DataBind();
            string attachment = "attachment;filename=SALARY LIST.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='21'>");
            Response.Write("<a style=\"font-weight:bold\">" + CompName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + Request.Cookies["SessionLcode"].Value.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='21'>");
            Response.Write("<a style=\"font-weight:bold\">SALARY LIST &nbsp;&nbsp;&nbsp;</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='21'>");
            Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
        public ActionResult PaySlipDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months, string CatName)
        {

            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                GetPaySlipDetails(FromDate, ToDate, WagesType, FinYear, Months, CatName);

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }


        public ActionResult PaySlipDetailsNew(string FromDate, string ToDate, string WagesType, string FinYear, string Months, string CatName)
        {

            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                GetPaySlipDetailsNew(FromDate, ToDate, WagesType, FinYear, Months, CatName);

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        public void GetPaySlipDetailsNew(string FromDate, string ToDate, string WagesType, string FinYear, string Months, string CatName)
        {

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            string SSQL = "";
            SSQL = "Select EM.EmpNo,isnull(EM.FirstName,'') as FirstName,isnull(DM.DeptName,'') As DeptName,isnull(DS.DesignName,'') As DesignName,EM.AccountNo as FromBankACno,";
            SSQL = SSQL + "SD.Fixed_Work_Days,SD.BasicandDA,SD.ESI,SD.LeaveDays,SD.Advance,SD.MediAllow,SD.allowances3,SD.Month,SD.FinancialYear,";
            SSQL = SSQL + "SD.WorkedDays,EM.BaseSalary,MG.GradeName,MC.CateName,SD.allowances4,SD.allowances5,SD.Deduction3,SD.Deduction4,SD.Deduction5,";
            SSQL = SSQL + "SD.Messdeduction,SD.GrossEarnings,SD.TotalDeductions,SD.RoundOffNetPay,SD.NetPay,SD.OTHoursNew,SD.Wagestype,";
            SSQL = SSQL + "EM.DOJ,SD.ProvidentFund,SD.HRA,SD.ConvAllow,isnull(SD.PerDaySal,'0') As PerDaySal,SD.Losspay,SD.WashingAllow,SD.Conveyance";
            SSQL = SSQL + " from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo";
            SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
            SSQL = SSQL + " inner join Designation_Mst DS on EM.Designation = DS.DesignNo";
            SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
            SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
            SSQL = SSQL + " where EM.CompCode = '" + Session["SessionCcode"].ToString() + "' And EM.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "' And SD.Ccode = '" + Session["SessionCcode"].ToString() + "' And SD.Lcode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            //SSQL = SSQL + " And DM.CompCode = '" + Session["SessionCcode"].ToString() + "' And DM.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "' And MG.CompCode = '" + Session["SessionCcode"].ToString() + "' And MG.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "' ";
            //SSQL = SSQL + " And MC.CompCode = '" + Session["SessionCcode"].ToString() + "' And MC.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            if (FromDate != "")
            {
                SSQL = SSQL + " And Convert(datetime,SD.FromDate,120) = Convert(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }

            if (ToDate != "")
            {
                SSQL = SSQL + " And Convert(datetime,SD.ToDate,120) = Convert(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            }

            if (WagesType != "0")
            {
                SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
            }
            if (CatName != "0")
            {
                SSQL = SSQL + " And EM.WageCategoty='" + CatName + "'";
            }


            if (FinYear != "0")
            {
                SSQL = SSQL + " And SD.FinancialYear='" + FinYear + "'";
            }

            if (Months != "0")
            {
                SSQL = SSQL + " And SD.Month='" + Months + "'";
            }

            SSQL = SSQL + "";

            dt1 = ManualAttend.GetDetails(SSQL);

            SSQL = "";
            SSQL = "select CompName from Company_Mst where CompCode = '" + Session["SessionCcode"].ToString() + "'";
            dt = ManualAttend.GetDetails(SSQL);

            Document document = new Document(PageSize.A4, 35, 25, 40, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(4);
            float[] widths = new float[] { 50f, 50f, 50f, 50f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(4);
            float[] widths1 = new float[] { 50f, 50f, 50f, 50f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            if (dt1.Rows.Count > 0)
            {

                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    cell = PhraseCell(new Phrase("" + dt.Rows[0]["CompName"].ToString()+ " - " + Request.Cookies["SessionLcode"].Value.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 4;
                    cell.PaddingTop = 25f;
                    cell.PaddingBottom = 7f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    //cell = PhraseCell(new Phrase("" + Request.Cookies["SessionLcode"].Value.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    //cell.Border = 0;
                    //cell.Colspan = 4;
                    //cell.PaddingTop = 5f;
                    //cell.PaddingBottom = 5f;
                    //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //table.AddCell(cell);

                    cell = PhraseCell(new Phrase("PAYSLIP FOR THE PERIOD OF " + dt1.Rows[i]["Month"].ToString() + "," + dt1.Rows[i]["FinancialYear"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 4;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 4f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 2;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 4f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 2;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 4f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 7, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 4;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Employee ID", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 2f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(": " + dt1.Rows[i]["EmpNo"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 2f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Eployee Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(": " + dt1.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Department", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(": " + dt1.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border =  Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Designation : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(": " + dt1.Rows[i]["DesignName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Date Of Joining", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(": " + dt1.Rows[i]["DOJ"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("PF Account Number", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(": " , new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Days Worked", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(": " + dt1.Rows[i]["WorkedDays"].ToString() , new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("ESI Account Number", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(":", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Bank Acct/Cheque Number", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(": " + dt1.Rows[i]["FromBankACno"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Father's/Husband's Name", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Earned Leave", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Casual Leave", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border =  Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);
 
                    string Wages = dt1.Rows[i]["Wagestype"].ToString();

                    
                 
                

                    cell = PhraseCell(new Phrase("Earnings", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Amount", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Deductions", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Amount", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Basic Pay", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["BasicandDA"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("EPF / ESI", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["ProvidentFund"].ToString() + " / " + dt1.Rows[i]["ESI"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("HRA", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["HRA"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Lodging / Amenities", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Deduction3"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Night Shift Inc", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border =  Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["allowances3"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Advance / Medical", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Advance"].ToString() + " / " + dt1.Rows[i]["MediAllow"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Washing Allow", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["WashingAllow"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Others-Spl", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Losspay"].ToString() /*dt1.Rows[i]["Deduction4"].ToString()*/, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Conveyance", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Conveyance"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Others-1", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["ConvAllow"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Others", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["allowances5"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Canteen", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Deduction5"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 3f;
                    cell.PaddingBottom = 3f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total Earnings", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["GrossEarnings"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("Total Deductions", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["TotalDeductions"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                   

                    double Values = Convert.ToDouble(dt1.Rows[i]["RoundOffNetPay"].ToString()) - Convert.ToDouble(dt1.Rows[i]["NetPay"].ToString());

                    cell = PhraseCell(new Phrase("Round Off : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + Values, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    

                  

                    cell = PhraseCell(new Phrase("Net Pay : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["RoundOffNetPay"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("HRD ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 25f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("FM", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                    cell.PaddingTop = 25f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("SM", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                    cell.PaddingTop = 25f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("EMPLOYEE SIGN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 25f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    table.SpacingAfter = 10f;


                }

            }
            else
            {
                cell = PhraseCell(new Phrase("" + dt.Rows[0]["CompName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + Request.Cookies["SessionLcode"].Value.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("PAYSLIP FOR THE PERIOD OF " + Months + "," + FinYear, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 2;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Data Not Found ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);
            }

            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=PAY_SLIP.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();

        }


        public void GetPaySlipDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months, string CatName)
        {

            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();

            string SSQL = "";
            SSQL = "Select EM.EmpNo,isnull(EM.FirstName,'') as FirstName,isnull(DM.DeptName,'') As DeptName,isnull(DS.DesignName,'') As DesignName,EM.AccountNo as FromBankACno,";
            SSQL = SSQL + "SD.Fixed_Work_Days,SD.BasicandDA,SD.ESI,SD.LeaveDays,SD.Advance,SD.MediAllow,SD.allowances3,SD.Month,SD.FinancialYear,";
            SSQL = SSQL + "SD.WorkedDays,EM.BaseSalary,MG.GradeName,MC.CateName,SD.allowances4,SD.allowances5,SD.Deduction3,SD.Deduction4,SD.Deduction5,";
            SSQL = SSQL + "SD.Messdeduction,SD.GrossEarnings,SD.TotalDeductions,SD.RoundOffNetPay,SD.NetPay,SD.OTHoursNew,SD.Wagestype,";
            SSQL = SSQL + "EM.DOJ,SD.ProvidentFund,SD.HRA,SD.ConvAllow,isnull(SD.PerDaySal,'0') As PerDaySal,SD.Losspay,SD.WashingAllow,SD.Conveyance";
            SSQL = SSQL + " from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo";
            SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
            SSQL = SSQL + " inner join Designation_Mst DS on EM.Designation = DS.DesignNo";
            SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
            SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
            SSQL = SSQL + " where EM.CompCode = '" + Session["SessionCcode"].ToString() + "' And EM.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "' And SD.Ccode = '" + Session["SessionCcode"].ToString() + "' And SD.Lcode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            //SSQL = SSQL + " And DM.CompCode = '" + Session["SessionCcode"].ToString() + "' And DM.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "' And MG.CompCode = '" + Session["SessionCcode"].ToString() + "' And MG.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "' ";
            //SSQL = SSQL + " And MC.CompCode = '" + Session["SessionCcode"].ToString() + "' And MC.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            if (FromDate != "")
            {
                SSQL = SSQL + " And Convert(datetime,SD.FromDate,120) = Convert(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
            }

            if (ToDate != "")
            {
                SSQL = SSQL + " And Convert(datetime,SD.ToDate,120) = Convert(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
            }

            if (WagesType != "0")
            {
                SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
            }
            if (CatName != "0")
            {
                SSQL = SSQL + " And EM.WageCategoty='" + CatName + "'";
            }


            if (FinYear != "0")
            {
                SSQL = SSQL + " And SD.FinancialYear='" + FinYear + "'";
            }

            if (Months != "0")
            {
                SSQL = SSQL + " And SD.Month='" + Months + "'";
            }

            SSQL = SSQL + "";

            dt1 = ManualAttend.GetDetails(SSQL);

            SSQL = "";
            SSQL = "select CompName from Company_Mst where CompCode = '" + Session["SessionCcode"].ToString() + "'";
            dt = ManualAttend.GetDetails(SSQL);

            Document document = new Document(PageSize.A4, 35, 25, 40, 40);
            Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

            writer.PageEvent = new HeaderFooter();
            document.Open();


            PdfPTable table = new PdfPTable(4);
            float[] widths = new float[] { 50f, 50f, 50f, 50f };
            table.SetWidths(widths);
            table.WidthPercentage = 100;

            PdfPTable table1 = new PdfPTable(4);
            float[] widths1 = new float[] { 50f, 50f, 50f, 50f };
            table1.SetWidths(widths1);
            table1.WidthPercentage = 100;

            PdfPTable table2 = new PdfPTable(4);
            float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
            table2.SetWidths(widths2);
            table2.WidthPercentage = 100;

            PdfPCell cell;

            if (dt1.Rows.Count > 0)
            {

                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    cell = PhraseCell(new Phrase("" + dt.Rows[0]["CompName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 4;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + Request.Cookies["SessionLcode"].Value.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 4;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("PAYSLIP FOR THE MONTH OF " + dt1.Rows[i]["Month"].ToString() + "," + dt1.Rows[i]["FinancialYear"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 4;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 2;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 2;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                    cell.Border = 0;
                    cell.Colspan = 4;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("EMPLOYEE ID : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["EmpNo"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("EMPLOYEE NAME : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("DEPARTMENT : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("WORKED DAYS / Extra Hours : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["WorkedDays"].ToString() + " / " + dt1.Rows[i]["OTHoursNew"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("DESIG  : " + dt1.Rows[i]["DesignName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("GRADE : " + dt1.Rows[i]["GradeName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    string Wages = dt1.Rows[i]["Wagestype"].ToString();

                    if (Wages.Trim() == "1")
                    {
                        //double OndaySalary = Convert.ToDouble(dt1.Rows[i]["BaseSalary"].ToString()) / Convert.ToDouble(dt1.Rows[i]["Fixed_Work_Days"].ToString());

                        //OndaySalary = Math.Round(OndaySalary);

                        cell = PhraseCell(new Phrase("SALARY FIXED : " + dt1.Rows[i]["BaseSalary"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table.AddCell(cell);

                        cell = PhraseCell(new Phrase("D.SALARY : " + dt1.Rows[i]["PerDaySal"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table.AddCell(cell);
                    }
                    else
                    {
                        double Salary = Convert.ToDouble(dt1.Rows[i]["BaseSalary"].ToString()) * Convert.ToDouble(dt1.Rows[i]["WorkedDays"].ToString());

                        cell = PhraseCell(new Phrase("SALARY FIXED : " + Salary, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table.AddCell(cell);

                        cell = PhraseCell(new Phrase("D.SALARY : " + dt1.Rows[i]["PerDaySal"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                        cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                        cell.PaddingTop = 5f;
                        cell.PaddingBottom = 5f;
                        cell.HorizontalAlignment = Element.ALIGN_LEFT;
                        table.AddCell(cell);
                    }

                    cell = PhraseCell(new Phrase("DOJ : " + dt1.Rows[i]["DOJ"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("LEAVE : " + dt1.Rows[i]["LeaveDays"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("A/C NO : " + dt1.Rows[i]["FromBankACno"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("TYPE :  " + dt1.Rows[i]["CateName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("EARNING", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("AMOUNT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("DEDUCTION", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("AMOUNT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("BASIC (CONSOLIDATED) : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["BasicandDA"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("EPF / ESI : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["ProvidentFund"].ToString() + " / " + dt1.Rows[i]["ESI"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("HRA : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["HRA"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("LODGING / AMENITIES : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Deduction3"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("NIGHT SHIFT INC : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["allowances3"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("ADVANCE / MEDICAL : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Advance"].ToString() + " / " + dt1.Rows[i]["MediAllow"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("WASHING ALLOW : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["WashingAllow"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("OTHERS-SPL : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Losspay"].ToString() /*dt1.Rows[i]["Deduction4"].ToString()*/, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("CONVEYANCE : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Conveyance"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("OTHERS-1 : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["ConvAllow"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("OTHERS : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["allowances5"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("CANTEEN : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Deduction5"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("TOTAL : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["GrossEarnings"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("TOTAL : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["TotalDeductions"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    double Values = Convert.ToDouble(dt1.Rows[i]["RoundOffNetPay"].ToString()) - Convert.ToDouble(dt1.Rows[i]["NetPay"].ToString());

                    cell = PhraseCell(new Phrase("ROUND OFF : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + Values, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("NET PAY : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("" + dt1.Rows[i]["RoundOffNetPay"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("HRD ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("FM", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("SM", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    cell = PhraseCell(new Phrase("EMPLOYEE SIGN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
                    cell.PaddingTop = 5f;
                    cell.PaddingBottom = 5f;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(cell);

                    table.SpacingAfter = 10f;


                }

            }
            else
            {
                cell = PhraseCell(new Phrase("" + dt.Rows[0]["CompName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("" + Request.Cookies["SessionLcode"].Value.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("PAYSLIP FOR THE MONTH OF " + Months + "," + FinYear, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 3;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 2;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.TOP_BORDER;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase("Data Not Found ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = 0;
                cell.Colspan = 5;
                cell.PaddingTop = 5f;
                cell.PaddingBottom = 5f;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
                cell.Border = Rectangle.BOTTOM_BORDER;
                cell.Colspan = 5;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);
            }

            table.SpacingBefore = 10f;
            table.SpacingAfter = 10f;

            document.Add(table);

            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=PAY_SLIP.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();

        }

        //public void GetPaySlipDetails(string FromDate, string ToDate, string WagesType, string FinYear, string Months,string CatName)
        //{

        //    DataTable dt = new DataTable();
        //    DataTable dt1 = new DataTable();

        //    string SSQL = "";
        //    SSQL = "Select EM.EmpNo,isnull(EM.FirstName,'') as FirstName,isnull(DM.DeptName,'') As DeptName,isnull(DS.DesignName,'') As DesignName,";
        //    SSQL = SSQL + "SD.Fixed_Work_Days,SD.BasicandDA,SD.ESI,SD.LeaveDays,SD.Advance,SD.MediAllow,SD.allowances3,SD.Month,SD.FinancialYear,";
        //    SSQL = SSQL + "SD.WorkedDays,EM.BaseSalary,MG.GradeName,MC.CateName,SD.allowances4,SD.allowances5,SD.Deduction3,SD.Deduction4,SD.Deduction5,";
        //    SSQL = SSQL + "SD.Messdeduction,SD.FromBankACno,SD.GrossEarnings,SD.TotalDeductions,SD.RoundOffNetPay,SD.NetPay,SD.OTHoursNew,SD.Wagestype,";
        //    SSQL = SSQL + "EM.DOJ,SD.ProvidentFund,SD.HRA,SD.ConvAllow,isnull(SD.PerDaySal,'0') As PerDaySal,SD.Losspay,SD.WashingAllow,SD.Conveyance";
        //    SSQL = SSQL + " from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo";
        //    SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
        //    SSQL = SSQL + " inner join Designation_Mst DS on EM.Designation = DS.DesignNo";
        //    SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
        //    SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
        //    SSQL = SSQL + " where EM.CompCode = '" + Session["SessionCcode"].ToString() + "' And EM.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "' And SD.Ccode = '" + Session["SessionCcode"].ToString() + "' And SD.Lcode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
        //    SSQL = SSQL + " And DM.CompCode = '" + Session["SessionCcode"].ToString() + "' And DM.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "' And MG.CompCode = '" + Session["SessionCcode"].ToString() + "' And MG.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "' ";
        //    SSQL = SSQL + " And MC.CompCode = '" + Session["SessionCcode"].ToString() + "' And MC.LocCode = '" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
        //    if (FromDate != "")
        //    {
        //        SSQL = SSQL + " And Convert(datetime,SD.FromDate,120) = Convert(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
        //    }

        //    if (ToDate != "")
        //    {
        //        SSQL = SSQL + " And Convert(datetime,SD.ToDate,120) = Convert(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
        //    }

        //    if (WagesType != "0")
        //    {
        //        SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
        //    }
        //    if (CatName != "0")
        //    {
        //        SSQL = SSQL + " And EM.WageCategoty='" + CatName + "'";
        //    }


        //    if (FinYear != "0")
        //    {
        //        SSQL = SSQL + " And SD.FinancialYear='" + FinYear + "'";
        //    }

        //    if (Months != "0")
        //    {
        //        SSQL = SSQL + " And SD.Month='" + Months + "'";
        //    }

        //    SSQL = SSQL + "";

        //    dt1 = ManualAttend.GetDetails(SSQL);

        //    SSQL = "";
        //    SSQL = "select CompName from Company_Mst where CompCode = '" + Session["SessionCcode"].ToString() + "'";
        //    dt = ManualAttend.GetDetails(SSQL);

        //    Document document = new Document(PageSize.A4, 35, 25, 40, 40);
        //    Font NormalFont = FontFactory.GetFont("Times New Roman", 12, Font.NORMAL);
        //    System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
        //    PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);

        //    writer.PageEvent = new HeaderFooter();
        //    document.Open();


        //    PdfPTable table = new PdfPTable(4);
        //    float[] widths = new float[] { 50f, 50f, 50f, 50f };
        //    table.SetWidths(widths);
        //    table.WidthPercentage = 100;

        //    PdfPTable table1 = new PdfPTable(4);
        //    float[] widths1 = new float[] { 50f, 50f, 50f, 50f };
        //    table1.SetWidths(widths1);
        //    table1.WidthPercentage = 100;

        //    PdfPTable table2 = new PdfPTable(4);
        //    float[] widths2 = new float[] { 50f, 50f, 50f, 50f };
        //    table2.SetWidths(widths2);
        //    table2.WidthPercentage = 100;

        //    PdfPCell cell;

        //    if (dt1.Rows.Count > 0)
        //    {

        //        for (int i = 0; i < dt1.Rows.Count; i++)
        //        {
        //            cell = PhraseCell(new Phrase("" + dt.Rows[0]["CompName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = 0;
        //            cell.Colspan = 4;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + Request.Cookies["SessionLcode"].Value.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = 0;
        //            cell.Colspan = 4;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("PAYSLIP FOR THE MONTH OF " + dt1.Rows[i]["Month"].ToString() + "," + dt1.Rows[i]["FinancialYear"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = 0;
        //            cell.Colspan = 4;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = 0;
        //            cell.Colspan = 2;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = 0;
        //            cell.Colspan = 2;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = 0;
        //            cell.Colspan = 4;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("EMPLOYEE ID : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["EmpNo"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("EMPLOYEE NAME : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["FirstName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("DEPARTMENT : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["DeptName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("WORKED DAYS / Extra Hours : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["WorkedDays"].ToString() + " / " + dt1.Rows[i]["OTHoursNew"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("DESIG  : " + dt1.Rows[i]["DesignName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("GRADE : " + dt1.Rows[i]["GradeName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            string Wages = dt1.Rows[i]["Wagestype"].ToString();

        //            if (Wages.Trim() == "1")
        //            {
        //                //double OndaySalary = Convert.ToDouble(dt1.Rows[i]["BaseSalary"].ToString()) / Convert.ToDouble(dt1.Rows[i]["Fixed_Work_Days"].ToString());

        //                //OndaySalary = Math.Round(OndaySalary);

        //                cell = PhraseCell(new Phrase("SALARY FIXED : " + dt1.Rows[i]["BaseSalary"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //                table.AddCell(cell);

        //                cell = PhraseCell(new Phrase("D.SALARY : " + dt1.Rows[i]["PerDaySal"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //                table.AddCell(cell);
        //            }
        //            else
        //            {
        //                double Salary = Convert.ToDouble(dt1.Rows[i]["BaseSalary"].ToString()) * Convert.ToDouble(dt1.Rows[i]["WorkedDays"].ToString());

        //                cell = PhraseCell(new Phrase("SALARY FIXED : " + Salary, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //                table.AddCell(cell);

        //                cell = PhraseCell(new Phrase("D.SALARY : " + dt1.Rows[i]["PerDaySal"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //                cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //                cell.PaddingTop = 5f;
        //                cell.PaddingBottom = 5f;
        //                cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //                table.AddCell(cell);
        //            }

        //            cell = PhraseCell(new Phrase("DOJ : " + dt1.Rows[i]["DOJ"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("LEAVE : " + dt1.Rows[i]["LeaveDays"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("A/C NO : " + dt1.Rows[i]["FromBankACno"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("TYPE :  " + dt1.Rows[i]["CateName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("EARNING", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("AMOUNT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("DEDUCTION", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("AMOUNT", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("BASIC (CONSOLIDATED) : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["BasicandDA"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("EPF / ESI : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["ProvidentFund"].ToString() + " / " + dt1.Rows[i]["ESI"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("HRA : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["HRA"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("LODGING / AMENITIES : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Deduction3"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("NIGHT SHIFT INC : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["allowances3"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("ADVANCE / MEDICAL : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Advance"].ToString() + " / " + dt1.Rows[i]["MediAllow"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("WASHING ALLOW : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["WashingAllow"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("OTHERS-SPL : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Losspay"].ToString() /*dt1.Rows[i]["Deduction4"].ToString()*/, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("CONVEYANCE : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Conveyance"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("OTHERS-1 : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Deduction5"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("OTHERS : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["allowances5"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("CANTEEN : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["Messdeduction"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("TOTAL : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["GrossEarnings"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("TOTAL : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["TotalDeductions"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            double Values = Convert.ToDouble(dt1.Rows[i]["RoundOffNetPay"].ToString()) - Convert.ToDouble(dt1.Rows[i]["NetPay"].ToString());

        //            cell = PhraseCell(new Phrase("ROUND OFF : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + Values, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("NET PAY : ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("" + dt1.Rows[i]["RoundOffNetPay"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("HRD ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.LEFT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("FM", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("SM", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            cell = PhraseCell(new Phrase("EMPLOYEE SIGN", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 9, iTextSharp.text.Font.NORMAL)), PdfPCell.ALIGN_LEFT);
        //            cell.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
        //            cell.PaddingTop = 5f;
        //            cell.PaddingBottom = 5f;
        //            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //            table.AddCell(cell);

        //            table.SpacingAfter = 10f;


        //        }

        //    }
        //    else
        //    {
        //        cell = PhraseCell(new Phrase("" + dt.Rows[0]["CompName"].ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = 0;
        //        cell.Colspan = 5;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table.AddCell(cell);

        //        cell = PhraseCell(new Phrase("" + Request.Cookies["SessionLcode"].Value.ToString(), new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = 0;
        //        cell.Colspan = 5;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table.AddCell(cell);

        //        cell = PhraseCell(new Phrase("PAYSLIP FOR THE MONTH OF " + Months + "," + FinYear, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = 0;
        //        cell.Colspan = 5;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table.AddCell(cell);

        //        cell = PhraseCell(new Phrase("From Date: " + FromDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = 0;
        //        cell.Colspan = 3;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
        //        table.AddCell(cell);

        //        cell = PhraseCell(new Phrase("To Date: " + ToDate, new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = 0;
        //        cell.Colspan = 2;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        //        table.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.TOP_BORDER;
        //        cell.Colspan = 5;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table.AddCell(cell);

        //        cell = PhraseCell(new Phrase("Data Not Found ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = 0;
        //        cell.Colspan = 5;
        //        cell.PaddingTop = 5f;
        //        cell.PaddingBottom = 5f;
        //        cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        //        table.AddCell(cell);

        //        cell = PhraseCell(new Phrase(" ", new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD)), PdfPCell.ALIGN_LEFT);
        //        cell.Border = Rectangle.BOTTOM_BORDER;
        //        cell.Colspan = 5;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        table.AddCell(cell);
        //    }

        //    table.SpacingBefore = 10f;
        //    table.SpacingAfter = 10f;

        //    document.Add(table);

        //    document.Close();

        //    byte[] bytes = memoryStream.ToArray();
        //    memoryStream.Close();
        //    Response.Clear();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("Content-Disposition", "attachment; filename=PAY_SLIP.pdf");
        //    Response.ContentType = "application/pdf";
        //    Response.Buffer = true;
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.BinaryWrite(bytes);
        //    Response.End();
        //    Response.Close();

        //}
        private static void DrawLine(PdfWriter writer, float x1, float y1, float x2, float y2)
        {
            PdfContentByte contentByte = writer.DirectContent;
            //contentByte.SetColorStroke(color);
            contentByte.MoveTo(x1, y1);
            contentByte.LineTo(x2, y2);
            contentByte.Stroke();
        }
        private static PdfPCell PhraseCell(Phrase phrase, int align)
        {
            PdfPCell cell = new PdfPCell(phrase);
            //cell.BorderColor = Color.WHITE;
            //cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            cell.HorizontalAlignment = align;
            cell.PaddingBottom = 2f;
            cell.PaddingTop = 0f;
            return cell;
        }

      
        public ActionResult SalaryList()
        {
            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

      
        public ActionResult SalaryListDetails(string CatName, string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {

            //if (Session["SessionCcode"] != null)
            //{
            //    SessionCcode = Session["SessionCcode"].ToString();
            //    SessionLcode = Session["SessionLcode"].ToString();
            //    SessionUserName = Session["Usernmdisplay"].ToString();
            //    SessionUserID = Session["UserId"].ToString();
            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {
                //SessionCcode = Request.Cookies["SessionCcode"].Value.ToString();

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                GetSalaryListDetails(CatName, FromDate, ToDate, WagesType, FinYear, Months);

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        public void GetSalaryListDetails(string CatName, string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {
            DataTable Category = new DataTable();
            DataTable AutoDTable = new DataTable();
            DataTable Salary = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("EmpName");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Days");
            AutoDTable.Columns.Add("BaseSalary");
            AutoDTable.Columns.Add("WorkDays");
            AutoDTable.Columns.Add("EarnedSalary");
            AutoDTable.Columns.Add("OTHrs");
            AutoDTable.Columns.Add("OTSalary");
            AutoDTable.Columns.Add("TEarnedSalary");
            AutoDTable.Columns.Add("PF");
            AutoDTable.Columns.Add("ESI");
            AutoDTable.Columns.Add("Medical");
            AutoDTable.Columns.Add("Advance");
            //AutoDTable.Columns.Add("Incentive");
            AutoDTable.Columns.Add("LessCanteen");
            AutoDTable.Columns.Add("Convey");
            AutoDTable.Columns.Add("Spl");
            AutoDTable.Columns.Add("TotDed");
            AutoDTable.Columns.Add("NetSalary");
            AutoDTable.Columns.Add("NetAmt");
            AutoDTable.Columns.Add("AccNo");
            AutoDTable.Columns.Add("Match");

            double TEarnedSalary = 0;
            double PF = 0;
            double ESI = 0;
            double Medical = 0;
            double Advance = 0;
            double Incentive = 0;
            double LessCanteen = 0;
            double Convey = 0;
            double Spl = 0;
            double TotDed = 0;
            double NetSalary = 0;
            double NetAmt = 0;
            double WorkDays = 0;
            double EarnedSalary = 0;
            double OtHours = 0;
            double OtAmoun = 0;

            SSQL = "";

            if (CatName == "1")
            {
                SSQL = "Select CateID,CateName from MstCategory"; //where CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                Category = ManualAttend.GetDetails(SSQL);
            }
            else if (CatName == "2")
            {

                SSQL = "select DeptCode,DeptName from Department_Mst"; //where CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "' order by ShortCode asc";
                Category = ManualAttend.GetDetails(SSQL);
            }
            else if (CatName == "3")
            {

                SSQL = "select AgentID,AgentName from  AgentMst where CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                Category = ManualAttend.GetDetails(SSQL);

            }

            string Name = "";
            string Number = "";

            for (int i = 0; i < Category.Rows.Count; i++)
            {
                //Name = "";

                if (CatName == "1")
                {
                    Name = Category.Rows[i]["CateID"].ToString();
                }
                else if (CatName == "2")
                {
                    Name = Category.Rows[i]["DeptCode"].ToString();
                }
                else if (CatName == "3")
                {
                    Name = Category.Rows[i]["AgentID"].ToString();

                }

                SSQL = "";
                SSQL = "Select EM.EmpNo as EmpNo,EM.FirstName as EmpName,DM.DeptName as Dept,CM.CateName as Type,SD.PerDaySal as SalperDay,EM.BasicSalary as BaseSalary,";
                SSQL = SSQL + "SD.WorkedDays as WorkDays,SD.Basic_SM as EarnedSalary,SD.OTHoursNew as OTHrs,SD.OTHoursAmtNew as OTSalary,SD.GrossEarnings as TotalEarnedSalary,";
                SSQL = SSQL + "SD.PfSalary as PF,SD.ESI,SD.Advance,(SD.Deduction3) as MediAllow,SD.DayIncentive as Incentive,SD.Deduction5 as LessCanteen,SD.ConvAllow as Conveyance,SD.Losspay as Spl,SD.TotalDeductions as TotDed,SD.NetPay as NetSalary,";
                SSQL = SSQL + "SD.RoundOffNetPay as NetAmt,EM.AccountNo as AccNo from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo inner join Department_Mst DM";
                SSQL = SSQL + " on EM.DeptName=DM.DeptCode inner join MstCategory CM on EM.WageCategoty=CM.CateID";
                SSQL = SSQL + " where EM.CompCode='" + Session["SessionCcode"].ToString() + "' And EM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                //SSQL = SSQL + " And DM.CompCode='" + Session["SessionCcode"].ToString() + "' And DM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                SSQL = SSQL + " And SD.Ccode='" + Session["SessionCcode"].ToString() + "' And SD.Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                //SSQL = SSQL + " And CM.CompCode='" + Session["SessionCcode"].ToString() + "' And CM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";

                if (WagesType != "0")
                {
                    SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
                    SSQL = SSQL + " And SD.Wagestype='" + WagesType + "'";
                }


                if (FromDate != "" && ToDate != "")
                {
                    SSQL = SSQL + " And CONVERT(datetime,SD.FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                    SSQL = SSQL + " And CONVERT(datetime,SD.ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                }

                if (CatName == "1")
                {
                    SSQL = SSQL + " And EM.WageCategoty ='" + Name + "'";
                }
                else if (CatName == "2")
                {
                    SSQL = SSQL + " And EM.DeptName ='" + Name + "'";
                }
                else if (CatName == "3")
                {
                    SSQL = SSQL + " And EM.BrokerName ='" + Name + "'";
                }
                SSQL = SSQL + " order by DM.ShortCode asc";
                Salary = ManualAttend.GetDetails(SSQL);

                double count = 0;
                int SNo = 1;

                for (int j = 0; j < Salary.Rows.Count; j++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SNo"] = SNo;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = Salary.Rows[j]["EmpNo"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpName"] = Salary.Rows[j]["EmpName"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = Salary.Rows[j]["Dept"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Type"] = Salary.Rows[j]["Type"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Days"] = Salary.Rows[j]["SalperDay"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["BaseSalary"] = Salary.Rows[j]["BaseSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WorkDays"] = Salary.Rows[j]["WorkDays"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EarnedSalary"] = Salary.Rows[j]["EarnedSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = Salary.Rows[j]["OTHrs"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTSalary"] = Salary.Rows[j]["OTSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TEarnedSalary"] = Salary.Rows[j]["TotalEarnedSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PF"] = Salary.Rows[j]["PF"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ESI"] = Salary.Rows[j]["ESI"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Medical"] = Salary.Rows[j]["MediAllow"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Advance"] = Salary.Rows[j]["Advance"].ToString();
                    //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Incentive"] = Salary.Rows[j]["Incentive"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LessCanteen"] = Salary.Rows[j]["LessCanteen"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Convey"] = Salary.Rows[j]["Conveyance"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Spl"] = Salary.Rows[j]["Spl"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotDed"] = Salary.Rows[j]["TotDed"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetSalary"] = Salary.Rows[j]["NetSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetAmt"] = Salary.Rows[j]["NetAmt"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AccNo"] = Salary.Rows[j]["AccNo"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Match"] = Name;

                    WorkDays = WorkDays + Convert.ToDouble(Salary.Rows[j]["WorkDays"].ToString());
                    EarnedSalary = EarnedSalary + Convert.ToDouble(Salary.Rows[j]["EarnedSalary"].ToString());
                    OtHours = OtHours + Convert.ToDouble(Salary.Rows[j]["OTHrs"].ToString());
                    OtAmoun = OtAmoun + Convert.ToDouble(Salary.Rows[j]["OTSalary"].ToString());

                    TEarnedSalary = TEarnedSalary + Convert.ToDouble(Salary.Rows[j]["TotalEarnedSalary"].ToString());
                    PF = PF + Convert.ToDouble(Salary.Rows[j]["PF"].ToString());
                    ESI = ESI + Convert.ToDouble(Salary.Rows[j]["ESI"].ToString());
                    Medical = Medical + Convert.ToDouble(Salary.Rows[j]["MediAllow"].ToString());
                    Advance = Advance + Convert.ToDouble(Salary.Rows[j]["Advance"].ToString());
                   // Incentive = Incentive + Convert.ToDouble(Salary.Rows[j]["Incentive"].ToString());
                    LessCanteen = LessCanteen + Convert.ToDouble(Salary.Rows[j]["LessCanteen"].ToString());
                    Convey = Convey + Convert.ToDouble(Salary.Rows[j]["Conveyance"].ToString());
                    Spl = Spl + Convert.ToDouble(Salary.Rows[j]["Spl"].ToString());
                    TotDed = TotDed + Convert.ToDouble(Salary.Rows[j]["TotDed"].ToString());
                    NetSalary = NetSalary + Convert.ToDouble(Salary.Rows[j]["NetSalary"].ToString());
                    NetAmt = NetAmt + Convert.ToDouble(Salary.Rows[j]["NetAmt"].ToString());

                    count = count + 1;
                    SNo = SNo + 1;
                }

                if (count > 0)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpName"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Type"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Days"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["BaseSalary"] = " Total ";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WorkDays"] = WorkDays;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EarnedSalary"] = EarnedSalary;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OtHours;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTSalary"] = OtAmoun;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TEarnedSalary"] = TEarnedSalary;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PF"] = PF;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ESI"] = ESI;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Medical"] = Medical;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Advance"] = Advance;
                    //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Incentive"] = Incentive;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LessCanteen"] = LessCanteen;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Convey"] = Convey;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Spl"] = Spl;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotDed"] = TotDed;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetSalary"] = NetSalary;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetAmt"] = NetAmt;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AccNo"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Match"] = Name;

                }
                else
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpName"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Type"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Days"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["BaseSalary"] = "Total";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WorkDays"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EarnedSalary"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTSalary"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TEarnedSalary"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PF"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ESI"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Medical"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Advance"] = "0";
                   // AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Incentive"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LessCanteen"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Convey"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Spl"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotDed"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetSalary"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetAmt"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AccNo"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Match"] = Name;

                }


                TEarnedSalary = 0;
                PF = 0;
                ESI = 0;
                Medical = 0;
                Advance = 0;
                Incentive = 0;
                LessCanteen = 0;
                Convey = 0;
                TotDed = 0;
                Spl = 0;
                NetSalary = 0;
                NetAmt = 0;
                WorkDays = 0;
                EarnedSalary = 0;
                OtAmoun = 0;
                OtHours = 0;

                Name = "";
            }

            SSQL = "";
            DataTable Company = new DataTable();
            SSQL = " Select CompName from Company_Mst Where CompCode='" + Session["SessionCcode"].ToString() + "' ";
            Company = ManualAttend.GetDetails(SSQL);

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=COST RPT-SALARY CASH WISE.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='24'>");
            Response.Write("<a style=\"font-weight:bold\">" + Company.Rows[0]["CompName"].ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + Request.Cookies["SessionLcode"].Value.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='24'>");
            Response.Write("<a style=\"font-weight:bold\">COST REPORT SALARY CASH WISE &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='24'>");
            Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">SNo</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">EmpNo</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">EmpName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Dept</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Type</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Sal / Day</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Base Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Work Days</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Earned Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">OT Hrs</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">OT Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">T.Earned Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">PF</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">ESI</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Medical</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Advance</a>");
            Response.Write("</td>");

         

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Less Canteen</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Convey</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Spl</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Tot Dedu</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Net Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Net Amt</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Acc No</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

            int Count = 0;

            for (int i = 0; i < Category.Rows.Count; i++)
            {
                if (CatName == "1")
                {
                    Number = Category.Rows[i]["CateID"].ToString();
                    Name = Category.Rows[i]["CateName"].ToString();

                }
                else if (CatName == "2")
                {
                    Number = Category.Rows[i]["DeptCode"].ToString();
                    Name = Category.Rows[i]["DeptName"].ToString();
                }
                else if (CatName == "3")
                {
                    Number = Category.Rows[i]["AgentID"].ToString();
                    Name = Category.Rows[i]["AgentName"].ToString();
                }

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='24'>");
                Response.Write("<a style=\"font-weight:bold\">" + Name + " </a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                int SNo = 1;

                for (int k = 0; k < AutoDTable.Rows.Count; k++)
                {
                    string Name1 = AutoDTable.Rows[k]["Match"].ToString();

                    if (Number == Name1)
                    {
                        Response.Write("<tr Font-Bold='true' align='center'>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["SNo"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["EmpNo"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td >");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["EmpName"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td >");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DeptName"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Type"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Days"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["BaseSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["WorkDays"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["EarnedSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["OTHrs"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["OTSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["TEarnedSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["PF"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["ESI"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Medical"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Advance"].ToString() + " </a>");
                        Response.Write("</td>");

                        //Response.Write("<td>");
                        //Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Incentive"].ToString() + " </a>");
                        //Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["LessCanteen"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Convey"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Spl"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["TotDed"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["NetSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["NetAmt"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["AccNo"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("</tr>");

                        SNo = SNo + 1;

                        //Count = SNo;
                        string Temp = AutoDTable.Rows[k]["EmpNo"].ToString();
                        if (Temp != "")
                        {
                            Count = Count + 1;
                        }
                        else
                        {

                        }
                    }

                }


            }

            TEarnedSalary = 0;
            PF = 0;
            ESI = 0;
            Medical = 0;
            Advance = 0;
            Incentive = 0;
            LessCanteen = 0;
            Convey = 0;
            TotDed = 0;
            Spl = 0;
            NetSalary = 0;
            NetAmt = 0;
            WorkDays = 0;
            EarnedSalary = 0;
            OtHours = 0;
            OtAmoun = 0;

            for (int m = 0; m < AutoDTable.Rows.Count; m++)
            {
                string EmpNos = AutoDTable.Rows[m]["EmpNo"].ToString();

                if (EmpNos != "")
                {
                    WorkDays = WorkDays + Convert.ToDouble(AutoDTable.Rows[m]["WorkDays"].ToString());
                    EarnedSalary = EarnedSalary + Convert.ToDouble(AutoDTable.Rows[m]["EarnedSalary"].ToString());
                    OtHours = OtHours + Convert.ToDouble(AutoDTable.Rows[m]["OTHrs"].ToString());
                    OtAmoun = OtAmoun + Convert.ToDouble(AutoDTable.Rows[m]["OTSalary"].ToString());

                    TEarnedSalary = TEarnedSalary + Convert.ToDouble(AutoDTable.Rows[m]["TEarnedSalary"].ToString());
                    PF = PF + Convert.ToDouble(AutoDTable.Rows[m]["PF"].ToString());
                    ESI = ESI + Convert.ToDouble(AutoDTable.Rows[m]["ESI"].ToString());
                    Medical = Medical + Convert.ToDouble(AutoDTable.Rows[m]["Medical"].ToString());
                    Advance = Advance + Convert.ToDouble(AutoDTable.Rows[m]["Advance"].ToString());
                   // Incentive = Incentive + Convert.ToDouble(AutoDTable.Rows[m]["Incentive"].ToString());
                    LessCanteen = LessCanteen + Convert.ToDouble(AutoDTable.Rows[m]["LessCanteen"].ToString());
                    Convey = Convey + Convert.ToDouble(AutoDTable.Rows[m]["Convey"].ToString());
                    Spl = Spl + Convert.ToDouble(AutoDTable.Rows[m]["Spl"].ToString());
                    TotDed = TotDed + Convert.ToDouble(AutoDTable.Rows[m]["TotDed"].ToString());
                    NetSalary = NetSalary + Convert.ToDouble(AutoDTable.Rows[m]["NetSalary"].ToString());
                    NetAmt = NetAmt + Convert.ToDouble(AutoDTable.Rows[m]["NetAmt"].ToString());

                }
                else
                {

                }

            }


            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td colspan='2' align='right'>");
            Response.Write("<a style=\"font-weight:bold\">Total Labours</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Count + "</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' align='right'>");
            Response.Write("<a style=\"font-weight:bold\">Grand Total</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + WorkDays + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + EarnedSalary + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + OtHours + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + OtAmoun + "</a>");
            Response.Write("</td>");


            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + TEarnedSalary + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + PF + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + ESI + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Medical + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Advance + "</a>");
            Response.Write("</td>");

            //Response.Write("<td>");
            //Response.Write("<a style=\"font-weight:bold\">" + Incentive + "</a>");
            //Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + LessCanteen + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Convey + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Spl + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + TotDed + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + NetSalary + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + NetAmt + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("</tr>");


            Response.Write("</table>");

            // Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }


        public ActionResult PFESISalaryListDetails(string PFCode,string ESICode, string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {

            var cookie = Request.Cookies["SessionLcode"].Value;
            if (cookie != null)
            {

                SessionLcode = Request.Cookies["SessionLcode"].Value.ToString();
                SessionUserID = Request.Cookies["UserId"].Value.ToString();
                if (Session["SessionCcode"] != null)
                {
                    SessionCcode = Session["SessionCcode"].ToString();
                    SessionUserName = Session["Usernmdisplay"].ToString();
                }

                PFESIGetSalaryListDetails(PFCode, ESICode, FromDate, ToDate, WagesType, FinYear, Months);

                return View();

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }


        }

        public void PFESIGetSalaryListDetails(string PFCode,string ESICode, string FromDate, string ToDate, string WagesType, string FinYear, string Months)
        {
            DataTable Category = new DataTable();
            DataTable AutoDTable = new DataTable();
            DataTable Salary = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("EmpName");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Days");
            AutoDTable.Columns.Add("BaseSalary");
            AutoDTable.Columns.Add("WorkDays");
            AutoDTable.Columns.Add("EarnedSalary");
            AutoDTable.Columns.Add("OTHrs");
            AutoDTable.Columns.Add("OTSalary");
            AutoDTable.Columns.Add("TEarnedSalary");
            AutoDTable.Columns.Add("PF");
            AutoDTable.Columns.Add("ESI");
            AutoDTable.Columns.Add("Medical");
            AutoDTable.Columns.Add("Advance");
            //AutoDTable.Columns.Add("Incentive");
            AutoDTable.Columns.Add("LessCanteen");
            AutoDTable.Columns.Add("Convey");
            AutoDTable.Columns.Add("Spl");
            AutoDTable.Columns.Add("TotDed");
            AutoDTable.Columns.Add("NetSalary");
            AutoDTable.Columns.Add("NetAmt");
            AutoDTable.Columns.Add("AccNo");
            AutoDTable.Columns.Add("Match");

            double TEarnedSalary = 0;
            double PF = 0;
            double ESI = 0;
            double Medical = 0;
            double Advance = 0;
            double Incentive = 0;
            double LessCanteen = 0;
            double Convey = 0;
            double Spl = 0;
            double TotDed = 0;
            double NetSalary = 0;
            double NetAmt = 0;
            double WorkDays = 0;
            double EarnedSalary = 0;
            double OtHours = 0;
            double OtAmoun = 0;

            SSQL = "";
            string CatName = "1";
            if (CatName == "1")
            {
                SSQL = "Select Distinct MC.CateID,MC.CateName from MstCategory MC inner join Employee_Mst EM"; //where CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                SSQL = SSQL + " on MC.CateID=EM.WageCategoty where";
                if (PFCode != "0")
                {
                    SSQL = SSQL + " EM.PF_PF_No='" + PFCode + "'";
                }
                if (ESICode != "0")
                {
                    SSQL = SSQL + " EM.PF_ESI_No='" + ESICode + "'";
                }
                Category = ManualAttend.GetDetails(SSQL);
            }
            else if (CatName == "2")
            {

                SSQL = "select DeptCode,DeptName from Department_Mst"; //where CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "' order by ShortCode asc";
                Category = ManualAttend.GetDetails(SSQL);
            }
            else if (CatName == "3")
            {

                SSQL = "select AgentID,AgentName from  AgentMst where CompCode='" + Session["SessionCcode"].ToString() + "' And LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                Category = ManualAttend.GetDetails(SSQL);

            }

            string Name = "";
            string Number = "";

            for (int i = 0; i < Category.Rows.Count; i++)
            {
                //Name = "";

                if (CatName == "1")
                {
                    Name = Category.Rows[i]["CateID"].ToString();
                }
                else if (CatName == "2")
                {
                    Name = Category.Rows[i]["DeptCode"].ToString();
                }
                else if (CatName == "3")
                {
                    Name = Category.Rows[i]["AgentID"].ToString();

                }

                SSQL = "";
                SSQL = "Select EM.EmpNo as EmpNo,EM.FirstName as EmpName,DM.DeptName as Dept,CM.CateName as Type,SD.PerDaySal as SalperDay,EM.BasicSalary as BaseSalary,";
                SSQL = SSQL + "SD.WorkedDays as WorkDays,SD.Basic_SM as EarnedSalary,SD.OTHoursNew as OTHrs,SD.OTHoursAmtNew as OTSalary,SD.GrossEarnings as TotalEarnedSalary,";
                SSQL = SSQL + "SD.PfSalary as PF,SD.ESI,SD.Advance,(SD.Deduction3) as MediAllow,SD.DayIncentive as Incentive,SD.Deduction5 as LessCanteen,SD.ConvAllow as Conveyance,SD.Losspay as Spl,SD.TotalDeductions as TotDed,SD.NetPay as NetSalary,";
                SSQL = SSQL + "SD.RoundOffNetPay as NetAmt,EM.AccountNo as AccNo from Employee_Mst EM inner join SalaryDetails SD on EM.EmpNo=SD.EmpNo inner join Department_Mst DM";
                SSQL = SSQL + " on EM.DeptName=DM.DeptCode inner join MstCategory CM on EM.WageCategoty=CM.CateID where";
                //SSQL = SSQL + " where EM.CompCode='" + Session["SessionCcode"].ToString() + "' And EM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                //SSQL = SSQL + " And DM.CompCode='" + Session["SessionCcode"].ToString() + "' And DM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                //SSQL = SSQL + " And SD.Ccode='" + Session["SessionCcode"].ToString() + "' And SD.Lcode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                //SSQL = SSQL + " And CM.CompCode='" + Session["SessionCcode"].ToString() + "' And CM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
                if (CatName == "1")
                {
                    SSQL = SSQL + " EM.WageCategoty ='" + Name + "'";
                }
                if (PFCode!="0")
                {
                    SSQL = SSQL + " And EM.PF_PF_No='" + PFCode + "'";
                }
                if (ESICode != "0")
                {
                    SSQL = SSQL + " And EM.PF_ESI_No='" + ESICode + "'";
                }

                if (WagesType != "0")
                {
                    SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
                    SSQL = SSQL + " And SD.Wagestype='" + WagesType + "'";
                }


                if (FromDate != "" && ToDate != "")
                {
                    SSQL = SSQL + " And CONVERT(datetime,SD.FromDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(FromDate).ToString("yyyy/MM/dd") + "',120)";
                    SSQL = SSQL + " And CONVERT(datetime,SD.ToDate,120) = CONVERT(datetime,'" + Convert.ToDateTime(ToDate).ToString("yyyy/MM/dd") + "',120)";
                }

                
                SSQL = SSQL + " order by DM.ShortCode asc";
                Salary = ManualAttend.GetDetails(SSQL);

                double count = 0;
                int SNo = 1;

                for (int j = 0; j < Salary.Rows.Count; j++)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["SNo"] = SNo;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = Salary.Rows[j]["EmpNo"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpName"] = Salary.Rows[j]["EmpName"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = Salary.Rows[j]["Dept"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Type"] = Salary.Rows[j]["Type"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Days"] = Salary.Rows[j]["SalperDay"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["BaseSalary"] = Salary.Rows[j]["BaseSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WorkDays"] = Salary.Rows[j]["WorkDays"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EarnedSalary"] = Salary.Rows[j]["EarnedSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = Salary.Rows[j]["OTHrs"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTSalary"] = Salary.Rows[j]["OTSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TEarnedSalary"] = Salary.Rows[j]["TotalEarnedSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PF"] = Salary.Rows[j]["PF"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ESI"] = Salary.Rows[j]["ESI"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Medical"] = Salary.Rows[j]["MediAllow"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Advance"] = Salary.Rows[j]["Advance"].ToString();
                    //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Incentive"] = Salary.Rows[j]["Incentive"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LessCanteen"] = Salary.Rows[j]["LessCanteen"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Convey"] = Salary.Rows[j]["Conveyance"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Spl"] = Salary.Rows[j]["Spl"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotDed"] = Salary.Rows[j]["TotDed"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetSalary"] = Salary.Rows[j]["NetSalary"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetAmt"] = Salary.Rows[j]["NetAmt"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AccNo"] = Salary.Rows[j]["AccNo"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Match"] = Name;

                    WorkDays = WorkDays + Convert.ToDouble(Salary.Rows[j]["WorkDays"].ToString());
                    EarnedSalary = EarnedSalary + Convert.ToDouble(Salary.Rows[j]["EarnedSalary"].ToString());
                    OtHours = OtHours + Convert.ToDouble(Salary.Rows[j]["OTHrs"].ToString());
                    OtAmoun = OtAmoun + Convert.ToDouble(Salary.Rows[j]["OTSalary"].ToString());

                    TEarnedSalary = TEarnedSalary + Convert.ToDouble(Salary.Rows[j]["TotalEarnedSalary"].ToString());
                    PF = PF + Convert.ToDouble(Salary.Rows[j]["PF"].ToString());
                    ESI = ESI + Convert.ToDouble(Salary.Rows[j]["ESI"].ToString());
                    Medical = Medical + Convert.ToDouble(Salary.Rows[j]["MediAllow"].ToString());
                    Advance = Advance + Convert.ToDouble(Salary.Rows[j]["Advance"].ToString());
                    // Incentive = Incentive + Convert.ToDouble(Salary.Rows[j]["Incentive"].ToString());
                    LessCanteen = LessCanteen + Convert.ToDouble(Salary.Rows[j]["LessCanteen"].ToString());
                    Convey = Convey + Convert.ToDouble(Salary.Rows[j]["Conveyance"].ToString());
                    Spl = Spl + Convert.ToDouble(Salary.Rows[j]["Spl"].ToString());
                    TotDed = TotDed + Convert.ToDouble(Salary.Rows[j]["TotDed"].ToString());
                    NetSalary = NetSalary + Convert.ToDouble(Salary.Rows[j]["NetSalary"].ToString());
                    NetAmt = NetAmt + Convert.ToDouble(Salary.Rows[j]["NetAmt"].ToString());

                    count = count + 1;
                    SNo = SNo + 1;
                }

                if (count > 0)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpName"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Type"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Days"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["BaseSalary"] = " Total ";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WorkDays"] = WorkDays;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EarnedSalary"] = EarnedSalary;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OtHours;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTSalary"] = OtAmoun;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TEarnedSalary"] = TEarnedSalary;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PF"] = PF;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ESI"] = ESI;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Medical"] = Medical;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Advance"] = Advance;
                    //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Incentive"] = Incentive;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LessCanteen"] = LessCanteen;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Convey"] = Convey;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Spl"] = Spl;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotDed"] = TotDed;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetSalary"] = NetSalary;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetAmt"] = NetAmt;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AccNo"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Match"] = Name;

                }
                else
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpNo"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EmpName"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Type"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Days"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["BaseSalary"] = "Total";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WorkDays"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["EarnedSalary"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTSalary"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TEarnedSalary"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PF"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ESI"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Medical"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Advance"] = "0";
                    // AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Incentive"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["LessCanteen"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Convey"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Spl"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotDed"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetSalary"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["NetAmt"] = "0";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AccNo"] = "";
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Match"] = Name;

                }


                TEarnedSalary = 0;
                PF = 0;
                ESI = 0;
                Medical = 0;
                Advance = 0;
                Incentive = 0;
                LessCanteen = 0;
                Convey = 0;
                TotDed = 0;
                Spl = 0;
                NetSalary = 0;
                NetAmt = 0;
                WorkDays = 0;
                EarnedSalary = 0;
                OtAmoun = 0;
                OtHours = 0;

                Name = "";
            }

            SSQL = "";
            DataTable Company = new DataTable();
            SSQL = " Select CompName from Company_Mst Where CompCode='" + Session["SessionCcode"].ToString() + "' ";
            Company = ManualAttend.GetDetails(SSQL);

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=PFESIRPTSALARYWISE.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='24'>");
            Response.Write("<a style=\"font-weight:bold\">" + Company.Rows[0]["CompName"].ToString() + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + Request.Cookies["SessionLcode"].Value.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='24'>");
            Response.Write("<a style=\"font-weight:bold\">COST REPORT SALARY CASH WISE &nbsp;&nbsp;&nbsp;</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='24'>");
            Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            Response.Write("&nbsp;&nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">SNo</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">EmpNo</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">EmpName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Dept</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Type</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Sal / Day</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Base Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Work Days</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Earned Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">OT Hrs</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">OT Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">T.Earned Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">PF</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">ESI</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Medical</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Advance</a>");
            Response.Write("</td>");



            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Less Canteen</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Convey</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Spl</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Tot Dedu</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Net Salary</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Net Amt</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Acc No</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

            int Count = 0;

            for (int i = 0; i < Category.Rows.Count; i++)
            {
                if (CatName == "1")
                {
                    Number = Category.Rows[i]["CateID"].ToString();
                    Name = Category.Rows[i]["CateName"].ToString();

                }
                else if (CatName == "2")
                {
                    Number = Category.Rows[i]["DeptCode"].ToString();
                    Name = Category.Rows[i]["DeptName"].ToString();
                }
                else if (CatName == "3")
                {
                    Number = Category.Rows[i]["AgentID"].ToString();
                    Name = Category.Rows[i]["AgentName"].ToString();
                }

                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='24'>");
                Response.Write("<a style=\"font-weight:bold\">" + Name + " </a>");
                Response.Write("</td>");
                Response.Write("</tr>");

                int SNo = 1;

                for (int k = 0; k < AutoDTable.Rows.Count; k++)
                {
                    string Name1 = AutoDTable.Rows[k]["Match"].ToString();

                    if (Number == Name1)
                    {
                        Response.Write("<tr Font-Bold='true' align='center'>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["SNo"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["EmpNo"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td >");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["EmpName"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td >");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["DeptName"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Type"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Days"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["BaseSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["WorkDays"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["EarnedSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["OTHrs"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["OTSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["TEarnedSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["PF"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["ESI"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Medical"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Advance"].ToString() + " </a>");
                        Response.Write("</td>");

                        //Response.Write("<td>");
                        //Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Incentive"].ToString() + " </a>");
                        //Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["LessCanteen"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Convey"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["Spl"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["TotDed"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["NetSalary"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["NetAmt"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("<a style=\"font-weight:bold\">" + AutoDTable.Rows[k]["AccNo"].ToString() + " </a>");
                        Response.Write("</td>");

                        Response.Write("</tr>");

                        SNo = SNo + 1;

                        //Count = SNo;
                        string Temp = AutoDTable.Rows[k]["EmpNo"].ToString();
                        if (Temp != "")
                        {
                            Count = Count + 1;
                        }
                        else
                        {

                        }
                    }

                }


            }

            TEarnedSalary = 0;
            PF = 0;
            ESI = 0;
            Medical = 0;
            Advance = 0;
            Incentive = 0;
            LessCanteen = 0;
            Convey = 0;
            TotDed = 0;
            Spl = 0;
            NetSalary = 0;
            NetAmt = 0;
            WorkDays = 0;
            EarnedSalary = 0;
            OtHours = 0;
            OtAmoun = 0;

            for (int m = 0; m < AutoDTable.Rows.Count; m++)
            {
                string EmpNos = AutoDTable.Rows[m]["EmpNo"].ToString();

                if (EmpNos != "")
                {
                    WorkDays = WorkDays + Convert.ToDouble(AutoDTable.Rows[m]["WorkDays"].ToString());
                    EarnedSalary = EarnedSalary + Convert.ToDouble(AutoDTable.Rows[m]["EarnedSalary"].ToString());
                    OtHours = OtHours + Convert.ToDouble(AutoDTable.Rows[m]["OTHrs"].ToString());
                    OtAmoun = OtAmoun + Convert.ToDouble(AutoDTable.Rows[m]["OTSalary"].ToString());

                    TEarnedSalary = TEarnedSalary + Convert.ToDouble(AutoDTable.Rows[m]["TEarnedSalary"].ToString());
                    PF = PF + Convert.ToDouble(AutoDTable.Rows[m]["PF"].ToString());
                    ESI = ESI + Convert.ToDouble(AutoDTable.Rows[m]["ESI"].ToString());
                    Medical = Medical + Convert.ToDouble(AutoDTable.Rows[m]["Medical"].ToString());
                    Advance = Advance + Convert.ToDouble(AutoDTable.Rows[m]["Advance"].ToString());
                    // Incentive = Incentive + Convert.ToDouble(AutoDTable.Rows[m]["Incentive"].ToString());
                    LessCanteen = LessCanteen + Convert.ToDouble(AutoDTable.Rows[m]["LessCanteen"].ToString());
                    Convey = Convey + Convert.ToDouble(AutoDTable.Rows[m]["Convey"].ToString());
                    Spl = Spl + Convert.ToDouble(AutoDTable.Rows[m]["Spl"].ToString());
                    TotDed = TotDed + Convert.ToDouble(AutoDTable.Rows[m]["TotDed"].ToString());
                    NetSalary = NetSalary + Convert.ToDouble(AutoDTable.Rows[m]["NetSalary"].ToString());
                    NetAmt = NetAmt + Convert.ToDouble(AutoDTable.Rows[m]["NetAmt"].ToString());

                }
                else
                {

                }

            }


            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td colspan='2' align='right'>");
            Response.Write("<a style=\"font-weight:bold\">Total Labours</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Count + "</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("<td colspan='2' align='right'>");
            Response.Write("<a style=\"font-weight:bold\">Grand Total</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + WorkDays + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + EarnedSalary + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + OtHours + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + OtAmoun + "</a>");
            Response.Write("</td>");


            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + TEarnedSalary + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + PF + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + ESI + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Medical + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Advance + "</a>");
            Response.Write("</td>");

            //Response.Write("<td>");
            //Response.Write("<a style=\"font-weight:bold\">" + Incentive + "</a>");
            //Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + LessCanteen + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Convey + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + Spl + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + TotDed + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + NetSalary + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">" + NetAmt + "</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"></a>");
            Response.Write("</td>");

            Response.Write("</tr>");


            Response.Write("</table>");

            // Response.Write(stw.ToString());
            Response.End();
            Response.Clear();


        }

    }
}
