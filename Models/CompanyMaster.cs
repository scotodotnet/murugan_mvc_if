﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttendanceMVC.Models
{
    public class CompanyMaster
    {
        
        public string CompCode { get; set; }
        public string CompName { get; set; }
        public string Add1 { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Tel_No_Code { get; set; }
        public string Tel_No { get; set; }
        public string Mobile { get; set; }
        public string EmailID { get; set; }
        public string TINNo { get; set; }
        public string CSTNo { get; set; }
        public string GSTNo { get; set; }
        public string Branch { get; set; }
        public string PAN_No { get; set; }
        public string BankName { get; set; }
        public string ACCNo { get; set; }
        public string Branch_IFSCode { get; set; }
        public string HSN_SAC_No { get; set; }

        public string LocCode { get; set; }
        public string LocName { get; set; }
      public string RegNo { get; set; }

    }
}