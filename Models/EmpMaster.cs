﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace AttendanceMVC.Models
{
    public class EmpMaster
    {
        public string PF_per { get; set; }
        public string ESI_per { get; set; }
        public string StaffSalary { get; set; }
        public string Ccode { get; set; }
        public string Lcode { get; set; }
        public string EmployeerPFone { get; set; }
        public string EmployeerPFTwo { get; set; }
        public string EmployeerESI { get; set; }

        static string connString = ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString;
        public static DataTable GetDetails(string query)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();

                //var query = "Select *from LogTime_Days where Present='1.0' and Attn_Date_Str='2017/02/28'";
                SqlCommand com = new SqlCommand(query, con);
                DataTable ds = new DataTable();

                SqlDataAdapter sda = new SqlDataAdapter(query, con);
                sda.Fill(ds);
                con.Close();
                return ds;
            }
        }

    }
}