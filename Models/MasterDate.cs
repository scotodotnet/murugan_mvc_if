﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttendanceMVC.Models
{
    public class MasterDate
    {
        // Read the connection string from the web.config file
        static string connString = ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString;

        public bool sessionValue { set; get; }

        public string ID { set; get; }

        public string Name { get; set; }

        public string Category { get; set; }

        public string Shift { set; get; }

        public string PF_IF_No { set; get; }

        public string PF_ESI_No { set; get; }

        public string PF_PF_No { set; get; }
        public string SubCatName { get; set; }
        public string MachineID { get; set; }
        public string ExistingCode { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string DOJ { get; set; }
        public string ActiveMode { get; set; }
        public string RejoinDate { get; set; }
        public string ReleaveDate { get; set; }
        public string DeptName { get; set; }
        public string Designation { get; set; }
        public string EmpMobileNo { get; set; }
        public string OTEligible { get; set; }
        public string EmpType { get; set; }
        public string PFEligible { get; set; }
        public string PFNo { get; set; }
        public string PFDate { get; set; }
        public string ESIEligible { get; set; }
        public string ESINo { get; set; }
        public string ESIDate { get; set; }
        public string Qualification { get; set; }
        public string IsNonAdmin { get; set; }
        public string LeaderEligible { get; set; }
        public string HostelRoom { get; set; }
        public string BusRoute { get; set; }
        public string BusNo { get; set; }
        public string SalaryThrough { get; set; }
        public string BankName { get; set; }

        public string AccountNo { get; set; }
        public string IFSCCode { get; set; }
        public string Branch { get; set; }
        public string BasicSal { get; set; }
        public string OTSal { get; set; }
        public string Allowance1 { get; set; }
        public string Allowance2 { get; set; }
        public string Deduction1 { get; set; }
        public string Deduction2 { get; set; }
        public string PFSal { get; set; }
        public string MartialStatus { get; set; }
        public string Nationality { get; set; }
        public string Religion { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string PhyChallenged { get; set; }
        public string PhyReason { get; set; }
        public string StdWorkingHrs { get; set; }
        public string IncentAmt { get; set; }
        public string AgentName { get; set; }
        public string AgentType { get; set; }
        public string RecruitThrg { get; set; }
        public string RecruitMobile { get; set; }
        public string Nominee { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string GuardianName { get; set; }
        public string PermAddr { get; set; }
        public string PermTaluk { get; set; }
        public string PermDist { get; set; }
        public string TempAddr { get; set; }
        public string TempTaluk { get; set; }
        public string TempDist { get; set; }
        public string State { get; set; }
        public string IdenMark1 { get; set; }
        public string IdenMark2 { get; set; }
        public string ParentMob1 { get; set; }
        public string ParentMob2 { get; set; }
        public string DocType { get; set; }
        public string DocNo { get; set; }
        public string DocDesc { get; set; }
        public string BloodGrp { get; set; }
        public string WeekOff { get; set; }
        public string Community { get; set; }
        public string Caste { get; set; }
        public string VPFSal { get; set; }
        public string Others1 { get; set; }
        public string Others2 { get; set; }
        //public bool sessionValue { set; get; }
        public SelectList DeptList { get; set; }

        public string ConvenyAmt { get; set; }

        public string SplAmt { get; set; }
        public string Grade { get; set; }
        public string WageCategoty { get; set; }
        public string WorkCode { get; set; }
        public string EligibleCateen { get; set; }
        public string CanteenName { get; set; }
        public string CanteenOperator { get; set; }
        public string LabourAmt { get; set; }
        public string CanManageAmt { get; set; }
        public string EligibleSnacks { get; set; }
        public string SnacksOperator { get; set; }
        public string SnacksAmt { get; set; }
        public string EligibleIncentive { get; set; }
        public string LeaveFrom1 { get; set; }
        public string LeaveTo1 { get; set; }
        public string LeaveFrom2 { get; set; }
        public string LeaveTo2 { get; set; }
        public string Residance { get; set; }

        public string LevDesign1 { get; set; }
        public string LevDesign2 { get; set; }




        public static DataTable GetDetails(string query)
        {
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();

                //var query = "Select *from LogTime_Days where Present='1.0' and Attn_Date_Str='2017/02/28'";
                SqlCommand com = new SqlCommand(query, con);
                DataTable ds = new DataTable();

                SqlDataAdapter sda = new SqlDataAdapter(query, con);
                sda.Fill(ds);
                con.Close();
                return ds;
            }
        }
    }
}