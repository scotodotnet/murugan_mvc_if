﻿$(function () {

    //alert('Test')
    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });




});

function LoadEmployeeType() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Others/GetEmployeeType',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlEmpType').empty();
            $("#ddlEmpType").append($('<option></option>').val('0').text('--Select Employee Type--'));
            $.each(data, function (i, val) {

                $("#ddlEmpType").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadFinancialYear() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Others/FinancialYear1',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlFin').empty();
            $("#ddlFin").append($('<option></option>').val('0').text('-Select-'));
            $.each(data, function (i, val) {

                $("#ddlFin").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

$(document).ready(function () {

    LoadEmployeeType();
    LoadFinancialYear();

    $("#btnReport").click(function () {
        //alert('hi');
        var CatName = $('input:radio[name="radiorequired"]:checked').val();
        var FromDate = $('#txtFromDate').val();
        var ToDate = $('#txtToDate').val();
        var FinYear = $('#ddlFin').val();
        var WagesType = $('#ddlEmpType').val();
        var Months = $('#ddlMonth').val();


        var isAllValid = true;


        //if ($('input:radio[name="radiorequired"]:checked').val().trim() == '') {

        //    document.getElementById("RadioError").innerHTML = "Enter the From Date";

        //    isAllValid = false;
        //}
        //else {

        //    document.getElementById("RadioError").innerHTML = " ";  // remove it
        //}

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }
        if ($('#txtToDate').val().trim() == '') {

            document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

            isAllValid = false;
        }
        else {

            document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/ReportPayroll/SalaryList',
                success: function () {
                    var url = '/ReportPayroll/SalaryListDetails/?CatName=' + CatName + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '&WagesType=' + WagesType + '&FinYear=' + FinYear + '&Months=' + Months + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }


    });

    $("#btnPFESIReport").click(function () {
        //alert('hi');
        //var CatName = $('input:radio[name="radiorequired"]:checked').val();
        var FromDate = $('#txtFromDate').val();
        var ToDate = $('#txtToDate').val();
        var FinYear = $('#ddlFin').val();
        var WagesType = $('#ddlEmpType').val();
        var Months = $('#ddlMonth').val();
        var PFCode = $('#ddlPFCode').val();
        var ESICode = $('#ddlESICode').val();

        var isAllValid = true;


        //if (PFCode == '0' || ESICode == '0')
        //{
        //    alert('Please select the PF code or ESI Code');
        //    isAllValid = false;
        //}

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }
        if ($('#txtToDate').val().trim() == '') {

            document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

            isAllValid = false;
        }
        else {

            document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                url: '/ReportPayroll/SalaryList',
                success: function () {
                    var url = '/ReportPayroll/PFESISalaryListDetails/?PFCode=' + PFCode + '&ESICode=' + ESICode + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '&WagesType=' + WagesType + '&FinYear=' + FinYear + '&Months=' + Months + '';
                    window.open(url);
                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });


        }


    });

});
