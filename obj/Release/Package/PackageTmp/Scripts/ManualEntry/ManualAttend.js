﻿var GobalDate;
$(function () {

    //alert('Test')
    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });

    $('.select2').select2();
    $('#example1').DataTable();
    $('#TimeInTable').DataTable();
    $('#TimeOutTable').DataTable();

    $('#OTDataTable').DataTable();
    $('#AllLeaveTable').DataTable();
    $('#AttendDataTable').DataTable();
    $('#LeaveTable').DataTable();

});

$(document).ready(function () {
    LoadGoodsDataTable();
    LoadOTDataTable();
    LoadAttendanceDataTable();
    LoadLeaveDataTable();
    LoadLeaveName();
    LoadAllLeaveDataTable();
});

function ClearOT() {
    $('#txtMachineID').val('');
    $('#txtExistingCode').val('');
    $('#txtname').val('');
    $('#txtOTDate').val('');
    $('#txtOTHrs').val('');
}

function ClearINTime() {
    $('#txtMachineID').val('');
    $('#txtExistingCode').val('');
    $('#txtname').val('');
    $('#txtINDate').val('');
}

function ClearOUTTime() {
    $('#txtMachineID').val('');
    $('#txtExistingCode').val('');
    $('#txtname').val('');
    $('#txtTimeOUTDate').val('');
}

function ClearManAtted() {
    $('#txtMachineID').val('');
    $('#txtExistingCode').val('');
    $('#txtname').val('');
    $('#txtDateIN').val('');
    $('#txtTimeIN').val('');

    $('#txtDateOUT').val('');
    $('#txtTimeOUT').val('');
}

function ClearRelive() {
    $('#txtMachineID').val('');
    $('#txtExistingCode').val('');
    $('#txtname').val('');
    $('#txtFromDate').val('');
    $('#txtLeaveDesc').val('');

}
$('#btnAddOT').click(function () {
    $('#OTmain').css('display', 'none');
    $('#OTsub').css('display', 'block');
});

$('#btnAddLeave').click(function () {
    $('#DisplayLeaveMain').css('display', 'none');
    $('#DisplayLeaveSub').css('display', 'block');
});

$('#btnAddManAtted').click(function () {
    $('#DisplayManAttendMain').css('display', 'none');
    $('#DisplayManAttendSub').css('display', 'block');
});


$('#btnINClear').click(function () {
    ClearINTime();
});


$('#btnOUTClear').click(function () {
    ClearOUTTime();
});
$('#btnClearOT').click(function () {
    ClearOT();
});
$('#btnClearManAttend').click(function () {
    ClearManAtted();
});
function LoadLeaveName() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Manual/GetLeaveType',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlLeaveType').empty();
            $("#ddlLeaveType").append($('<option></option>').val('0').text('--Select Leave--'));
            $.each(data, function (i, val) {

                $("#ddlLeaveType").append($('<option></option>').val(val.CodeVal).text(val.TypeVal));

            })
        }
    })
}

$('#btnSaveLeaveEntry').click(function (e) {
    //validation of order

    var isAllValid = true;

    if ($('#txtFromDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("FromDateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtToDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("ToDateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtLeaveDesc').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("LeaveDescError").innerHTML = " "; // remove it
        e.preventDefault();
    }
    if (isAllValid) {

        //Save data to database
        var data = {
            MachineID: $('#txtMachineID').val().trim(),
            ExistingCode: $('#txtExistingCode').val().trim(),
            EmpName: $('#txtname').val().trim(),
            AttnDate: $('#txtFromDate').val().trim(),
            AttnDateStr: $('#txtToDate').val().trim(),
            LeaveType: $('#ddlLeaveType').val(),
            LeaveDesc: $('#txtLeaveDesc').val().trim()
        }

        console.log(data);
        $.ajax({
            url: '/Manual/SaveManualLeaveEntry',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                //check is successfully save to database 
                if (d == "Insert") {
                    alert('Manual Leave Entry Added...,');

                }
                else if (d == "Update") {

                    alert('Manual Leave Entry Update...,');

                }
                else {
                    alert('Failed');
                }
                //$('#btnSave').val('Save');
            },
            error: function () {
                alert('Error. Please try again.');
                //$('#btnSave').val('Save');
            }
        });
    }

});

//Update Relive Employee
$('#btnRelive').click(function (e) {
    //validation of order

    var isAllValid = true;

    if ($('#txtFromDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("FromDateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

  
    if (isAllValid) {

        //Save data to database
        var data = {
            Machine_No: $('#txtMachineID').val().trim(),
            ExistingCode: $('#txtExistingCode').val().trim(),
            EmpName: $('#txtname').val().trim(),
            AttnDate: $('#txtFromDate').val().trim(),
            LeaveDesc: $('#txtLeaveDesc').val().trim()
        }

        console.log(data);
        $.ajax({
            url: '/Manual/UpdateRelive',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                //check is successfully save to database 
                if (d == "Insert") {
                    alert('Employee Added...,');
                    ClearRelive();
                }
                else if (d == "Update") {

                    alert('Employee Update...,');
                    ClearRelive();
                }
                else {
                    alert('Failed');
                }
                //$('#btnSave').val('Save');
            },
            error: function () {
                alert('Error. Please try again.');
                //$('#btnSave').val('Save');
            }
        });
    }

});

//Clear Relive Page
$('#btnClearRelive').click(function (e) {
    //validation of order
    ClearRelive();
  

});
//Add Manual Leave
$('#btnLeaveTypesave').click(function (e) {
    //validation of order

    var isAllValid = true;

    if ($('#txtLeaveType').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("LeaveTypeerror").innerHTML = " "; // remove it
        e.preventDefault();
    }
    if (isAllValid) {

        //Save data to database
        var data = {
            UserType: $('#txtLeaveType').val().trim()

        }

        console.log(data);
        $.ajax({
            url: '/Manual/SaveManualLeave',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                //check is successfully save to database 
                if (d == "Insert") {
                    alert('Manual Leave Added...,');
                    LoadLeaveDataTable();
                    $('#txtLeaveType').val('');
                    //ClearOT();
                    $('#modal-dialog-LeaveType').modal('hide');
                }
                else if (d == "Update") {

                    alert('Manual Leave Update...,');
                    LoadLeaveDataTable();
                    $('#txtLeaveType').val('');
                    //ClearOT();
                    $('#modal-dialog-LeaveType').modal('hide');
                }
                else {
                    alert('Failed');
                }
                //$('#btnSave').val('Save');
            },
            error: function () {
                alert('Error. Please try again.');
                //$('#btnSave').val('Save');
            }
        });
    }

});


//Add Manual OT
$('#btnSaveOT').click(function (e) {
    //validation of order

    var isAllValid = true;

    if ($('#txtOTDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("OTdateeError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtOTHrs').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("OthoursError").innerHTML = " "; // remove 
        e.preventDefault();
    }



    if (isAllValid) {

        //Save data to database
        var data = {
            EmpNo: $('#txtMachineID').val().trim(),
            ExistingCode: $('#txtExistingCode').val().trim(),
            EmpName: $('#txtname').val().trim(),
            AttnDate: $('#txtOTDate').val().trim(),
            TimeIn: $('#txtOTHrs').val().trim()



        }

        console.log(data);
        $.ajax({
            url: '/Manual/SaveManuaOT',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                //check is successfully save to database 
                if (d == "Insert") {
                    alert('Manual OT Added...,');
                    ClearOT();
                }
                else if (d == "Update") {

                    alert('Manual OT Update...,');
                    ClearOT();

                }
                else {
                    alert('Failed');
                }
                //$('#btnSave').val('Save');
            },
            error: function () {
                alert('Error. Please try again.');
                //$('#btnSave').val('Save');
            }
        });
    }

    $('#OTmain').css('display', 'block');
    $('#OTsub').css('display', 'none');
});
//Add Manual Attendance 
$('#btnSaveManAttend').click(function (e) {



    //validation of order
    var isAllValid = true;

    if ($('#txtDateIN').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("INdateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtTimeIN').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("IntimeError").innerHTML = " "; // remove 
        e.preventDefault();
    }

    if ($('#txtDateOUT').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("OUTdateError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtTimeOUT').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("OUTtimeError").innerHTML = " "; // remove it
        e.preventDefault();
    }



    //Save if valid

    if (isAllValid) {

        //Save data to database
        var data = {
            EmpNo: $('#txtMachineID').val().trim(),
            ExistingCode: $('#txtExistingCode').val().trim(),
            EmpName: $('#txtname').val().trim(),
            AttnDate: $('#txtDateIN').val().trim(),
            TimeIn: $('#txtTimeIN').val().trim(),
            AttnDateStr: $('#txtDateOUT').val().trim(),
            TimeOut: $('#txtTimeOUT').val().trim()


        }

        console.log(data);
        $.ajax({
            url: '/Manual/SaveManulAtt',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                //check is successfully save to database 
                if (d == "Insert") {
                    alert('Manual Attendance Added...,');
                    ClearManAtted();
                }
                else if (d == "Update") {

                    alert('Manual Attedance Update...,');
                    ClearManAtted();

                }
                else {
                    alert('Failed');
                }
                //$('#btnSave').val('Save');
            },
            error: function () {
                alert('Error. Please try again.');
                //$('#btnSave').val('Save');
            }
        });
    }
    $('#DisplayManAttendMain').css('display', 'block');
    $('#DisplayManAttendSub').css('display', 'none');

});


//Load EmployeeDetails Datatable 
function LoadGoodsDataTable() {

    $.ajax({
        type: "GET",
        url: '/Manual/GetEmpID',
        success: function (data) {
            var data1 = $('#example1').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.EmpNo,
                item.ExistingCode,
                item.EmpName,
                item.DeptName,
               "<button class='btn btn-success' type='button' onClick='EditEmpID(\"" + item.EmpNo + "\")'><i class='fa fa-pencil'></i></button> "
                ]).draw();
            });
        }
    });
}
//Load All Leave Datatable 
function LoadAllLeaveDataTable() {

    $.ajax({
        type: "GET",
        url: '/Manual/GetAllLeaveLoad',
        success: function (data) {
            var data1 = $('#AllLeaveTable').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.EmpNo,
                item.ExistingCode,
                item.EmpName,
                item.AttnDate,
                item.AttnDateStr,
                item.LeaveDesc,
               "<button class='btn btn-success' type='button' onClick='EditAllLeaveUpdate(\"" + item.EmpNo + "\")'><i class='fa fa-pencil'></i></button> "
                ]).draw();
            });
        }
    });
}
//Load OT Datatable 
function LoadOTDataTable() {

    $.ajax({
        type: "GET",
        url: '/Manual/GetOTLoad',
        success: function (data) {
            var data1 = $('#OTDataTable').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.EmpNo,
                item.ExistingCode,
                item.EmpName,
                item.AttnDate,
                item.TimeIn,
               "<button class='btn btn-success' type='button' onClick='EditEmpidOTUpdate(\"" + item.EmpNo + "\")'><i class='fa fa-pencil'></i></button> "
                ]).draw();
            });
        }
    });
}
//Load Leave Type Datatable 
function LoadLeaveDataTable() {

    $.ajax({
        type: "GET",
        url: '/Manual/GetLeaveTypeLoad',
        success: function (data) {
            var data1 = $('#LeaveTable').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.UserType,
               "<button class='btn btn-danger' type='button' onClick='DeleteLeaveType(\"" + item.UserType + "\")'><i class='fa fa-trash-o'></i></button> "

                ]).draw();
            });
        }
    });
}
function DeleteLeaveType(index) {

}

//Load Attendance Datatable a
function LoadAttendanceDataTable() {

    $.ajax({
        type: "GET",
        url: '/Manual/GetAttendanceLoad',
        success: function (data) {
            var data1 = $('#AttendDataTable').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.EmpNo,
                item.ExistingCode,
                item.EmpName,
                item.AttnDate,
                item.TimeIn,
                item.TimeOut,

                ]).draw();
            });
        }
    });
}

function EditEmpidOTUpdate(index) {
    $('#OTmain').css('display', 'none');
    $('#OTsub').css('display', 'block');

    $.ajax({
        type: "GET",
        url: '/Manual/GetEditEmpidOT/?EmpNo=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtMachineID").val(item.EmpNo);
                $("#txtExistingCode").val(item.ExistingCode);
                $("#txtname").val(item.EmpName);
                $("#txtOTDate").val(item.AttnDate);
                $("#txtOTHrs").val(item.TimeIn);
            });

        }
    });
}


//Selete MachineID to Datatable 
function EditEmpID(index) {

    $.ajax({
        type: "GET",
        url: '/Manual/GetEditEmpID/?EmpNo=' + index + '',
        success: function (data) {
            //var data1 = $('#example1').DataTable();
            //data1.clear();
            $.each(data, function (key, item) {
                // document.getElementById('txtArrivalID').disabled = true;
                $("#txtMachineID").val(item.EmpNo);
                $("#txtExistingCode").val(item.ExistingCode);
                $("#txtname").val(item.EmpName);

                $('#modal-dialog').modal('hide');
            });

        }
    });

}

//Get Manual IN Time
$('#btnINview').click(function (e) {



    //validation of order
    var isAllValid = true;

    //Get machineID from MachineID textbox
    var MachineID = $("#txtMachineID").val();
    var EmpName = $("#txtname").val();
    var INdate = $("#txtINDate").val();
    GobalDate = $("#txtTimeOUTDate").val();
    if ($('#txtMachineID').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("MachineIDError").innerHTML = " "; // remove it
        e.preventDefault();
    }

    if ($('#txtINDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("INDateError").innerHTML = " "; // remove 
        e.preventDefault();
    }

    if ($('#txtTimeOUTDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("OutDateError").innerHTML = " "; // remove it
        e.preventDefault();
    }


    //Save if valid

    if (isAllValid) {

        $.ajax({
            type: "GET",
            url: '/Manual/GetTimeDeleteIN/?EmpNo=' + MachineID + '&TimeINdate=' + INdate + '',
            success: function (data) {
                var data1 = $('#TimeInTable').DataTable();
                data1.clear();
                $.each(data, function (key, item) {

                    data1.row.add([
                    MachineID,
                    EmpName,
                    INdate,
                    item.TimeIn,
                   "<button class='btn btn-danger' type='button' onClick='DeleteTimeIN(" + MachineID + "," + INdate + ")'><i class='fa fa-trash-o'></i></button> "
                    ]).draw();
                });
            }
        });
    }
});


//Delete TimeIN 
function DeleteTimeIN(index1, index2) {

    var Temp = GobalDate;
    //alert(Temp);
    var MachineID = $("#txtMachineID").val();
    var EmpName = $("#txtname").val();
    var INdate = $("#txtINDate").val();

    ClearINTime();

    $.ajax({
        type: "GET",
        url: '/Manual/DeleteTimeIN/?EmpNo=' + index1 + '&TimeINdate=' + Temp + '',
        success: function (data) {
            var data1 = $('#TimeInTable').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                MachineID,
                EmpName,
                INdate,
                item.TimeIn,
               "<button class='btn btn-danger' type='button' onClick='DeleteTimeOUT(\"" + MachineID + "\")'><i class='fa fa-trash-o'></i></button> "
                ]).draw();
            });
        }
    });
}


//Get Manual OUT Time
$('#btnOUTview').click(function (e) {



    //validation of order
    var isAllValid = true;

    //Get machineID from MachineID textbox
    var MachineID = $("#txtMachineID").val();
    var EmpName = $("#txtname").val();
    var Outdate = $("#txtTimeOUTDate").val();

    GobalDate = $("#txtTimeOUTDate").val();

    if ($('#txtMachineID').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("MachineIDError").innerHTML = " "; // remove it
        e.preventDefault();
    }


    if ($('#txtTimeOUTDate').val().trim() == '') {

        isAllValid = false;
    }
    else {

        document.getElementById("OutDateError").innerHTML = " "; // remove it
        e.preventDefault();
    }


    //Save if valid

    if (isAllValid) {

        $.ajax({
            type: "GET",
            url: '/Manual/GetTimeDeleteOUT/?EmpNo=' + MachineID + '&TimeOUTdate=' + Outdate + '',
            success: function (data) {
                var data1 = $('#TimeOutTable').DataTable();
                data1.clear();
                $.each(data, function (key, item) {

                    data1.row.add([
                    MachineID,
                    EmpName,
                    Outdate,
                    item.TimeIn,
                   "<button class='btn btn-danger' type='button' onClick='DeleteTimeOUT(" + MachineID + "," + Outdate + ")'><i class='fa fa-trash-o'></i></button> "
                    ]).draw();
                });
            }
        });
    }
});

//Delete TimeIN 
function DeleteTimeOUT(index1, index2) {
    var Temp = GobalDate;
    //alert(Temp);
    var MachineID = $("#txtMachineID").val();
    var EmpName = $("#txtname").val();
    var Outdate = $("#txtTimeOUTDate").val();

    ClearOUTTime();


    $.ajax({
        type: "GET",
        url: '/Manual/DeleteTimeOUT/?EmpNo=' + index1 + '&TimeOUTdate=' + Temp + '',
        success: function (data) {
            var data1 = $('#TimeOutTable').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                MachineID,
                EmpName,
                Outdate,
                item.TimeIn,
               "<button class='btn btn-danger' type='button' onClick='DeleteTimeOUT(\"" + MachineID + "\")'><i class='fa fa-trash-o'></i></button> "
                ]).draw();
            });
        }
    });
}

