﻿$(function () {
    $('#example1').DataTable();
});


function LoadUserDetails() {

    $.ajax({
        type: "GET",
        url: '/Master/GetUserDetails',
        success: function (data) {
            var data1 = $('#example1').DataTable();
            data1.clear();
            $.each(data, function (key, item) {
                data1.row.add([
                item.DeptName,
                item.EmpName,
                item.UserName,
               "<button class='btn btn-danger' type='button' onClick='DeleteUser(\"" + item.UserName + "\")'><i class='fa fa-times-circle'></i></button>"
                ]).draw();
            });
        }
    });
}

function DeleteUser(index) {

    $.ajax({
        type: "GET",
        url: '/Master/GetDeleteUser/?ID=' + index + '',
        success: function (data) {
            if (data == "Delete") {
                LoadUserDetails();

                alert('Deleted Successfully..');

            }
           
        }
    });
}


$(document).ready(function () {
    LoadUserDetails();
});


//BtnSave
$("#BtnSave").click(function () {

    var isAllValid = true;

    if ($('#ddlUserType').val() == '0') {

        document.getElementById("UserTypeerror").innerHTML = "Select the UserType";

        isAllValid = false;
    }
    else {

        document.getElementById("UserTypeerror").innerHTML = " ";  // remove it
    }

    if ($('#txtUserName').val() == '') {

        document.getElementById("UserNameerror").innerHTML = "Enter the UserName";

        isAllValid = false;
    }
    else {

        document.getElementById("UserNameerror").innerHTML = " ";  // remove it
    }

     if ($('#txtPassword').val() == '') {

        document.getElementById("Passwordeerror").innerHTML = "Enter the Password";

        isAllValid = false;
    }
    else {

        document.getElementById("Passwordeerror").innerHTML = " ";  // remove it
    }

     if ($('#txtConfirmPassword').val() == '') {

         document.getElementById("ConfirmPassworderror").innerHTML = "Enter the Confirm Password";

        isAllValid = false;
    }
    else {

         document.getElementById("ConfirmPassworderror").innerHTML = " ";  // remove it
     }

     if (isAllValid) {

         if ($('#txtPassword').val() != $('#txtConfirmPassword').val()) {

             isAllValid = false;
             alert('Check Password and Confirm Password');
         }
     }

    if (isAllValid) {

        var RoomDet = {
            UserName: $('#txtUserName').val().trim(),
            Password: $('#txtPassword').val().trim(),
            UserType: $('#ddlUserType').val().trim(),

        }
        console.log(RoomDet);
        $.ajax({
            url: "/Master/InsertUserCreation",
            type: "POST",
            data: JSON.stringify(RoomDet),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d == "Success") {
                    alert('Inserted Successfully..');

                    LoadUserDetails();
                }
                else if (d == "Update") {
                    alert('Update Successfully...');
                }
            },
            error: function () {
                alert('Error. Please try again.');
            }

        });
    }
});

