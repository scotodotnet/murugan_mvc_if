﻿$(function () {

    ////alert('Test')
    //$('.datepicker').datepicker(
    //    {
    //        format: "dd/mm/yyyy",
    //        autoclose: true
    //    });

    $('.select2').select2();
    $('#example1').DataTable();


});

function LoadDatatable()
{
    $.ajax({
        type: "GET",
        url: '/Master/GetESICode',
        success: function (data) {
            if (data.length != 0)
            {
                var data1 = $('#ESICodeTable').DataTable();
                data1.clear();
                $.each(data, function (key, item) {

                    data1.row.add([
                    item.Days,
                   "<button class='btn btn-danger' type='submit' onClick='DeleteESICode(" + item.Days + ")'><i class='fa fa-times-circle'></i></button>"
                    ]).draw();
                });
            }
            else {
                $("#ESICodeTable").find('tbody').empty();
                var data2 = $('#ESICodeTable').DataTable();
                data2.clear();
            }

           
        }
    });

}

function LoadAllData() {
    //alert('Dept');
    //var EmpType = $('#ddlEmpType').val();

    $.ajax({
        type: "GET",
        url: '/Master/GetAllDatas',
        success: function (data) {
            //console.log(data);
            if (data.length != 0) {
                $('#PF').val(data[0].PF_per);
                $('#ESI').val(data[0].ESI_per);
                $('#PFSalary').val(data[0].StaffSalary);
                $('#EmperAC1').val(data[0].EmployeerPFone);
                $('#EmperAC2').val(data[0].EmployeerPFTwo);
                $('#EmperESI').val(data[0].EmployeerESI);
            }
            else {
                $('#PF').val('');
                $('#ESI').val('');
                $('#PFSalary').val('');
                $('#EmperESI').val('');
                $('#EmperAC1').val('');
                $('#EmperAC2').val('');
            }
        }
    })

}

function DeleteESICode(index)
{
    $.ajax({
        type: "GET",
        url: '/Master/DeleteESICode/?ESICode='+ index +'',
        success: function (d) {
            if (d == "Deleted") {
                alert('Deleted Successfully..');
                LoadDatatable();
            }
            else
            {
                alert('ESICode Not Found');
                LoadDatatable();

            }
        },
        error: function () {
            alert('Error. Please try again.');
        }
    });
}


$(document).ready(function () {
 
    LoadDatatable();
    LoadAllData();

    $('#PFESIMSave').click(function () {

        //alert('hi');
        //validation 
        var isAllValid = true;

        if ($('#PF').val() == '') {
            document.getElementById("PFerror").innerHTML = "Enter the PF";
            isAllValid = false;
        }
        else {

            document.getElementById("PFerror").innerHTML = " "; // remove it
        }

        if ($('#ESI').val() == '') {
            document.getElementById("ESIerror").innerHTML = "Enter the ESI";
            isAllValid = false;
        }
        else {

            document.getElementById("ESIerror").innerHTML = " "; // remove it
        }

        if ($('#PFSalary').val() == '') {
            document.getElementById("PFSalaryerror").innerHTML = "Enter the PF Salary";
            isAllValid = false;
        }
        else {

            document.getElementById("PFSalaryerror").innerHTML = " "; // remove it
        }

        if ($('#EmperESI').val() == '') {
            document.getElementById("EmperESIerror").innerHTML = "Enter the Employeer ESI";
            isAllValid = false;
        }
        else {

            document.getElementById("EmperESIerror").innerHTML = " "; // remove it
        }

        if ($('#EmperAC1').val() == '') {
            document.getElementById("EmperAC1error").innerHTML = "Enter the Employeer PF A/C 1";
            isAllValid = false;
        }
        else {

            document.getElementById("EmperAC1error").innerHTML = " "; // remove it
        }

        if ($('#EmperAC2').val() == '') {
            document.getElementById("EmperAC2error").innerHTML = "Enter the Employeer PF A/C 2";
            isAllValid = false;
        }
        else {

            document.getElementById("EmperAC2error").innerHTML = " "; // remove it
        }
        //Save if valid
        if (isAllValid) {
            var RoomDet = {
                UserType: $('#PF').val().trim(),
                BasicSalary: $('#ESI').val().trim(),
                Convey: $('#PFSalary').val().trim(),
                CodeVal: $('#EmperESI').val().trim(),
                TypeVal: $('#EmperAC1').val().trim(),
                LeaveType: $('#EmperAC2').val().trim(),
            }
            console.log(RoomDet);
            $.ajax({
                url: "/Master/SavePFESIDetails",
                type: "POST",
                data: JSON.stringify(RoomDet),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d == "Insert") {
                        clear();
                        LoadDatatable();
                        alert('Inserted Successfully..');

                    }
                    else if (d == "Update") {

                        clear();
                        LoadDatatable();
                        alert('Update Successfully...');

                    }

                },
                error: function () {
                    alert('Error. Please try again.');
                }
            });
        }
  
    });


    $('#PFESIMClear').click(function () {
        clear1();
    });


    $('#ESIMSave').click(function () {
        //alert('hi');
        //validation 
        var isAllValid = true;


        if ($('#ESICode').val() == '') {
            document.getElementById("ESICodeerror").innerHTML = "Enter the ESICode";
            isAllValid = false;
        }
        else {

            document.getElementById("ESICodeerror").innerHTML = " "; // remove it
        }

        //Save if valid
        if (isAllValid) {
            var RoomDet = {               
                HRA: $('#ESICode').val().trim(),
            }
            console.log(RoomDet);
            $.ajax({
                url: "/Master/SaveESICodeDetails",
                type: "POST",
                data: JSON.stringify(RoomDet),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d == "Insert") {
                        clear1();
                        LoadDatatable();
                        alert('Inserted Successfully..');
                    }
                    else if (d == "Update") {

                        clear1();
                        LoadDatatable();
                        alert('Update Successfully...');
                    }

                },
                error: function () {
                    alert('Error. Please try again.');
                }
            });
        }
    });

    $('#ESIMClear').click(function () {
        clear1();
    });

});

function clear() {

    $("#PF").val('');
    $("#ESI").val('');
    $("#PFSalary").val('');
    $("#EmperESI").val('');
    $("#EmperAC1").val('');
    $("#EmperAC2").val('');

    document.getElementById("PFerror").innerHTML = " ";
    document.getElementById("ESIerror").innerHTML = " ";
    document.getElementById("PFSalaryerror").innerHTML = " ";
    document.getElementById("EmperESIerror").innerHTML = " ";
    document.getElementById("EmperAC1error").innerHTML = " ";
    document.getElementById("EmperAC2error").innerHTML = " ";
   
}

function clear1() {
    
    $("#ESICode").val('');    

    document.getElementById("ESICodeerror").innerHTML = " ";
}