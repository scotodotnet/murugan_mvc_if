﻿
$(function () {

    $('.datepicker').datepicker(
        {
            format: "dd/mm/yyyy",
            autoclose: true
        });

    $('.select2').select2();

    $('#example1').DataTable();
    $('#example2').DataTable();

});

function LoadFinancialYear() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Report/GetFinancialYear1',
        success: function (data) {

            $('#ddlYear').empty();
            $("#ddlYear").append($('<option></option>').val('0').text('-Select-'));
            $.each(data, function (i, val) {

                $("#ddlYear").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}


function LoadBrokerName() {
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GetBrokerName',
        success: function (data) {

            $("#ddlBrokerName").empty();
            $("#ddlBrokerName").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlBrokerName").append($('<option></option>').val(val.BrokerID).text(val.BrokerName));

            })
        }
    })

}

function LoadOperatorsName() {
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GetOperatorsNames',
        success: function (data) {

            $("#ddlOperatorName").empty();
            $("#ddlOperatorName").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlOperatorName").append($('<option></option>').val(val.OperatorID).text(val.OperatorName));

            })
        }
    })
}

function LoadReportMenu() {
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GetReportMenu',
        success: function (data) {

            $("#ddlReportMenu").empty();
            $("#ddlReportMenu").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlReportMenu").append($('<option></option>').val(val.UserName).text(val.UserName));

            })
        }
    })

}

function LoadReportSubMenu() {
    //alert('hi');
    var Report_Head = $("#ddlReportMenu").val();
    $("#ddlReportSubMenu").removeAttr("disabled");
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GetReportSubMenu/?Report_Head=' + Report_Head + '',
        success: function (data) {

            $("#ddlReportSubMenu").empty();
            $("#ddlReportSubMenu").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlReportSubMenu").append($('<option></option>').val(val.WagesType).text(val.WagesType));

            })
        }
    })

}

function LoadReportShift() {
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GettShifType',
        success: function (data) {

            $("#ddlShift").empty();
            $("#ddlShift").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlShift").append($('<option></option>').val(val.ShiftType).text(val.ShiftType));

            })
        }
    })

}
function LoadDepartment() {
    //alert('Dept');

    $.ajax({
        type: "GET",
        url: '/Report/GetDepartment',
        success: function (data) {
            //console.log(data);
            //Employee = data;
            $('#ddlDept').empty();
            $("#ddlDept").append($('<option></option>').val('0').text('-Select-'));
            $.each(data, function (i, val) {

                $("#ddlDept").append($('<option></option>').val(val.ID).text(val.Name));

            })
        }
    })
}

function LoadReportWages() {
    //ajax function for fetch data
    $.ajax({
        type: "GET",
        url: '/Report/GettWagesType',
        success: function (data) {

            $("#ddlWagesType").empty();
            $("#ddlWagesType").append($('<option></option>').val('0').text('Select'));

            $.each(data, function (i, val) {

                $("#ddlWagesType").append($('<option></option>').val(val.UserType).text(val.WagesType));

            })
        }
    })

}



function LoadGoodsDataTable() {

    $.ajax({
        type: "GET",
        url: '/Report/GetEmpID',
        success: function (data) {
            var data1 = $('#example1').DataTable();
            data1.clear();
            $.each(data, function (key, item) {

                data1.row.add([
                item.EmpNo,
                item.ExistingCode,
                item.EmpName,
                item.DeptName,
               "<button class='btn btn-success' type='button' onClick='EditEmpID(\"" + item.EmpNo + "\")'><i class='fa fa-pencil'></i></button> "
                ]).draw();
            });
        }
    });
}
function EditEmpID(index) {

   
                $("#txtMachineID").val(index);
              
          
            $('.modal.in').modal('hide')
      

}

function Clear() {
    $("#ddlShift").val('0').change();
    $("#ddlWagesType").val('0').change();
    //$("#ddlLocSite").val('0').change();
    $("#txtFromDate").val(' ');
    $("#txtToDate").val(' ');
    $('#ddlReportSubMenu').val('0').change();
    $('#ddlReportMenu').val('0').change();

    $("#ddlShift").attr("disabled", true);
    $("#ddlWagesType").attr("disabled", true);
    $("#txtFromDate").attr("disabled", true);
    $("#txtToDate").attr("disabled", true);
    $("#txtMachineID").attr("disabled", true);

    ShowAndHide();
}


$("#btnLoad").click(function () {

    var FromDate = $('#txtFromDate').val();
    var ShiftType = $('#ddlShift').val();
    var ReportNames = $('#ddlReportSubMenu').val();

    if (ReportNames == "DayAttendanceDayWise") {

        alert(ReportNames);
        var isAllValid = true;

        //if ($('#ddlShift').val() == '0') {

        //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

        //    isAllValid = false;
        //}
        //else {

        //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
        //}

        if ($('#txtFromDate').val().trim() == '') {

            document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            isAllValid = false;
        }
        else {

            document.getElementById("FromDateerror").innerHTML = " ";  // remove it
        }

        if (isAllValid) {

            $.ajax({
                type: "GET",
                url: '/Report/DayWiseDatatable/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '',
                success: function (data) {

                    var data1 = $('#example2').DataTable();
                    data1.clear();
                    $.each(data, function (key, item) {

                        data1.row.add([
                        item.SNo,
                        item.EmpNo,
                        item.EmpName,
                        item.DeptName,
                        item.TimeIn,
                        item.TimeOut,
                        item.Total_Hrs,
                       //"<button class='btn btn-success' type='button' onClick='EditEmpID(\"" + item.EmpNo + "\")'><i class='fa fa-pencil'></i></button> "
                        ]).draw();
                    });

                },
                error: function () {
                    alert('Error. Please try again.');

                }
            });
        }
    }
    //DayAttendanceBetWDates
});

$(document).ready(function () {

    //LoadReportMenu();
    //LoadReportSubMenu();
    LoadReportShift();
    LoadReportWages();
    LoadDepartment();
    LoadGoodsDataTable();
    LoadBrokerName();
    LoadOperatorsName();
    LoadFinancialYear();

    $("#btnReport").click(function () {
        //alert('hi');
        var FromDate = $('#txtFromDate').val();
        var ToDate = $('#txtToDate').val();
        var ShiftType = $('#ddlShift').val();
        var WagesType = $('#ddlWagesType').val();
        var ModeType = $('#ddlModeType').val();
        var ReportNames = $('#ddlReportSubMenu').val();
        var EmpNo = $('#txtMachineID').val();
        var BelowHour = $('#txtBelowHours').val();
        var AboveHour = $('#txtAboveHours').val();

        if (ReportNames == "DAY ATTENDANCE - DAY WISE") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/DayAttendanceDayWise/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }
        else if (ReportNames == "DAY ATTENDANCE - BETWEEN DATES") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/MusterReportBWDates/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }
        //HR LEAVE REPORT - LABOUR WISE
        if (ReportNames == "HR LEAVE REPORT - LABOUR WISE") {

            var isAllValid = true;

            //if ($('#ddlBrokerName').val() == '0') {

            //    document.getElementById("BrokerNameError").innerHTML = "Select the BrokerName";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HRLeaveLabourWise/?FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }
        //Salary Consolidate
        if (ReportNames == "SALARY CONSOLIDATE REPORT") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/SalaryConsolidate/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }
        else if (ReportNames == "EMPLOYEE MUSTER") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/GetEmpMuster/?WagesType=' + WagesType + '&Mode=' + ModeType + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }



        //BelowHours
        if (ReportNames == "BELOW HOURS REPORT") {


            var isAllValid = true;



            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/belowhour/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '&BelowHour=' + BelowHour + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }





        //Abovehours
        if (ReportNames == "ABOVE HOURS REPORT") {


            var isAllValid = true;



            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/Abovehour/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '&AboveHour=' + AboveHour + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }




        // Betweenhours
        if (ReportNames == "BELOW AND ABOVE HOURS REPORT") {


            var isAllValid = true;



            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/Betweenhour/?AboveHour=' + AboveHour + '&BelowHour=' + BelowHour + '&FromDate=' + FromDate + '&ShiftType=' + ShiftType + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }




        //  Payrollot
        if (ReportNames == "PAYROLL OT") {


            var isAllValid = true;

            //if ($('#ddlWagesType').val() == '0') {

            //    document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/payrollotreport/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }



        //AbsentBetWDates

        if (ReportNames == "ABSENT REPORT - BETWEEN DATES") {

            var isAllValid = true;

            //if ($('#ddlWagesType').val() == '0') {

            //    document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/AbsentReportBetweenDates/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //AbsentDayWise

        if (ReportNames == "ABSENT REPORT DAY WISE") {


            var isAllValid = true;



            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/AbsentReportDayWise/?FromDate=' + FromDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }

        //Day Attendance Summary

        if (ReportNames == "DAY ATTENDANCE SUMMARY") {


            var isAllValid = true;



            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/DayAttendanceSummary/?FromDate=' + FromDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }





        //hours attendance between dates

        if (ReportNames == "HOURS ATTENDANCE - BETWEEN DATES") {

            var isAllValid = true;

            //if ($('#ddlWagesType').val() == '0') {

            //    document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HoursBtwnDates/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }



        if (ReportNames == "DAY EMPLOYEE SUMMARY") {

            var isAllValid = true;

            //if ($('#ddlWagesType').val() == '0') {

            //    document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/ImproperPunch/?FromDate=' + FromDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //PayRollAttendance
        if (ReportNames == "PAYROLL ATTENDANCE") {

            //alert(ReportNames);

            var isAllValid = true;

            //if ($('#ddlWagesType').val() == '0') {

            //    document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/PayRollAttendance/?WagesType=' + WagesType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //MisMatchShif

        if (ReportNames == "MISMATCH SHIFT REPORT - DAY WISE") {

            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the SHift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }


            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/MisMatchShifDayWise/?FromDate=' + FromDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //DayAttendanceDayWiseOT

        if (ReportNames == "DAY ATTENDANCE WITH OT HOURS") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/DayAttendanceDayWiseWithOT/?FromDate=' + FromDate + '&ShiftType=' + ShiftType + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }
        //EmployeeWiseDayAttendBTDates


        if (ReportNames == "EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES") {


            var isAllValid = true;

            if ($('#txtMachineID').val() == '') {

                document.getElementById("EmpNoerror").innerHTML = "Enter the EmpNo";

                isAllValid = false;
            }
            else {

                document.getElementById("EmpNoerror").innerHTML = " ";  // remove it
            }

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/EmployeeWiseAttendBTDates/?EmpNo=' + EmpNo + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }


        if (ReportNames == "INDIVIDUAL EMPLOYEE HISTORY") {

            var FinYear = $('#ddlYear').val();
            var isAllValid = true;

            if ($('#txtMachineID').val() == '') {

                document.getElementById("EmpNoerror").innerHTML = "Enter the EmpNo";

                isAllValid = false;
            }
            else {

                document.getElementById("EmpNoerror").innerHTML = " ";  // remove it
            }

            if (FinYear == '0') {

                alert('Select the Year');
                isAllValid = false;
            }


            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/EmployeeHistory/?EmpNo=' + EmpNo + '&FinYear=' + FinYear + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }



        //OTimeBTDates

        if (ReportNames == "OT REPORT - BETWEEN DATES") {

            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/OTTimeBWTDates/?ShiftType=' + ShiftType + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        if (ReportNames == "HR CONSOLDT - ON /OFF DUTY REPORT") {


            var isAllValid = true;




            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/OnOffDutyReport';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }

        if (ReportNames == "HR CONSOLDT - ON /OFF DUTY REPORT") {


            var isAllValid = true;




            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/OnOffDutyReport';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }



        if (ReportNames == "HR DAILY - SHIFT / GRADE WISE ON DT RPT") {


            var isAllValid = true;


            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/gradewiseonduty/?FromDate=' + FromDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }





        else if (ReportNames == "HR CONSOLIDATED REPORT") {


            var isAllValid = true;


            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/hrconsolidatereport/?FromDate=' + FromDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }

        else if (ReportNames == "NEW& REL") {


            var isAllValid = true;


            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/NewReliveHead/?FromDate=' + FromDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }




        //LEADER COMMISSION
        if (ReportNames == "LEADER COMMISSION") {

            var isAllValid = true;

            var BrokerName = $('#ddlBrokerName').val();

            //if ($('#ddlBrokerName').val() == '0') {

            //    document.getElementById("BrokerNameError").innerHTML = "Select the BrokerName";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/LeaderCommission/?BrokerName=' + BrokerName + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }




        else if (ReportNames == "LEADER COMMISION CONSOLIDATE") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/commisionconsolidate/?FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }











        else if (ReportNames == "HR DEPARTMENT LEADER WISE") {


            var isAllValid = true;



            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/departmentleadersalary/?FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }



                });


            }

        }




        else if (ReportNames == "HR DEPARTMENT") {


            var isAllValid = true;



            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/departmentwisesalary/?FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }







        else if (ReportNames == "LABOURS LIST(HR DEPARTMENT)") {


            var isAllValid = true;



            //if ($('#ddlWagesType').val() == '0') {

            //    document.getElementById("WagesTypeerror").innerHTML = "Select the Wages";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("WagesTypeerror").innerHTML = " ";  // remove it
            //}
            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/LabourList/?WagesType=' + WagesType + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }













        else if (ReportNames == "EMPLOYEE RESIGN") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/employeeresign/?FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }







        else if (ReportNames == "EMPLOYEE NEW JOINING") {


            var isAllValid = true;

            //if ($('#ddlShift').val() == '0') {

            //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/employeejoining/?FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }


        if (ReportNames == "HR TRACER") {

            var isAllValid = true;

            var CurrentDate = $('#txtCurrentDate').val();
            var DeptName = $('#ddlDept').val();

            //if ($('#ddlBrokerName').val() == '0') {

            //    document.getElementById("BrokerNameError").innerHTML = "Select the BrokerName";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtCurrentDate').val().trim() == '') {

                document.getElementById("CurrentDateError").innerHTML = "Enter the Current Date";

                isAllValid = false;
            }
            else {

                document.getElementById("CurrentDateError").innerHTML = " ";  // remove it
            }

            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HRTracer/?DeptName=' + DeptName + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '&CurrentDate=' + CurrentDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //HR CONSOLIDATED - GROUP WISE
        if (ReportNames == "HR CONSOLIDATED - GROUP WISE") {

            var isAllValid = true;


            //if ($('#txtFromDate').val().trim() == '') {

            //    document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HRConsolidatesGroupWise';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //HR CONSOLIDATED - DEPT WISE
        if (ReportNames == "HR CONSOLIDATED - DEPT WISE") {

            var isAllValid = true;


            //if ($('#txtFromDate').val().trim() == '') {

            //    document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HRConsolidatesDeptWise';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        // HR CONSOLIDATE SOURCE GRADE DEPT WISE
        if (ReportNames == "SOURCE GRADE DEPT WISE") {

            var isAllValid = true;


            //if ($('#txtFromDate').val().trim() == '') {

            //    document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HRConsolidatesDeptWiseSource';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //HR CONSOLIDATED - DEPT GROUP WISE
        if (ReportNames == "HR CONSOLIDATED - DEPT GROUP WISE") {

            var isAllValid = true;


            //if ($('#txtFromDate').val().trim() == '') {

            //    document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HRConsolidatesDeptGroupWise';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //HR CONSOLIDATED - STRENGTH
        if (ReportNames == "HR CONSOLIDATED - STRENGTH") {

            var isAllValid = true;


            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HRConsolidatesStrength/?FromDate=' + FromDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //LEADER COMMISSION
        if (ReportNames == "LEADER COMMISSION") {

            var isAllValid = true;

            var BrokerName = $('#ddlBrokerName').val();

            //if ($('#ddlBrokerName').val() == '0') {

            //    document.getElementById("BrokerNameError").innerHTML = "Select the BrokerName";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/LeaderCommission/?BrokerName=' + BrokerName + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //CANTEEN COST
        if (ReportNames == "CANTEEN COST") {

            var isAllValid = true;

            var BrokerName = $('#ddlBrokerName').val();
            var OperatorName = $('#ddlOperatorName').val();
            if ($('#ddlBrokerName').val() == '0') {

                document.getElementById("BrokerNameError").innerHTML = "Select the BrokerName";

                isAllValid = false;
            }
            else {

                document.getElementById("BrokerNameError").innerHTML = " ";  // remove it
            }

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/CanteenCost/?BrokerName=' + BrokerName + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '&OperatorName=' + OperatorName + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //HR LEAVE REPORT - BROKER WISE
        if (ReportNames == "HR LEAVE REPORT - BROKER WISE") {

            var isAllValid = true;

            var BrokerName = $('#ddlBrokerName').val();

            //if ($('#ddlBrokerName').val() == '0') {

            //    document.getElementById("BrokerNameError").innerHTML = "Select the BrokerName";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HRLeaveBrokerWise/?BrokerName=' + BrokerName + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }
        //HR LEAVE REPORT - LABOUR WISE
        

        //HR LEAVE REPORT - DEPTARTMENT WISE
        if (ReportNames == "HR LEAVE REPORT - DEPTARTMENT WISE") {

            var isAllValid = true;


            //if ($('#ddlBrokerName').val() == '0') {

            //    document.getElementById("BrokerNameError").innerHTML = "Select the BrokerName";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }




            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/HRLeaveDeptWise/?FromDate=' + FromDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

        //EMPLOYEE'S GRADE
        if (ReportNames == "EMPLOYEES GRADE") {

            var isAllValid = true;

            var DeptName = $('#ddlDept').val();

            //if ($('#ddlBrokerName').val() == '0') {

            //    document.getElementById("BrokerNameError").innerHTML = "Select the BrokerName";

            //    isAllValid = false;
            //}
            //else {

            //    document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            //}

            if ($('#txtFromDate').val().trim() == '') {

                document.getElementById("FromDateerror").innerHTML = "Enter the From Date";

                isAllValid = false;
            }
            else {

                document.getElementById("FromDateerror").innerHTML = " ";  // remove it
            }

            if ($('#txtToDate').val().trim() == '') {

                document.getElementById("txtToDateerror").innerHTML = "Enter the To Date";

                isAllValid = false;
            }
            else {

                document.getElementById("txtToDateerror").innerHTML = " ";  // remove it
            }


            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/EmployeesGrade/?DeptName=' + DeptName + '&FromDate=' + FromDate + '&ToDate=' + ToDate + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }
        }

    });

  

    $("#btnPFESIReport").click(function () {
        var PFNo = $('#ddlPFCode').val();
        var ESINo = $('#ddlESICode').val();
        var ReportNames = $('#ddlReportSubMenu').val();



     if (ReportNames == "EMPLOYEE MUSTER") {


            var isAllValid = true;

        //if ($('#ddlShift').val() == '0') {

        //    document.getElementById("ShiftTypeerror").innerHTML = "Select the Shift";

        //    isAllValid = false;
        //}
        //else {

        //    document.getElementById("ShiftTypeerror").innerHTML = " ";  // remove it
        //}



            if (isAllValid) {

                $.ajax({
                    url: '/Report/ReportMaster',
                    success: function () {
                        var url = '/Report/GetEmpMusterESIPF/?PFNo=' + PFNo + '&ESINo=' + ESINo + '';
                        window.open(url);
                    },
                    error: function () {
                        alert('Error. Please try again.');

                    }
                });


            }

        }


    });
});

function ShowAndHide() {

    var ReportNames = $('#ddlReportSubMenu').val();

    if (ReportNames == "0") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');


        $("#ddlShift").attr("disabled", true);
        $("#ddlWagesType").attr("disabled", true);
        $("#txtFromDate").attr("disabled", true);
        $("#txtToDate").attr("disabled", true);
        $("#txtMachineID").attr("disabled", true);
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

      
        $('#btnReport').css('display', 'block');
       

    }
    else if (ReportNames == "DAY ATTENDANCE - DAY WISE") {
        //alert(ReportNames);
        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');

        $("#ddlShift").removeAttr("disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

        $('#btnReport').css('display', 'block');
       

    }
    else if (ReportNames == "EMPLOYEE MUSTER") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'block');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlWagesType").removeAttr("disabled");
        $("#txtFromDate").attr("disabled", "disabled");
       
        //$("#txtToDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

        //$('#btnReport').css('display', 'block');
        

    }
    else if (ReportNames == "DAY ATTENDANCE - BETWEEN DATES") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'block');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").removeAttr("disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

        $('#btnReport').css('display', 'block');
        

    }

    else if (ReportNames == "SALARY CONSOLIDATE REPORT") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');




        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlEmployeeName").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlYear").attr("disabled", "disabled");
        $("#ddlIsAct").attr("disabled", "disabled");

        $("#ddlCategory").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");
        $("#txtAboveHours").attr("disabled", "disabled");
        $("#txtBelowHours").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");

        $("#ddlShift").removeAttr("disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

      


    }






    else if (ReportNames == "BELOW AND ABOVE HOURS REPORT") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');




        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlEmployeeName").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlYear").attr("disabled", "disabled");
        $("#ddlIsAct").attr("disabled", "disabled");

        $("#ddlCategory").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");
        $("#txtAboveHours").removeAttr("disabled");
        $("#txtBelowHours").removeAttr("disabled");
        $("#ddlMonth").attr("disabled", "disabled");

        $("#ddlShift").removeAttr("disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");

      
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");



    }
    else if (ReportNames == "ABOVE HOURS REPORT") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');




        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlEmployeeName").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlYear").attr("disabled", "disabled");
        $("#ddlIsAct").attr("disabled", "disabled");

        $("#ddlCategory").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");
        $("#txtAboveHours").removeAttr("disabled");
        $("#txtBelowHours").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");

        $("#ddlShift").removeAttr("disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");

       
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
       




    }
    else if (ReportNames == "BELOW HOURS REPORT") {



        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');




        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlEmployeeName").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlYear").attr("disabled", "disabled");
        $("#ddlIsAct").attr("disabled", "disabled");

        $("#ddlCategory").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");
        $("#txtAboveHours").attr("disabled", "disabled");
        $("#txtBelowHours").removeAttr("disabled");
        $("#ddlMonth").attr("disabled", "disabled");

        $("#ddlShift").removeAttr("disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");

       
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");



    }
    else if (ReportNames == "ON OFF DUTY REPORT") {
        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');
        $('#DaysWiseDatatable1').css('display', 'block');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');
        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").removeAttr("disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
       
    }
    else if (ReportNames == "LEADER COMMISSION") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").removeAttr("disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

        
    }
    else if (ReportNames == "ABSENT REPORT DAY WISE") {



        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');




        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlEmployeeName").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlYear").attr("disabled", "disabled");
        $("#ddlIsAct").attr("disabled", "disabled");

        $("#ddlCategory").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");
        $("#txtAboveHours").attr("disabled", "disabled");
        $("#txtBelowHours").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");

        

        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");


    }
    else if (ReportNames == "DAY ATTENDANCE SUMMARY") {



        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');




        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlEmployeeName").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlYear").attr("disabled", "disabled");
        $("#ddlIsAct").attr("disabled", "disabled");

        $("#ddlCategory").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");
        $("#txtAboveHours").attr("disabled", "disabled");
        $("#txtBelowHours").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");


        

        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

    }

    else if (ReportNames == "PAYROLL OT") {



        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');




        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlEmployeeName").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlYear").attr("disabled", "disabled");
        $("#ddlIsAct").attr("disabled", "disabled");

        $("#ddlCategory").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");
        $("#txtAboveHours").attr("disabled", "disabled");
        $("#txtBelowHours").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").removeAttr("disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");

        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
    }




    else if (ReportNames == "ABSENT REPORT - BETWEEN DATES") {





        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');




        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlEmployeeName").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlYear").attr("disabled", "disabled");
        $("#ddlIsAct").attr("disabled", "disabled");

        $("#ddlCategory").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");
        $("#txtAboveHours").attr("disabled", "disabled");
        $("#txtBelowHours").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").removeAttr("disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");



     
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

    }




    else if (ReportNames == "HOURS ATTENDANCE - BETWEEN DATES") {




        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');




        $("#ddlShift").attr("disabled", "disabled");

        $("#ddlEmployeeName").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlYear").attr("disabled", "disabled");
        $("#ddlIsAct").attr("disabled", "disabled");

        $("#ddlCategory").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");
        $("#txtAboveHours").attr("disabled", "disabled");
        $("#txtBelowHours").attr("disabled", "disabled");
        $("#ddlMonth").attr("disabled", "disabled");

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").removeAttr("disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");

       
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

    }

    else if (ReportNames == "DAY ATTENDANCE SUMMARY") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        //$("#txtArrivalNo").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
       

    }

    else if (ReportNames == "DAY EMPLOYEE SUMMARY") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        //$("#txtArrivalNo").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
    }

    else if (ReportNames == "PAYROLL ATTENDANCE") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").removeAttr("disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        //$("#txtArrivalNo").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
      
    }

    else if (ReportNames == "MISMATCH SHIFT REPORT - DAY WISE") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        //$("#txtArrivalNo").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
    }


    else if (ReportNames == "DAY ATTENDANCE WITH OT HOURS") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');

        $("#ddlShift").removeAttr("disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        

    }

    else if (ReportNames == "EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $("#txtMachineID").removeAttr("disabled");
        

    }

     else if (ReportNames == "INDIVIDUAL EMPLOYEE HISTORY") {

        $("#ddlShift").val('0').change();
        $("#ddlYear").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#ddlYear").removeAttr("disabled");
        $("#txtFromDate").attr("disabled", "disabled");
        $("#txtToDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $("#txtMachineID").removeAttr("disabled");
       

    }

    else if (ReportNames == "OT REPORT - BETWEEN DATES") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');

        $("#ddlShift").attr("disabled", "disabled");
        //$("#ddlLocSite").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", true);
        $("#txtFromDate").attr("disabled", false);
        $("#txtToDate").attr("disabled", false);
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");

       

    }

    else if (ReportNames == "ATTENDANCE CONVERSION") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');
        $("#ddlShift").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");

        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", false);
        $("#txtFromDate").attr("disabled", false);
        $("#txtToDate").attr("disabled", false);

        
        $('#btnReport').css('display', 'none');
        

    }
    else if (ReportNames == "HR CONSOLDT - ON /OFF DUTY REPORT") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").attr("disabled", "disabled");
        $("#txtToDate").attr("disabled", "disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }
    else if (ReportNames == "EMPLOYEE RESIGN") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }
    else if (ReportNames == "EMPLOYEE NEW JOINING") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }

    else if (ReportNames == "HR CONSOLIDATED REPORT") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $("#ddlModeType").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }
        //New & Relive

    else if (ReportNames == "NEW& REL") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $("#ddlModeType").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
       
    }


    else if (ReportNames == "LABOURS LIST(HR DEPARTMENT)") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").removeAttr("disabled");

        $("#txtFromDate").attr("disabled", "disabled");
        $("#txtToDate").attr("disabled", "disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }

    else if (ReportNames == "LEADER COMMISION CONSOLIDATE") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }


    else if (ReportNames == "HR DEPARTMENT LEADER WISE") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }
    else if (ReportNames == "HR DEPARTMENT") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }

    else if (ReportNames == "HR DAILY - SHIFT / GRADE WISE ON DT RPT") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }

    else if (ReportNames == "HR CONSOLDT - ON /OFF DUTY REPORT") {

        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        //$("#ddlLocSite").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#txtMachineID").val(' ');


        $('#DaysWiseDatatable1').css('display', 'none');
        $('#DaysAttendanceBTDatesDatatable1').css('display', 'none');


        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");

        $("#txtFromDate").attr("disabled", "disabled");
        $("#txtToDate").attr("disabled", "disabled");

        $("#txtMachineID").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $('#btnReport').css('display', 'block');
        
    }
    else if (ReportNames == "HR TRACER") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").removeAttr("disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");      
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").removeAttr("disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

        
    }
    else if (ReportNames == "HR CONSOLIDATED - GROUP WISE") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").attr("disabled", "disabled");
        $("#txtToDate").attr("disabled", "disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        
    }
    else if (ReportNames == "HR CONSOLIDATED - DEPT WISE") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").attr("disabled", "disabled");
        $("#txtToDate").attr("disabled", "disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $("#ddlModeType").attr("disabled", "disabled");
       
    }
    else if (ReportNames == "SOURCE GRADE DEPT WISE") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").attr("disabled", "disabled");
        $("#txtToDate").attr("disabled", "disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");
        $("#ddlModeType").attr("disabled", "disabled");
        
    }
    else if (ReportNames == "HR CONSOLIDATED - DEPT GROUP WISE") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").attr("disabled", "disabled");
        $("#txtToDate").attr("disabled", "disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

       
    }
    else if (ReportNames == "HR CONSOLIDATED - STRENGTH") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

       
    }
    else if (ReportNames == "LEADER COMMISSION") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").removeAttr("disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

       
    }
    else if (ReportNames == "CANTEEN COST") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").removeAttr("disabled");
        $("#ddlOperatorName").removeAttr("disabled");


      
    }
    else if (ReportNames == "HR LEAVE REPORT - BROKER WISE") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").removeAttr("disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");


     
    }
    else if (ReportNames == "HR LEAVE REPORT - LABOUR WISE") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");


        
    }
    else if (ReportNames == "HR LEAVE REPORT - DEPTARTMENT WISE") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").attr("disabled", "disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").attr("disabled", "disabled");
        //$("#txtLotNo").attr("disabled", "disabled");
        //$("#txtBatchCount").attr("disabled", "disabled");
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");


      
    }

    else if (ReportNames == "EMPLOYEES GRADE") {


        $("#ddlShift").val('0').change();
        $("#ddlWagesType").val('0').change();
        $("#ddlDept").val('0').change();
        $("#txtFromDate").val(' ');
        $("#txtToDate").val(' ');
        $("#ddlBrokerName").val('0').change();
        $("#txtMachineID").val(' ');
        $('#txtCurrentDate').val('');

        $("#ddlShift").attr("disabled", "disabled");
        $("#ddlDept").removeAttr("disabled");
        $("#ddlWagesType").attr("disabled", "disabled");
        $("#txtFromDate").removeAttr("disabled");
        $("#txtToDate").removeAttr("disabled");
            
        $("#txtMachineID").attr("disabled", "disabled");
        $("#txtCurrentDate").attr("disabled", "disabled");
        $("#ddlBrokerName").attr("disabled", "disabled");
        $("#ddlOperatorName").attr("disabled", "disabled");

       
    }

}

